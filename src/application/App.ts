import '../../node_modules/@fortawesome/fontawesome-free/css/all.css';
import Component                                  from 'vue-class-component';
import MenuComponent                              from '@/componentsGui/menuComponent/MenuComponent';
import NotificationsHarness                       from '@/harnesses/notifications/NotificationsHarness';
import Vue                                        from 'vue';

/**
 * Main application component. This hosts the TestParametersComponent
 * that lets a user interactively explore the avatar parameters.
 *
 * @export
 * @class App
 * @extends {Vue}
 */
@Component({
  components: {
    MenuComponent,
  },
})
export default class App extends Vue {
  private notificationsHarnessHandlers: NotificationsHarness | undefined;
  private showToolbar: boolean = true;
  private showHeader: boolean = true;

  private mounted() {
    this.setupNotificationHarnessHandlers();
  }

  public data(): any {
    return {};
  }


  /**
   * listen to requests to hide or show the header and toolbars
   *
   * @private
   * @memberof App
   */
  private setupNotificationHarnessHandlers() {
    this.notificationsHarnessHandlers = new NotificationsHarness();
    this.notificationsHarnessHandlers
        .onHeaderVisibilityUpdated(this, (isVisible: boolean) => {
          this.showHeader = isVisible;
        })
        .onToolbarVisibilityUpdated(this, (isVisible: boolean) => {
          this.showToolbar = isVisible;
        });
  }

}
