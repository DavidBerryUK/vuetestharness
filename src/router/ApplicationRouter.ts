import { RouteConfig }                          from 'vue-router';
import AboutPage                                from '@/views/about/AboutPage';
import AvatarListPage                           from '@/harnesses/pages/avatarList/view/AvatarListPage';
import AvatarPage                               from '@/harnesses/pages/avatar/view/AvatarPage';
import AxisTimePage                             from '@/harnesses/pages/axisTime/view/AxisTimePage';
import BadgePage                                from '@/harnesses/pages/badge/view/BadgePage';
import ColourPreviewLensPage                    from '@/harnesses/pages/colourPreviewLens/view/ColourPreviewLensPage';
import ColourSelectorPage                       from '@/harnesses/pages/colourSelector/view/ColourSelectorPage';
import ColourSlidersPage                        from '@/harnesses/pages/colourSliders/view/ColourSlidersPage';
import DataListKeyPage                          from '@/harnesses/pages/dataListKey/view/DataListKeyPage';
import DatePickerPage                           from '@/harnesses/pages/datePicker/view/DatePickerPage';
import EventLimiterPage                         from '@/harnesses/pages/eventLimiter/EventLimiterPage';
import HomePage                                 from '@/views/home/HomePage';
import ImagePickerPage                          from '@/harnesses/pages/imagePicker/view/ImagePickerPage';
import LoaderPage                               from '@/harnesses/pages/loader/view/LoaderPage';
import MessageListPage                          from '@/harnesses/pages/messageList/view/MessageListPage';
import MultipleGalleryPage                      from '@/harnesses/pages/multipleImageGallery/view/MultipleGalleryPage';
import NumberSpinnerPage                        from '@/harnesses/pages/numberSpinner/view/NumberSpinnerPage';
import ResizeDirectivePage                      from '@/harnesses/pages/resizeDirectivePage/ResizeDirectivePage';
import SamplePage                               from '@/harnesses/pages/sample/view/SamplePage';
import ScheduleDemoPage                         from '@/harnesses/demos/schedule/view/ScheduleDemoPage';
import ScheduleGridPage                         from '@/harnesses/pages/scheduleGrid/view/ScheduleGridPage';
import ScheduleProgramGridPage                  from '@/harnesses/pages/scheduleProgramGrid/view/ScheduleProgramGridPage';
import SimpleChartPage                          from '@/harnesses/pages/simpleChart/view/SimpleChartPage';
import SingleGalleryPage                        from '@/harnesses/pages/singleImageGallery/view/SingleGalleryPage';
import TimeLineSummaryPage                      from '@/harnesses/pages/timeLineSummary/view/TimeLineSummaryPage';
import Vue                                      from 'vue';
import VueRouter                                from 'vue-router';

Vue.use(VueRouter);



export default class ApplicationRouter extends VueRouter {
  constructor() {
    super({
      routes: [
        ApplicationRouter.registerAbout(),
        ApplicationRouter.registerAvatar(),
        ApplicationRouter.registerAvatarList(),
        ApplicationRouter.registerAxisTime(),
        ApplicationRouter.registerBadge(),
        ApplicationRouter.registerColourPreviewLens(),
        ApplicationRouter.registerColourSelector(),
        ApplicationRouter.registerColourSliders(),
        ApplicationRouter.registerDataListKey(),
        ApplicationRouter.registerDatePicker(),
        ApplicationRouter.registerDefaultRoute(),
        ApplicationRouter.registerDirectiveResize(),
        ApplicationRouter.registerEventLimiter(),
        ApplicationRouter.registerHome(),
        ApplicationRouter.registerImagePicker(),
        ApplicationRouter.registerLoader(),
        ApplicationRouter.registerMessageList(),
        ApplicationRouter.registerMultipleGallery(),
        ApplicationRouter.registerNumberSpinner(),
        ApplicationRouter.registerSample(),
        ApplicationRouter.registerScheduleGrid(),
        ApplicationRouter.registerScheduleProgramGrid(),
        ApplicationRouter.registerSchedulerDemo(),
        ApplicationRouter.registerSimpleChart(),
        ApplicationRouter.registerSingleGallery(),
        ApplicationRouter.registerTimeLineSummary(),
      ]});
  }

  // tslint:disable-next-line:member-ordering
  public static registerDefaultRoute(): RouteConfig {

    const route: RouteConfig = {
      path: '/',
      redirect: 'home',
    };

    return route;
  }

  public static registerEventLimiter(): RouteConfig {

    const route: RouteConfig = {
      path: '/eventLimiters',
      name: 'eventLimiters',
      component: EventLimiterPage,
    };
    return route;
  }

  // tslint:disable-next-line:member-ordering
  public static registerHome(): RouteConfig {

    const route: RouteConfig = {
      path: '/home',
      name: 'home',
      component: HomePage,
    };
    return route;
  }

  // tslint:disable-next-line:member-ordering
  public static registerAbout(): RouteConfig {

    const route: RouteConfig = {
      path: '/about',
      name: 'about',
      component: AboutPage,
    };
    return route;
  }


  public static registerDataListKey(): RouteConfig {

    const route: RouteConfig = {
      path: '/dataListKey',
      name: 'dataListKey',
      component: DataListKeyPage,
    };
    return route;
  }

  //
  public static registerDirectiveResize(): RouteConfig {

    const route: RouteConfig = {
      path: '/resizeDirective',
      name: 'resizeDirective',
      component: ResizeDirectivePage,
    };
    return route;
  }

  public static registerImagePicker(): RouteConfig {

    const route: RouteConfig = {
      path: '/imagePicker',
      name: 'imagePicker',
      component: ImagePickerPage,
    };
    return route;
  }

  public static registerAxisTime(): RouteConfig {

    const route: RouteConfig = {
      path: '/axisTime',
      name: 'axisTime',
      component: AxisTimePage,
    };
    return route;
  }


  public static registerSchedulerDemo(): RouteConfig {

    const route: RouteConfig = {
      path: '/schedulerDemo',
      name: 'schedulerDemo',
      component: ScheduleDemoPage,
    };
    return route;
  }

  public static registerDatePicker(): RouteConfig {

    const route: RouteConfig = {
      path: '/datePicker',
      name: 'datePicker',
      component: DatePickerPage,
    };
    return route;
  }

  public static registerTimeLineSummary(): RouteConfig {

    const route: RouteConfig = {
      path: '/timeLineSummary',
      name: 'timeLineSummary',
      component: TimeLineSummaryPage,
    };
    return route;
  }

  public static registerScheduleGrid(): RouteConfig {

    const route: RouteConfig = {
      path: '/scheduleGrid',
      name: 'scheduleGrid',
      component: ScheduleGridPage,
    };
    return route;
  }

  public static registerScheduleProgramGrid(): RouteConfig {

    const route: RouteConfig = {
      path: '/scheduleProgramGrid',
      name: 'scheduleProgramGrid',
      component: ScheduleProgramGridPage,
    };
    return route;
  }

  public static registerColourPreviewLens(): RouteConfig {

    const route: RouteConfig = {
      path: '/colourPreviewLens',
      name: 'colourPreviewLens',
      component: ColourPreviewLensPage,
    };
    return route;
  }

  // tslint:disable-next-line:member-ordering
  public static registerSample(): RouteConfig {

    const route: RouteConfig = {
      path: '/sample',
      name: 'sample',
      component: SamplePage,
    };

    return route;
  }

  public static registerSimpleChart(): RouteConfig {

    const route: RouteConfig = {
      path: '/simpleChart',
      name: 'simpleChart',
      component: SimpleChartPage,
    };

    return route;
  }

  public static registerNumberSpinner(): RouteConfig {

    const route: RouteConfig = {
      path: '/numberSpinner',
      name: 'numberSpinner',
      component: NumberSpinnerPage,
    };

    return route;
  }

  public static registerColourSelector(): RouteConfig {

    const route: RouteConfig = {
      path: '/colourSelector',
      name: 'colourSelector',
      component: ColourSelectorPage,
    };

    return route;
  }


  public static registerColourSliders(): RouteConfig {

    const route: RouteConfig = {
      path: '/colourSliders',
      name: 'colourSliders',
      component: ColourSlidersPage,
    };

    return route;
  }

  // tslint:disable-next-line:member-ordering
  public static registerAvatar(): RouteConfig {

    const route: RouteConfig = {
      path: '/avatar',
      name: 'avatar',
      component: AvatarPage,
    };
    return route;
  }

  public static registerAvatarList(): RouteConfig {

    const route: RouteConfig = {
      path: '/avatarList',
      name: 'avatarList',
      component: AvatarListPage,
    };
    return route;
  }


  // tslint:disable-next-line:member-ordering
  public static registerBadge(): RouteConfig {

    const route: RouteConfig = {
      path: '/badge',
      name: 'badge',
      component: BadgePage,
    };
    return route;
  }


  // tslint:disable-next-line:member-ordering
  public static registerLoader(): RouteConfig {

    const route: RouteConfig = {
      path: '/loader',
      name: 'loader',
      component: LoaderPage,
    };
    return route;
  }

  public static registerMessageList(): RouteConfig {

    const route: RouteConfig = {
      path: '/messageList',
      name: 'messageList',
      component: MessageListPage,
    };
    return route;
  }

  // tslint:disable-next-line:member-ordering
  public static registerSingleGallery(): RouteConfig {

    const route: RouteConfig = {
      path: '/singleGallery',
      name: 'singleGallery',
      component: SingleGalleryPage,
    };
    return route;
  }

  public static registerMultipleGallery(): RouteConfig {

    const route: RouteConfig = {
      path: '/multipleGallery',
      name: 'multipleGallery',
      component: MultipleGalleryPage,
    };
    return route;
  }

}
