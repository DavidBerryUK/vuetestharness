import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';
import { EnumPresentationVisualScale } from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';

@Component
export default class FooterComponent extends Vue   {

    public presentationScaleIndex: number = 7;
    private ticksLabels = [EnumPresentationVisualScale.quarterHour,
        EnumPresentationVisualScale.halfHour,
        EnumPresentationVisualScale.hour,
        EnumPresentationVisualScale.halfDay,
        EnumPresentationVisualScale.fullDay,
        EnumPresentationVisualScale.fiveDays,
        EnumPresentationVisualScale.week,
        EnumPresentationVisualScale.twoWeeks,
        EnumPresentationVisualScale.fourWeeks];

    public data(): any  {
        return {};
    }
  }

Vue.component('db-footer', FooterComponent);
