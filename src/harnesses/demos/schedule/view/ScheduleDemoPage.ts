import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import AxisRulerTimeComponent                   from '@/componentsGui/timeLineComponents/views/axisRulerTimeComponent/views/AxisRulerTimeComponent';
import Component                                from 'vue-class-component';
import FooterComponent                          from '../components/footer/FooterComponent';
import HeaderComponent                          from '../components/header/HeaderComponent';
import moment                                   from 'moment';
import NotificationsHarness                     from '@/harnesses/notifications/NotificationsHarness';
import ProgramFactory                           from '@/harnesses/testData/schedules/ProgramFactory';
import TimelineGestureRecognizerComponent       from '@/componentsGui/timeLineComponents/views/timelineGestureRecognizerComponent/TimelineGestureRecognizerComponent';
import TimeLineSummaryComponent                 from '@/componentsGui/timeLineComponents/views/timeLineSummaryComponent/views/TimeLineSummaryComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        AxisRulerTimeComponent,
        FooterComponent,
        HeaderComponent,
        TimelineGestureRecognizerComponent,
        TimeLineSummaryComponent,
    },
  })
export default class ScheduleDemoPage extends Vue {

    public dateFrom = moment().startOf('month').add(-11, 'month');
    public dateTo = moment().startOf('month').add(1, 'month').endOf('month');
    public programs = ProgramFactory.demoPrograms;

    public pixelOffsetX: number = 0;

    public presentationScale: EnumPresentationVisualScale = EnumPresentationVisualScale.twoWeeks;

    public mounted() {
        // hide headers and toolbar while this demo page is showing
        NotificationsHarness.publishHeaderHide();
        NotificationsHarness.publishToolbarHide();
    }

    public beforeDestroy() {
        // re-show headers and toolbar
        NotificationsHarness.publishHeaderShow();
        NotificationsHarness.publishToolbarShow();
    }

    public data(): any {
        return {};
    }
}
