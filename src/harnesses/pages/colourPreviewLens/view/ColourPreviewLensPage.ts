import ColourPreviewLensComponent               from '@/componentsGui/coloursComponents/colourPreviewLensComponent/ColourPreviewLensComponent';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import MaterialDesignColorConstants             from '@/componentsGui/coloursComponents/constants/MaterialDesignColourConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

@Component({
    components: {
        ColourPreviewLensComponent,
        TestHarnessComponent,
    },
  })
export default class ColourPreviewLensPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    private coloursWeb = WebNamedColourConstants;
    private coloursMaterial = MaterialDesignColorConstants;



    public data(): any {
        return {};
    }

}
