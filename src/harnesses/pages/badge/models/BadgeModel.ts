import { enumBadgeAlignment }                   from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { enumBadgePosition }                    from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyRange }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyValueOptions }                 from '@/harnessFramework/decorators/PropertyDecorator';

export default class BadgeModel {

    @Property(EnumDocumentSection.model, 'number of notifications' )
    @PropertyDefaultValue(0)
    @PropertyRange(0, 10000)
    public notifications: number;

    @Property(EnumDocumentSection.attributes, 'alignment of the badge, use enumBadgeAlignment')
    @PropertyType('enumBadgeAlignment')
    @PropertyDefaultValue(enumBadgeAlignment.topRight)
    @PropertyValueOptions([ enumBadgeAlignment.topleft, enumBadgeAlignment.topCenter, enumBadgeAlignment.topRight,
                            enumBadgeAlignment.middleLeft, enumBadgeAlignment.middleCenter, enumBadgeAlignment.middleRight,
                            enumBadgeAlignment.bottomLeft, enumBadgeAlignment.bottomCenter, enumBadgeAlignment.bottomRight])
    public alignment: string;

    @Property(EnumDocumentSection.attributes, 'position of the badge')
    @PropertyType('enumBadgePosition')
    @PropertyDefaultValue(enumBadgePosition.inside)
    @PropertyValueOptions([enumBadgePosition.inside, enumBadgePosition.border, enumBadgePosition.outside])
    public position: string;

    constructor() {
        this.notifications = 4;
        this.alignment = enumBadgeAlignment.topRight;
        this.position = enumBadgePosition.border;
    }
}
