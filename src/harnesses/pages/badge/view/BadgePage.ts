import BadgeComponent                           from '@/componentsGui/badgeComponent/BadgeComponent';
import BadgeModel                               from '@/harnesses/pages/badge/models/BadgeModel';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        BadgeComponent,
        TestHarnessComponent,
    },
  })
export default class BadgePage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new BadgeModel();

    public data(): any {
        return {};
    }

}
