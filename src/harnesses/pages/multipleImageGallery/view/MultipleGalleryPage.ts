import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';
import GallerySectionModel                      from '@/componentsGui/imageGallery/models/GallerySectionModel';
import HarnessImageGalleryModel                 from '../models/MultipleGalleryModel';
import MultipleGalleryComponent                 from '@/componentsGui/imageGallery/multipleGalleryComponent/MultipleGalleryComponent';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';


@Component({
    components: {
        MultipleGalleryComponent,
    },
  })
export default class MultipleGalleryPage extends Vue {

    private versionNo: number = 0;

    public parameters = new HarnessImageGalleryModel();

    public parametersOldValue: HarnessImageGalleryModel = this.parameters.clone();

    public ComponentIconConstants = ComponentIconConstants;

    @Prop()
    public autoShowSelectedImage!: boolean;

    private get testHarness(): TestHarnessComponent {
        const ref = this.$refs.harness as TestHarnessComponent;
        return ref;
    }

    public onSelectedNewImage(image: GalleryImageModel) {
        this.parameters.selectedImage = image;
        this.testHarness.auditEvent('selectedNewImage', JSON.stringify(image));
    }

    public onSelectedNewSection(section: GallerySectionModel) {
        this.testHarness.auditEvent('selectedNewSection', JSON.stringify(section));
    }

    public onSelectedNewGallery(gallery: GalleryModel) {
        this.testHarness.auditEvent('selectedNewGallery', JSON.stringify(gallery));
    }

    @Watch('parameters', {deep: true})
    private onParametersChanged(value: HarnessImageGalleryModel, oldValue: HarnessImageGalleryModel) {
        if ( this.parametersOldValue.autoOpenOnLoad !== this.parameters.autoOpenOnLoad) {
            this.autoOpenOnLoadValueChanged();
            this.parametersOldValue.autoOpenOnLoad = this.parameters.autoOpenOnLoad;
        }
    }

    private autoOpenOnLoadValueChanged() {
        console.log('autoOpenOnLoad ' + this.parameters.autoOpenOnLoad);
        this.versionNo++;
    }

    public data(): any {
        return {};
    }

}
