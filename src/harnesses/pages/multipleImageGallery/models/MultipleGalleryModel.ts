import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEvent }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEventValue }                   from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyObjectOptionsProvider }        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import GalleryFactory                           from '@/componentsGui/imageGallery/factory/GalleryFactory';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';
import GalleryUtilities                         from '@/utilities/gallery/GalleryUtilities';

export default class MultipleGalleryModel  {

    @Property(EnumDocumentSection.attributes, 'selected image on load')
    @PropertyType('GalleryImageModel')
    @PropertyObjectOptionsProvider('name')
    public selectedImage: GalleryImageModel;

    @Property(EnumDocumentSection.attributes, 'selection of galleries ')
    @PropertyType('Array<GalleryModel>')
    public galleries: Array<GalleryModel>;

    @Property(EnumDocumentSection.attributes, 'Automatically show selected image')
    @PropertyType(EnumVariableType.boolean)
    @PropertyDefaultValue(false)
    public autoShowSelectedImage: boolean;


    @Property(EnumDocumentSection.attributes,
        'Automatically open gallery on load, either currently selected image, or the first segment.<br> Changing this value will reload the component in the test harness')
    @PropertyDefaultValue(false)
    @PropertyType(EnumVariableType.boolean)
    public autoOpenOnLoad: boolean;


    @PropertyEvent('Event raised when user chooses a new gallery')
    @PropertyEventValue('GalleryModel')
    public selectedNewGallery!: string;

    @PropertyEvent('Event raised when user chooses a new section of images')
    @PropertyEventValue('GallerySectionModel')
    public selectedNewSection!: string;

    @PropertyEvent('Event raised when the user chooses a new image')
    @PropertyEventValue('GalleryImageModel')
    public selectedNewImage!: string;


    constructor() {
        this.autoShowSelectedImage = false;
        this.autoOpenOnLoad = false;
        this.selectedImage = GalleryImageModel.None;
        this.galleries =    [
                                GalleryFactory.getBusinessGallery(),
                                GalleryFactory.getAnimalGallery(),
                                GalleryFactory.getArtGallery(),
                            ];
    }

    public clone(): MultipleGalleryModel {
        return Object.assign(new MultipleGalleryModel(), this);
    }

    public dataForField(fieldName: string): Array<any> {

        if ( fieldName === 'selectedImage') {
            return GalleryUtilities.GetAllImagesFromGalleries(this.galleries);
        }

        return [];
    }

}
