import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import ScheduleGridComponent                    from '@/componentsGui/timeLineComponents/views/scheduleGridComponent/ScheduleGridComponent';
import ScheduleGridModel                        from '../models/ScheduleGridModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';


@Component({
    components: {
        ScheduleGridComponent,
        TestHarnessComponent,
    },
  })
export default class ScheduleGridPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new ScheduleGridModel();


    public data(): any {
        return {};
    }
}
