import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import MessageListComponent                     from '@/componentsGui/messageListComponent/view/MessageListComponent';
import MessageListDemoControlsComponent         from '../components/demoControls/MessageListDemoControlsComponent';
import MessageListModel                         from '../models/MessageListPropertiesModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        TestHarnessComponent,
        MessageListComponent,
        MessageListDemoControlsComponent,
    },
  })
export default class MessageListPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new MessageListModel();

    public data(): any {
        return {};
    }
}
