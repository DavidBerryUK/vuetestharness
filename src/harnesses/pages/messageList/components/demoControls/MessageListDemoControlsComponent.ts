import { EnumDateTimeFormat }                   from '@/componentsGui/messageListComponent/enums/MessageListEnums';
import { EnumPredefinedChats }                  from '../../enums/TestHarnessMessageListEnums';
import { Prop }                                 from 'vue-property-decorator';
import ChatFactory                              from '../../factories/ChatFactory';
import Component                                from 'vue-class-component';
import MessageListPropertiesModel               from '../../models/MessageListPropertiesModel';
import Vue                                      from 'vue';


@Component
export default class MessageListDemoControlsComponent extends Vue {

    @Prop()
    public value!: MessageListPropertiesModel;

    public predefinedChatsList: Array<string> = ChatFactory.getChatList();
    public selectedChat: EnumPredefinedChats = EnumPredefinedChats.BasicTwoWayChat;

    public showAvatar: boolean = true;

    public dateFormats = [ EnumDateTimeFormat.none, EnumDateTimeFormat.time, EnumDateTimeFormat.date, EnumDateTimeFormat.dateTime ];

    public  itemObject(obj: any): any {
        return obj;
    }

    public onResetChat() {
        this.value.chat = ChatFactory.getChat(this.selectedChat);
        this.updateModel();
    }

    public updateModel() {
        this.value.chat.showAvatar = this.showAvatar;
    }

    private data(): any  {
        return {};
    }
}

Vue.component('db-message-list-demo-controls', MessageListDemoControlsComponent);
