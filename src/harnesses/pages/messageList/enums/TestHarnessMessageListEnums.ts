export enum EnumPredefinedChats {
    BasicTwoWayChat = 'Basic 2 Way Chat',
    BasicGroupChat = 'Basic Group Chat',
    ShoppingList = 'Shopping List',
}
