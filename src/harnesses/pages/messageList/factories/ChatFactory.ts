import { EnumPredefinedChats }                  from '../enums/TestHarnessMessageListEnums';
import ChatModel                                from '@/componentsGui/messageListComponent/models/ChatModel';
import MessageModel                             from '@/componentsGui/messageListComponent/models/MessageModel';
import moment                                   from 'moment';
import UserFactory                              from './UserFactory';

export default class ChatFactory {


    public static getChat(chat: EnumPredefinedChats) {
        switch (chat) {
            case EnumPredefinedChats.BasicTwoWayChat:
                return this.basicTwoWayChat;

            case EnumPredefinedChats.ShoppingList:
                return this.shoppingList;

                case EnumPredefinedChats.BasicGroupChat:
                return this.basicGroupChat;
        }
    }

    public static getChatList(): Array<string> {
        return [
            EnumPredefinedChats.BasicGroupChat,
            EnumPredefinedChats.BasicTwoWayChat,
            EnumPredefinedChats.ShoppingList,
        ];
    }

    private static get basicGroupChat(): ChatModel {
        const barry = UserFactory.barry;
        const carry = UserFactory.carry;
        const dave = UserFactory.dave;
        const ethan = UserFactory.ethan;
        let timeTime =  moment().add(-1, 'hours');


        const chat = new ChatModel(barry);

        chat.addMessage( new MessageModel('m1', barry, timeTime, 'anybody got plans for lunch?'));

        timeTime = timeTime.add(10, 'seconds');
        chat.addMessage( new MessageModel('m2', carry, timeTime, 'no but happy to have a walk and get something'));
        timeTime = timeTime.add(2, 'seconds');
        chat.addMessage( new MessageModel('m3', dave, timeTime, 'Me too, have had a busy morning and need a break'));
        timeTime = timeTime.add(200, 'seconds');

        chat.addMessage( new MessageModel('m4', barry, timeTime, 'Great, I\'d like to visit the new sandwich shop if you are keen.'));

        timeTime = timeTime.add(30, 'seconds');
        chat.addMessage( new MessageModel('m5', ethan, timeTime, 'Sounds good Barry, 12?'));
        timeTime = timeTime.add(45, 'seconds');
        chat.addMessage( new MessageModel('m6', carry, timeTime, 'Great stuff, see you then'));
        timeTime = timeTime.add(30, 'seconds');
        chat.addMessage( new MessageModel('m7', dave, timeTime, 'excellent'));
        chat.addMessage( new MessageModel('m8', barry, timeTime, 'all sorted then, see you soon'));

        return chat;
    }



    /**
     * create basic 2 person chat with no read receipts
     *
     * @readonly
     * @static
     * @type {ChatModel}
     * @memberof ChatFactory
     */
    private static get basicTwoWayChat(): ChatModel {
        const ann = UserFactory.ann;
        const garry = UserFactory.garry;
        let timeTime =  moment().add(-2, 'days');


        const chat = new ChatModel(ann);

        chat.addMessage( new MessageModel('m1', garry, timeTime, 'Hi Ann, do you have the 2018 budget info?'));
        timeTime = timeTime.add(200, 'seconds');

        chat.addMessage( new MessageModel('m2a', ann, timeTime, 'Hello, yes, the drafts are available, do you want them?'));
        chat.addMessage( new MessageModel('m2b', ann, timeTime, 'I have just got them approved by Carry.'));
        timeTime = timeTime.add(1, 'days');
        timeTime = timeTime.add(800, 'minutes');

        chat.addMessage( new MessageModel('m3', garry, timeTime, 'Just the December sheets for the Dev project. I need to see what time frame we can deliver by, end of the year is looking ambitious'));
        timeTime = timeTime.add(350, 'seconds');

        chat.addMessage( new MessageModel('m4', ann, timeTime, 'I\'ll get it to you shortly'));
        timeTime = timeTime.add(50, 'seconds');

        chat.addMessage( new MessageModel('m5', garry, timeTime, 'thank you'));
        timeTime = timeTime.add(20, 'seconds');

        chat.addMessage( new MessageModel('m6', ann, timeTime, 'no problem'));

        return chat;
    }

    private static get shoppingList(): ChatModel {
        const fiona = UserFactory.fiona;
        const barry = UserFactory.barry;
        let timeTime =  moment().add(-2, 'hours');


        const chat = new ChatModel(barry);

        chat.addMessage( new MessageModel('m1', barry, timeTime, 'I\'m calling at Asda on way home, want anything?'));
        timeTime = timeTime.add(50, 'seconds');

        chat.addMessage( new MessageModel('ma', fiona, timeTime, 'apples'));
        chat.addMessage( new MessageModel('mb', fiona, timeTime, 'bananas'));
        chat.addMessage( new MessageModel('mc', fiona, timeTime, 'carrots'));
        chat.addMessage( new MessageModel('md', fiona, timeTime, 'dates'));
        chat.addMessage( new MessageModel('me', fiona, timeTime, 'eggs'));
        chat.addMessage( new MessageModel('mf', fiona, timeTime, 'figs'));
        chat.addMessage( new MessageModel('mg', fiona, timeTime, 'grapes'));
        chat.addMessage( new MessageModel('mh', fiona, timeTime, 'Honeydew Melon'));
        chat.addMessage( new MessageModel('mi', fiona, timeTime, 'Iceberg lettuce'));
        chat.addMessage( new MessageModel('mj', fiona, timeTime, 'Jerusalem artichoke'));
        chat.addMessage( new MessageModel('mk', fiona, timeTime, 'Kale'));
        chat.addMessage( new MessageModel('ml', fiona, timeTime, 'limes'));
        chat.addMessage( new MessageModel('mm', fiona, timeTime, 'mango'));
        chat.addMessage( new MessageModel('mn', fiona, timeTime, 'nuts'));
        chat.addMessage( new MessageModel('mo', fiona, timeTime, 'oranges'));
        chat.addMessage( new MessageModel('mp', fiona, timeTime, 'pears'));
        chat.addMessage( new MessageModel('mq', fiona, timeTime, 'Quince'));
        chat.addMessage( new MessageModel('mr', fiona, timeTime, 'Raisins'));
        chat.addMessage( new MessageModel('ms', fiona, timeTime, 'Satsumas'));
        chat.addMessage( new MessageModel('mt', fiona, timeTime, 'Tomato'));
        chat.addMessage( new MessageModel('mu', fiona, timeTime, 'Udon Noodles'));
        chat.addMessage( new MessageModel('mv', fiona, timeTime, 'Vinegar'));
        chat.addMessage( new MessageModel('mw', fiona, timeTime, 'Water Chestnut'));
        chat.addMessage( new MessageModel('mx', fiona, timeTime, 'Xigua'));
        chat.addMessage( new MessageModel('my', fiona, timeTime, 'Yams'));
        chat.addMessage( new MessageModel('mz', fiona, timeTime, 'Zucchini'));

        timeTime = timeTime.add(800, 'seconds');
        chat.addMessage( new MessageModel('m2', barry, timeTime, 'no problem'));

        return chat;
    }
}
