import MessageUserModel from '@/componentsGui/messageListComponent/models/MessageUserModel';

export default class UserFactory {
    private static userEmpty: MessageUserModel | undefined;
    private static userAnn: MessageUserModel | undefined;
    private static userBarry: MessageUserModel | undefined;
    private static userCarry: MessageUserModel | undefined;
    private static userDave: MessageUserModel | undefined;
    private static userEthan: MessageUserModel | undefined;
    private static userFiona: MessageUserModel | undefined;
    private static userGary: MessageUserModel | undefined;


    public static allUsers(): Array<MessageUserModel> {
        const users: Array<MessageUserModel> = [
            UserFactory.ann,
            UserFactory.barry,
            UserFactory.carry,
            UserFactory.dave,
            UserFactory.ethan,
            UserFactory.fiona,
            UserFactory.garry,
        ];

        return users;
    }

    public static get empty(): MessageUserModel {
        if (UserFactory.userEmpty === undefined) {
            UserFactory.userEmpty = new MessageUserModel('empty', '', '');
        }
        return UserFactory.userEmpty;
    }

    public static get ann(): MessageUserModel {
        if (UserFactory.userAnn === undefined) {
            UserFactory.userAnn = new MessageUserModel('U001', 'Ann', 'images//business//people/ann-tn.png');
        }
        return UserFactory.userAnn;
    }

    public static get barry(): MessageUserModel {
        if (UserFactory.userBarry === undefined) {
            UserFactory.userBarry = new MessageUserModel('U002', 'Barry', 'images//business//people/barry-tn.png');
        }
        return UserFactory.userBarry;
    }
    public static get carry(): MessageUserModel {
        if (UserFactory.userCarry === undefined) {
            UserFactory.userCarry = new MessageUserModel('U003', 'Carry', 'images//business//people/carry-tn.png');
        }
        return UserFactory.userCarry;
    }

    public static get dave(): MessageUserModel {
        if (UserFactory.userDave === undefined) {
            UserFactory.userDave = new MessageUserModel('U004', 'Dave', 'images//business//people/dave-tn.png');
        }
        return UserFactory.userDave;
    }

    public static get ethan(): MessageUserModel {
        if (UserFactory.userEthan === undefined) {
                UserFactory.userEthan = new MessageUserModel('U005', 'Ethan', 'images//business//people/ethan-tn.png');
        }
        return UserFactory.userEthan;
    }

    public static get fiona(): MessageUserModel {
        if (UserFactory.userFiona === undefined) {
            UserFactory.userFiona = new MessageUserModel('U006', 'Fiona', 'images//business//people/fiona-tn.png');
        }
        return UserFactory.userFiona;
    }

    public static get garry(): MessageUserModel {
        if (UserFactory.userGary === undefined) {
            UserFactory.userGary = new MessageUserModel('U007', 'Gary', 'images//business//people/gary-tn.png');
        }
        return UserFactory.userGary;
    }
}
