import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumPredefinedChats }                  from '../enums/TestHarnessMessageListEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import ChatFactory                              from '../factories/ChatFactory';
import IChatModel                               from '@/componentsGui/messageListComponent/interfaces/IChatModel';

export default class MessageListPropertiesModel  {

    @Property(EnumDocumentSection.model, 'List of messages')
    @PropertyType('IChatModel')
    public chat: IChatModel = ChatFactory.getChat(EnumPredefinedChats.BasicTwoWayChat);
}
