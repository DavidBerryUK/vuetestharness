import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEvent }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEventValue }                   from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyObjectOptionsProvider }        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import GalleryFactory                           from '@/componentsGui/imageGallery/factory/GalleryFactory';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';
import GalleryUtilities                         from '@/utilities/gallery/GalleryUtilities';

export default class ImagePickerModel {

    @Property(EnumDocumentSection.model, 'Selected Image Details' )
    @PropertyType('GalleryImageModel')
    @PropertyObjectOptionsProvider('name')
    public image: GalleryImageModel = GalleryImageModel.None;

    @PropertyEvent('An image was selected')
    @PropertyEventValue('GalleryImageModel')
    public selectedNewImage!: string;

    @Property(EnumDocumentSection.model, 'Array of available galleries for the picker' )
    @PropertyType('Array<GalleryModel>')
    public galleries!: Array<GalleryModel>;

    constructor() {
        this.image =  GalleryImageModel.None;
        this.galleries =    [
                                GalleryFactory.getAnimalGallery(),
                                GalleryFactory.getArtGallery(),
                                GalleryFactory.getBusinessGallery(),
                            ];
    }

    public dataForField(fieldName: string): Array<any> {

        if ( fieldName === 'image') {
            return GalleryUtilities.GetAllImagesFromGalleries(this.galleries);
        }

        return [];
    }
}
