import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import ImagePickerComponent                     from '@/componentsGui/imagePickerComponent/ImagePickerComponent';
import ImagePickerModel                         from '../models/ImagePickerModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        TestHarnessComponent,
        ImagePickerComponent,
    },
  })
export default class ImagePickerPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new ImagePickerModel();

    private get testHarness(): TestHarnessComponent {
        const ref = this.$refs.harness as TestHarnessComponent;
        return ref;
    }

    public onSelectedNewImage(image: GalleryImageModel) {
        this.parameters.image = image;
        this.testHarness.auditEvent('selectedNewImage', JSON.stringify(image));
    }

    public data(): any {
        return {};
    }

}
