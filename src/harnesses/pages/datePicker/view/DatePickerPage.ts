import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import DatePickerComponent                      from '@/componentsGui/datePickerComponent/DatePickerComponent';
import moment                                   from 'moment';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        DatePickerComponent,
        TestHarnessComponent,
    },
  })
export default class DatePickerPage extends Vue {

    public sampleDate: moment.Moment = moment().startOf('day');
    public ComponentIconConstants = ComponentIconConstants;

    private get dateFormatted(): string {
        const native = moment(this.sampleDate);
        if ( native.isValid() ) {
            return native.format('DD-MMM-YYYY');
        }
        return '';
    }

    public data(): any {
        return {};
    }

}
