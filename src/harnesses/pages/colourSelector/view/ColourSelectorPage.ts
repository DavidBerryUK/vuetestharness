import ColourSelectorComponent                  from '@/componentsGui/coloursComponents/colourSelectorComponent/ColourSelectorComponent';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        ColourSelectorComponent,
        TestHarnessComponent,
    },
  })
export default class ColourSelectorPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    public data(): any {
        return {};
    }

}
