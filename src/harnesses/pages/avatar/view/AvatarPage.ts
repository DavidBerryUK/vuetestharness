import AvatarComponent                          from '@/componentsGui/avatarComponent/AvatarComponent';
import AvatarModel                              from '../models/AvatarModel';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        TestHarnessComponent,
        AvatarComponent,
    },
  })
export default class AvatarPage extends Vue {


    public ComponentIconConstants = ComponentIconConstants;
    public parameters: AvatarModel = new AvatarModel();

    public data(): any {
        return {};
    }

}
