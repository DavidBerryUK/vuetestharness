import { BindImageSelectorUrlToField }          from '@/harnessFramework/decorators/PropertyDecorator';
import { EnumAvatarBorderStyle }                from '@/componentsGui/avatarComponent/enums/AvatarEnums';
import { enumBadgeAlignment }                   from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { enumBadgePosition }                    from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumGallerySelection }                 from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumImageId }                          from '@/componentsGui/imageGallery/constants/GalleryEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyImageSelector }                from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyRange }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyValueOptions }                 from '@/harnessFramework/decorators/PropertyDecorator';
import GalleryArrayHelpers                      from '@/componentsGui/imageGallery/helpers/GalleryArrayHelpers';
import GalleryFactory                           from '@/componentsGui/imageGallery/factory/GalleryFactory';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import { IAvatarModel }                         from '@/componentsGui/avatarComponent/interfaces/IAvatarModel';



export default class AvatarModel implements IAvatarModel {

    @Property(EnumDocumentSection.model, 'Name of the user')
    @PropertyType(EnumVariableType.string)
    public name: string;

    @Property(EnumDocumentSection.model, 'If supplied the text toggles with the user name')
    @PropertyType(EnumVariableType.string)
    public alternativeText: string;



    @Property(EnumDocumentSection.model, 'name of the image file of the avatar')
    @PropertyImageSelector(EnumGallerySelection.Business)
    @BindImageSelectorUrlToField('imageUrl')
    @PropertyType(EnumVariableType.string)
    public imageUrlModel: GalleryImageModel;
    public imageUrl: string;


    @Property(EnumDocumentSection.model, 'Highlights the avatar when selected')
    @PropertyType(EnumVariableType.boolean)
    @PropertyDefaultValue(false)
    public selected: boolean;


    @Property(EnumDocumentSection.model, 'alignment of the badge')
    @PropertyType(EnumVariableType.string)
    @PropertyDefaultValue(EnumAvatarBorderStyle.none)
    @PropertyValueOptions([EnumAvatarBorderStyle.none, EnumAvatarBorderStyle.greenQuick, EnumAvatarBorderStyle.greySlow])
    public borderAnimation: string;


    @Property(EnumDocumentSection.model, 'number of notifications' )
    @PropertyDefaultValue(0)
    @PropertyRange(0, 30)
    public notifications: number;

    @Property(EnumDocumentSection.attributes, 'alignment of the badge')
    @PropertyType('enumBadgeAlignment')
    @PropertyDefaultValue(enumBadgeAlignment.topRight)
    @PropertyValueOptions([ enumBadgeAlignment.topleft, enumBadgeAlignment.topCenter, enumBadgeAlignment.topRight,
                            enumBadgeAlignment.middleLeft, enumBadgeAlignment.middleCenter, enumBadgeAlignment.middleRight,
                            enumBadgeAlignment.bottomLeft, enumBadgeAlignment.bottomCenter, enumBadgeAlignment.bottomRight])
    public badgeAlignment: string;

    @Property(EnumDocumentSection.attributes, 'position of the badge')
    @PropertyType('enumBadgePosition')
    @PropertyDefaultValue(enumBadgePosition.inside)
    @PropertyValueOptions([enumBadgePosition.inside, enumBadgePosition.border, enumBadgePosition.outside])
    public badgePosition: string;


    constructor() {

        this.name = 'Barry';

        const galleries = [GalleryFactory.getBusinessGallery()];
        this.imageUrlModel = GalleryArrayHelpers.getImageById(galleries,  EnumImageId.BusinessPeopleBarryThumbnail);
        this.imageUrl = this.imageUrlModel.localFileName;

        this.alternativeText = '';
        this.badgeAlignment = 'TopRight';
        this.notifications = 4;
        this.badgePosition = 'Border';
        this.selected = false;
        this.borderAnimation = EnumAvatarBorderStyle.none;


    }
}
