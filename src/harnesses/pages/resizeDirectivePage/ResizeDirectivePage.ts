import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';


@Component({
    components: {
        TestHarnessComponent,
    },
  })
export default class ResizeDirectivePage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public elementWidth: number = 1;
    public elementHeight: number = 1;

    private onResize(event: CustomEvent) {
        const detail = event.detail;
        console.log(`***** resize ${detail.width}  ${detail.height}`);
    }

    public data(): any {
        return {};
    }

}
