import { BindImageSelectorUrlToField }          from '@/harnessFramework/decorators/PropertyDecorator';
import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumGallerySelection }                 from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEvent }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEventValue }                   from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyImageSelector }                from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import SampleModel                              from '@/componentsGui/sampleComponent/SampleModel';

export default class SampleFrameworkModel extends SampleModel {

    @Property(EnumDocumentSection.model, 'Sample model string value')
    @PropertyType(EnumVariableType.string)
    public name: string;

    @Property(EnumDocumentSection.model, 'Sample model numeric value')
    @PropertyType(EnumVariableType.number)
    public age: number;

    @Property(EnumDocumentSection.model, 'Sample model boolean value')
    @PropertyType(EnumVariableType.boolean)
    @PropertyDefaultValue(false)
    public eligibleToVote: boolean;

    @Property(EnumDocumentSection.attributes, 'Sample attribute numeric value')
    @PropertyType(EnumVariableType.number)
    public houseNumber: number;

    @Property(EnumDocumentSection.attributes, 'Sample attribute string value')
    @PropertyType(EnumVariableType.string)
    public address: string;

    @Property(EnumDocumentSection.attributes, 'Sample attribute boolean value')
    @PropertyDefaultValue(false)
    @PropertyType(EnumVariableType.boolean)
    public rented: boolean;


    @Property(EnumDocumentSection.model, 'Sample animal image selector')
    @PropertyImageSelector(EnumGallerySelection.Animals)
    @BindImageSelectorUrlToField('favoriteAnimalImageUrl')
    @PropertyType(EnumVariableType.string)
    public favoriteAnimalImageModel: GalleryImageModel;


    @Property(EnumDocumentSection.model, 'Sample paintings image selector')
    @PropertyImageSelector(EnumGallerySelection.Paintings)
    @BindImageSelectorUrlToField('favoritePaintingImageUrl')
    @PropertyType(EnumVariableType.string)
    public favoritePaintingImageModel: GalleryImageModel;

    public favoritePaintingImageUrl: string;

    @Property(EnumDocumentSection.model, 'Sample business image selector')
    @PropertyImageSelector(EnumGallerySelection.Business)
    @BindImageSelectorUrlToField('favoriteStockImageImageUrl')
    @PropertyType(EnumVariableType.string)
    public favoriteStockImageImageModel: GalleryImageModel;


    @Property(EnumDocumentSection.model, 'Sample image selector')
    @PropertyImageSelector(EnumGallerySelection.All)
    @BindImageSelectorUrlToField('favoriteImageImageUrl')
    @PropertyType(EnumVariableType.string)
    public favoriteImageImageModel: GalleryImageModel;



    @PropertyEvent('Sample event sampleEventA')
    @PropertyEventValue('')
    public sampleEventA!: string;

    @PropertyEvent('Sample event sampleEventB')
    @PropertyEventValue('')
    public sampleEventB!: string;

    @PropertyEvent('Sample event sampleEventC')
    @PropertyEventValue('')
    public sampleEventC!: string;

    @PropertyEvent('Sample event sampleEventD')
    @PropertyEventValue('')
    public sampleEventD!: string;

    constructor() {
        super();
        this.age = 21;
        this.name = 'Bill';
        this.eligibleToVote = true;
        this.houseNumber = 4;
        this.address = 'Springfield';
        this.rented = false;
        this.favoriteAnimalImageModel = GalleryImageModel.None;
        this.favoritePaintingImageModel = GalleryImageModel.None;
        this.favoriteStockImageImageModel = GalleryImageModel.None;
        this.favoriteImageImageModel = GalleryImageModel.None;
        this.favoritePaintingImageUrl = '';
    }
}
