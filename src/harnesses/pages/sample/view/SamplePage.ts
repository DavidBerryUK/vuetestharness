import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import EventCollectionModel                     from '@/harnessFramework/models/EventLogCollectionModel';
import SampleComponent                          from '@/componentsGui/sampleComponent/SampleComponent';
import SampleFrameworkModel                     from '../models/SampleFrameworkModel';
import Vue                                      from 'vue';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';

@Component({
    components: {
        SampleComponent,
    },
  })
export default class SamplePage extends Vue {

    private get testHarness(): TestHarnessComponent {
        const ref = this.$refs.harness as TestHarnessComponent;
        return ref;
    }

    public ComponentIconConstants = ComponentIconConstants;

    public parameters = new SampleFrameworkModel();

    public events: EventCollectionModel = new EventCollectionModel();


    public sampleEventA() {
        this.testHarness.auditEvent('sampleEventA', '');
    }

    public sampleEventB() {
        this.testHarness.auditEvent('sampleEventB', '');
    }

    public sampleEventC() {
        this.testHarness.auditEvent('sampleEventC', '');
    }

    public sampleEventD() {
        this.testHarness.auditEvent('sampleEventD', '');
    }

    public data(): any {
        return {};
    }
}
