import { EnumAvatarBorderStyle }                        from '@/componentsGui/avatarComponent/enums/AvatarEnums';
import { IAvatarListItemModel }                         from '@/componentsGui/avatarListComponent/interfaces/IAvatarListItemModel';
import AvatarListItemModel                              from '../models/AvatarListItemModel';

export default class AvatarListFactory {

    public static getAllAvatars(): Array<IAvatarListItemModel> {
        const list: Array<AvatarListItemModel> = [
                    new AvatarListItemModel(1, 'Ann', '', 'images//business//people/ann-tn.png'),
                    new AvatarListItemModel(2, 'Barry', '', 'images//business//people/barry-tn.png'),
                    new AvatarListItemModel(3, 'Carry', '', 'images//business//people/carry-tn.png'),
                    new AvatarListItemModel(4, 'Dave', '', 'images//business//people/dave-tn.png'),
                    new AvatarListItemModel(5, 'Ethan', '', 'images//business//people/ethan-tn.png'),
                    new AvatarListItemModel(6, 'Fiona', '', 'images//business//people/fiona-tn.png'),
                    new AvatarListItemModel(7, 'Gary', '', 'images//business//people/gary-tn.png'),
        ];

        list[0].visible = true;
        list[0].selected = true;
        list[0].borderAnimation = EnumAvatarBorderStyle.greenQuick;

        list[3].visible = true;
        list[3].notifications = 5;
        list[3].borderAnimation = EnumAvatarBorderStyle.none;

        list[5].visible = true;
        list[5].borderAnimation = EnumAvatarBorderStyle.greySlow;

        return list;
    }
}
