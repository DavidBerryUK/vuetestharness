import { BindImageSelectorUrlToField }          from '@/harnessFramework/decorators/PropertyDecorator';
import { EnumAvatarBorderStyle }                from '@/componentsGui/avatarComponent/enums/AvatarEnums';
import { enumBadgeAlignment }                   from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { enumBadgePosition }                    from '@/componentsGui/badgeComponent/enums/BadgeEnums';
import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumGallerySelection }                 from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumImageId }                          from '@/componentsGui/imageGallery/constants/GalleryEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import { EnumListOrientation }                  from '@/componentsGui/avatarListComponent/enums/AvatarListEnums';
import { IAvatarListItemModel }                 from '@/componentsGui/avatarListComponent/interfaces/IAvatarListItemModel';
import { PropertyValueOptions }                 from '@/harnessFramework/decorators/PropertyDecorator';
import AvatarListFactory                        from '../factories/AvatarListFactory';



export default class AvatarListModel {

    @Property(EnumDocumentSection.model, 'List of avatars to display')
    @PropertyType('Array<IAvatarListItemModel>')
    public avatars: Array<IAvatarListItemModel>;


    @Property(EnumDocumentSection.attributes, 'Orientation of the list')
    @PropertyType('EnumListOrientation')
    @PropertyDefaultValue(EnumListOrientation.landscape)
    @PropertyValueOptions([EnumListOrientation.landscape, EnumListOrientation.portrait])
    public orientation: EnumListOrientation;

    constructor() {
        this.orientation = EnumListOrientation.portrait;
        this.avatars = AvatarListFactory.getAllAvatars();
    }
}
