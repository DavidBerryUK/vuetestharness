import { EnumAvatarBorderStyle }                from '@/componentsGui/avatarComponent/enums/AvatarEnums';
import { IAvatarListItemModel }                 from '@/componentsGui/avatarListComponent/interfaces/IAvatarListItemModel';
import { IAvatarModel }                         from '@/componentsGui/avatarComponent/interfaces/IAvatarModel';

export default class AvatarListItemModel implements IAvatarModel, IAvatarListItemModel {

    public id: number;
    public name: string;
    public alternativeText: string;
    public imageUrl: string;
    public selected: boolean;
    public visible: boolean;
    public notifications: number;
    public borderAnimation: string;

    constructor(id: number, name: string, alternativeText: string, imageUrl: string) {
        this.id = id;
        this.name = name;
        this.alternativeText = alternativeText;
        this.imageUrl = imageUrl;
        this.selected = false;
        this.visible = false;
        this.borderAnimation = EnumAvatarBorderStyle.none;
        this.notifications = 0;
    }
}
