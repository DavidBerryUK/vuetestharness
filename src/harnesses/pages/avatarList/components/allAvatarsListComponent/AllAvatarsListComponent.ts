import { EnumAvatarBorderStyle }                from '@/componentsGui/avatarComponent/enums/AvatarEnums';
import { IAvatarListItemModel }                 from '@/componentsGui/avatarListComponent/interfaces/IAvatarListItemModel';
import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';

@Component
export default class AllAvatarsListComponent extends Vue {

    @Prop()
    public avatarList!: Array<IAvatarListItemModel>;

    private borderAnimationOptions = [EnumAvatarBorderStyle.none,
                                        EnumAvatarBorderStyle.greenQuick,
                                        EnumAvatarBorderStyle.greySlow];

    private data(): any  {
        return {};
    }
}

Vue.component('db-all-avatars-list', AllAvatarsListComponent);
