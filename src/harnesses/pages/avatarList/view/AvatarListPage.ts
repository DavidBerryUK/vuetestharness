import AvatarListComponent                      from '@/componentsGui/avatarListComponent/AvatarListComponent';
import AvatarListFactory                        from '../factories/AvatarListFactory';
import AvatarListItemModel                      from '../models/AvatarListItemModel';
import AvatarListModel                          from '../models/AvatarListModel';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';
import AllAvatarsListComponent                  from '../components/allAvatarsListComponent/AllAvatarsListComponent';

@Component({
    components: {
        TestHarnessComponent,
        AvatarListComponent,
        AllAvatarsListComponent,
    },
  })
export default class AvatarListPage extends Vue {


    public ComponentIconConstants = ComponentIconConstants;
    public parameters: AvatarListModel = new AvatarListModel();

    public data(): any {
        return {};
    }

}
