import AxisRulerTimeComponent                   from '@/componentsGui/timeLineComponents/views/axisRulerTimeComponent/views/AxisRulerTimeComponent';
import AxisRulerTimeModel                       from '../models/AxisRulerTimeModel';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import TimelineGestureRecognizerComponent       from '@/componentsGui/timeLineComponents/views/timelineGestureRecognizerComponent/TimelineGestureRecognizerComponent';
import Vue                                      from 'vue';


@Component({
    components: {
        AxisRulerTimeComponent,
        TestHarnessComponent,
        TimelineGestureRecognizerComponent,
    },
  })
export default class AxisTimePage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new AxisRulerTimeModel();

    public data(): any {
        return {};
    }
}
