import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { ITimeLineEnvironmentModel }            from '@/componentsGui/timeLineComponents/interfaces/ITimeLineEnvironmentModel';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyRange }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyValueOptions }                 from '@/harnessFramework/decorators/PropertyDecorator';
import moment                                   from 'moment';

export default class AxisRulerTimeModel implements ITimeLineEnvironmentModel {

    @Property(EnumDocumentSection.attributes, 'Start time of the axis')
    @PropertyType('Moment (MomentJs)')
    public startTime: moment.Moment;

    @Property(EnumDocumentSection.attributes, 'Presentation  scale of the axis')
    @PropertyDefaultValue(EnumPresentationVisualScale.fullDay)
    @PropertyValueOptions([ EnumPresentationVisualScale.quarterHour,
                            EnumPresentationVisualScale.halfHour,
                            EnumPresentationVisualScale.hour,
                            EnumPresentationVisualScale.halfDay,
                            EnumPresentationVisualScale.fullDay,
                            EnumPresentationVisualScale.fiveDays,
                            EnumPresentationVisualScale.week,
                            EnumPresentationVisualScale.twoWeeks,
                            EnumPresentationVisualScale.fourWeeks])
    public presentationScale: EnumPresentationVisualScale;

    @Property(EnumDocumentSection.attributes, 'Offset in pixels of the current view. (allows from horizontal scrolling)')
    @PropertyRange(0, 10000)
    public pixelOffsetX: number;

    @Property(EnumDocumentSection.attributes, 'start offset in days (harness parameter only)')
    @PropertyRange(0, 60)
    public startOffsetDays: number;

    @Property(EnumDocumentSection.attributes, 'start offset in hours (harness parameter only)')
    @PropertyRange(0, 60)
    public startOffsetHours: number;


    public get startDateTime(): moment.Moment {
        let date = this.startTime.clone();
        date = date.add(this.startOffsetDays, 'days');
        date = date.add(this.startOffsetHours, 'hours');
        return date;
    }

    constructor() {
        this.startTime = moment().startOf('day');
        this.presentationScale = EnumPresentationVisualScale.week;
        this.pixelOffsetX = 0;
        this.startOffsetDays = 0;
        this.startOffsetHours = 0;
    }
}
