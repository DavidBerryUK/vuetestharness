import Component                                from 'vue-class-component';
import LoaderComponent                          from '@/componentsGui/loaderComponent/LoaderComponent';
import LoaderModel                              from '../models/LoaderModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';

@Component({
    components: {
        TestHarnessComponent,
        LoaderComponent,
    },
  })
export default class LoaderPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new LoaderModel('default');

    public data(): any {
        return {};
    }

}
