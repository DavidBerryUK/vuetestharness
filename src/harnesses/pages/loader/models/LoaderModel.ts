import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { PropertyDefaultValue }              from '@/harnessFramework/decorators/PropertyDecorator';
import { Property }                  from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                      from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyValueOptions }              from '@/harnessFramework/decorators/PropertyDecorator';

export default class LoaderModel {


    @Property(EnumDocumentSection.attributes, 'size of the loader')
    @PropertyDefaultValue('default')
    @PropertyType(EnumVariableType.string)
    @PropertyValueOptions(['default', 'avatar', 'small', 'medium', 'large'])
    public loaderSize: string;

    constructor(loaderSize: string) {
        this.loaderSize = loaderSize;
    }
}
