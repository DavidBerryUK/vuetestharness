import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import ScheduleGridModel                        from '../models/ScheduleGridModel';
import ScheduleProgramGridComponent             from '@/componentsGui/timeLineComponents/views/scheduleProgramGridComponent/ScheduleProgramGridComponent';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        ScheduleProgramGridComponent,
        TestHarnessComponent,
    },
  })
export default class ScheduleProgramGridPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    public parameters = new ScheduleGridModel();


    public data(): any {
        return {};
    }
}
