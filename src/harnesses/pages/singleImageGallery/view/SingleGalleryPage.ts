import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GallerySectionModel                      from '@/componentsGui/imageGallery/models/GallerySectionModel';
import SingleGalleryComponent                   from '@/componentsGui/imageGallery/singleGalleryComponent/SingleGalleryComponent';
import SingleGalleryModel                       from '../models/SingleGalleryModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';
import { Watch }                                from 'vue-property-decorator';

@Component({
    components: {
        SingleGalleryComponent,
    },
  })
export default class SingleGalleryPage extends Vue {

    // note, changing the key of an element forces a complete reload.
    private versionNo: number = 0;

    private get testHarness(): TestHarnessComponent {
        const ref = this.$refs.harness as TestHarnessComponent;
        return ref;
    }

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new SingleGalleryModel();
    public parametersOldValue: SingleGalleryModel = this.parameters.clone();

    public selectedNewImage(image: GalleryImageModel) {
        this.parameters.selectedImage = image;
        this.testHarness.auditEvent('selectedNewImage', JSON.stringify(image));
    }

    public selectedNewSection(section: GallerySectionModel) {
        this.testHarness.auditEvent('selectedNewSection', JSON.stringify(section));
    }

    @Watch('parameters', {deep: true})
    private onParametersChanged(value: SingleGalleryModel, oldValue: SingleGalleryModel) {
        if ( this.parametersOldValue.autoOpenOnLoad !== this.parameters.autoOpenOnLoad) {
            this.autoOpenOnLoadValueChanged();
            this.parametersOldValue.autoOpenOnLoad = this.parameters.autoOpenOnLoad;
        }
    }

    private autoOpenOnLoadValueChanged() {
        console.log('autoOpenOnLoad ' + this.parameters.autoOpenOnLoad);
        this.versionNo++;
    }

    public data(): any {
        return {};
    }

}
