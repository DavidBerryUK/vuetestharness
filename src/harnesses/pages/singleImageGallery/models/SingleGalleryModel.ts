import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { ITestHarnessObjectArrayDataProvider }  from '@/harnessFramework/decorators/ITestHarnessObjectArrayDataProvider';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEvent }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEventValue }                   from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyObjectOptionsProvider }        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import GalleryFactory                           from '@/componentsGui/imageGallery/factory/GalleryFactory';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';

export default class SingleGalleryModel implements ITestHarnessObjectArrayDataProvider {

    @Property(EnumDocumentSection.attributes, 'gallery model containing sections and images ')
    @PropertyObjectOptionsProvider('name')
    @PropertyType('GalleryModel')
    public gallery: GalleryModel;


    @Property(EnumDocumentSection.attributes, 'selected image on load')
    @PropertyObjectOptionsProvider('name')
    @PropertyType('GalleryImageModel')
    public selectedImage: GalleryImageModel;

    @Property(EnumDocumentSection.attributes, 'Automatically show selected image')
    @PropertyDefaultValue(false)
    @PropertyType(EnumVariableType.boolean)
    public autoShowSelectedImage: boolean;


    @Property(EnumDocumentSection.attributes,
        'Automatically open gallery on load, either currently selected image, or the first segment.<br> Changing this value will reload the component in the test harness')
    @PropertyDefaultValue(false)
    @PropertyType(EnumVariableType.boolean)
    public autoOpenOnLoad: boolean;


    @PropertyEvent('Event raised when the user chooses a new image')
    @PropertyEventValue('GalleryImageModel')
    public selectedNewImage!: string;


    @PropertyEvent('Event raised when user chooses a new section of images')
    @PropertyEventValue('GallerySectionModel')
    public selectedNewSection!: string;


    constructor() {

        this.selectedImage = GalleryImageModel.None;
        this.gallery = GalleryFactory.getAnimalGallery();
        this.autoShowSelectedImage = false;
        this.autoOpenOnLoad = false;
    }

    /**
     * create a copy of the object
     *
     * @returns {GalleryImageModel}
     * @memberof GalleryImageModel
     */
    public clone(): SingleGalleryModel {
        return Object.assign(new SingleGalleryModel(), this);
    }

    public dataForField(fieldName: string): Array<any> {

        if ( fieldName === 'gallery' ) {
            const galleries =
            [
                GalleryFactory.getAnimalGallery(),
                GalleryFactory.getArtGallery(),
                GalleryFactory.getBusinessGallery(),
            ];

            return galleries;
        }

        if ( fieldName === 'selectedImage') {
            if (this.gallery) {
                return this.gallery.allImages;
            }
            return new Array<GalleryImageModel>();
        }

        return [];
    }
}
