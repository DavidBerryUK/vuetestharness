import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import TimeLineSummaryModel                     from '../../models/TimeLineSummaryModel';
import ProgramEventsService                     from '@/componentsGui/timeLineComponents/scheduler/ProgramEventsService';
import Vue                                      from 'vue';

@Component
export default class TimeLineDemoComponent extends Vue {

    @Prop()
    public value!: TimeLineSummaryModel;

    private onGenerateTestDate() {
        ProgramEventsService.createEventsForPrograms(this.value.programs, this.value.overviewDateFrom, this.value.overviewDateTo);
    }


    private data(): any  {
        return {};
    }
}

Vue.component('db-time-line-demo', TimeLineDemoComponent);
