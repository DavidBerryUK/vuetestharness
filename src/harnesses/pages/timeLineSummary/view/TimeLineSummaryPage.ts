import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import ProgramEventsFactory                     from '@/harnesses/testData/schedules/ProgramEventsFactory';
import ProgramEventsService                     from '@/componentsGui/timeLineComponents/scheduler/ProgramEventsService';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import TimeLineDemoComponent                    from '../components/timeLineDemoComponent/TimeLineDemoComponent';
import TimeLineSummaryComponent                 from '@/componentsGui/timeLineComponents/views/timeLineSummaryComponent/views/TimeLineSummaryComponent';
import TimeLineSummaryModel                     from '../models/TimeLineSummaryModel';
import Vue                                      from 'vue';


@Component({
    components: {
        TestHarnessComponent,
        TimeLineDemoComponent,
        TimeLineSummaryComponent,
    },
  })
export default class TimeLineSummaryPage extends Vue {

    public parameters = new TimeLineSummaryModel();
    public ComponentIconConstants = ComponentIconConstants;

    public testDateCreated = false;

    private get testHarness(): TestHarnessComponent {
        const ref = this.$refs.harness as TestHarnessComponent;
        return ref;
    }

    private mounted() {

        if (this.testDateCreated === false) {
            //
            // time out ensure this is only run after the page has loaded,
            // setTimeout to put this into event queue
            //
            setTimeout(() => {
                this.testDateCreated = true;
                // console.log('begin creating events');
                ProgramEventsService.createEventsForPrograms(this.parameters.programs, this.parameters.overviewDateFrom, this.parameters.overviewDateTo);
                ProgramEventsFactory.createHistoricEventsForPrograms(this.parameters.programs);
                // console.log('finished creating events');
                // console.log(this.parameters.programs);

                // this.parameters.programs.forEach((program: ProgramModel) => {
                //     console.log(program.events);
                // });

              }, 0);
        }
    }



    private onRendered() {
        this.testHarness.auditEvent('rendered', '');
    }
    public data(): any {
        return {};
    }

}
