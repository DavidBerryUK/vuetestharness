import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { EnumSummaryTimeLineSummaryStyle }      from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { EnumVariableType }                     from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyDefaultValue }                 from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEvent }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyEventValue }                   from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyRange }                        from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyValueOptions }                 from '@/harnessFramework/decorators/PropertyDecorator';
import moment                                   from 'moment';
import ProgramFactory                           from '@/harnesses/testData/schedules/ProgramFactory';
import ProgramModel                             from '@/componentsGui/timeLineComponents/models/ProgramModel';


export default class TimeLineSummaryModel {

    @Property(EnumDocumentSection.model, 'Period start time')
    @PropertyType(EnumVariableType.date)
    public overviewDateFrom: moment.Moment;

    @Property(EnumDocumentSection.model, 'Period end time')
    @PropertyType(EnumVariableType.date)
    public overviewDateTo: moment.Moment;


    @Property(EnumDocumentSection.model, 'Begin of the selected date range')
    @PropertyType(EnumVariableType.date)
    public selectedDate: moment.Moment;


    @Property(EnumDocumentSection.model, 'Radius of time line nodes')
    @PropertyRange(1, 30)
    public nodeRadius!: number;

    @Property(EnumDocumentSection.model, 'Width of line graphic')
    @PropertyRange(1, 20)
    public lineWidth!: number;

    @Property(EnumDocumentSection.model, 'programs in schedule')
    @PropertyType('Array<ProgramModel>')
    public programs: Array<ProgramModel> = new Array<ProgramModel>();

    @Property(EnumDocumentSection.model, 'Presentation  scale of the axis')
    @PropertyDefaultValue(EnumPresentationVisualScale.quarterHour)
    @PropertyValueOptions([ EnumPresentationVisualScale.quarterHour,
                            EnumPresentationVisualScale.halfHour,
                            EnumPresentationVisualScale.hour,
                            EnumPresentationVisualScale.halfDay,
                            EnumPresentationVisualScale.fullDay,
                            EnumPresentationVisualScale.fiveDays,
                            EnumPresentationVisualScale.week,
                            EnumPresentationVisualScale.twoWeeks,
                            EnumPresentationVisualScale.fourWeeks])
    public presentationScale: EnumPresentationVisualScale;


    @Property(EnumDocumentSection.model, 'Presentation style of the overview data')
    @PropertyDefaultValue(EnumSummaryTimeLineSummaryStyle.none)
    @PropertyValueOptions([ EnumSummaryTimeLineSummaryStyle.none,
                            EnumSummaryTimeLineSummaryStyle.lineGraph,
                            EnumSummaryTimeLineSummaryStyle.pieChartPerMonth,
                            EnumSummaryTimeLineSummaryStyle.summaryPerMonth])
    public timeLineSummaryStyle: EnumSummaryTimeLineSummaryStyle = EnumSummaryTimeLineSummaryStyle.lineGraph;


    @PropertyEvent('Event raised when the time line renders')
    @PropertyEventValue('')
    public rendered!: string;


    constructor() {
        this.presentationScale = EnumPresentationVisualScale.week;
        this.nodeRadius = 10;
        this.lineWidth = 2;

        const now = moment();

        this.overviewDateFrom = now.clone().startOf('month').add(-5, 'month');
        this.overviewDateTo = now.clone().startOf('month').add(1, 'month').endOf('month');
        this.selectedDate = now.clone();
        this.programs = ProgramFactory.demoPrograms;



    }
}
