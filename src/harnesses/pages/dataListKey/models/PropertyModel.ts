import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyType }                         from '@/harnessFramework/decorators/PropertyDecorator';
import DataKeyItemModel                         from '@/componentsGui/dataKeyListComponent/models/DataKeyItemModel';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';


export default class PropertyModel {

    @Property(EnumDocumentSection.model, 'List of key names')
    @PropertyType('Array<DataKeyItemModel>')
    public value: Array<DataKeyItemModel> = new Array<DataKeyItemModel>();


    constructor() {
        this.value = new Array<DataKeyItemModel>();
        this.value.push( new DataKeyItemModel('Canterbury', WebNamedColourConstants.Orange));
        this.value.push( new DataKeyItemModel('Glasgow', WebNamedColourConstants.Purple));
        this.value.push( new DataKeyItemModel('London', WebNamedColourConstants.Salmon));
        this.value.push( new DataKeyItemModel('Sheffield', WebNamedColourConstants.SeaGreen));
        this.value.push( new DataKeyItemModel('Stoke-on-Trent', WebNamedColourConstants.RosyBrown));
        this.value.push( new DataKeyItemModel('Wakefield', WebNamedColourConstants.SteelBlue));
    }
}
