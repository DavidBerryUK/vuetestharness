import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import DataKeyListComponent                     from '@/componentsGui/dataKeyListComponent/views/DataKeyListComponent';
import PropertyModel                            from '../models/PropertyModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        DataKeyListComponent,
        TestHarnessComponent,
    },
  })
export default class DataListKeyPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new PropertyModel();

    public data(): any {
        return {};
    }

}
