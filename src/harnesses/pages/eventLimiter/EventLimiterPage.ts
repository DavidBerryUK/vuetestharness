import { debounce }                             from 'throttle-debounce';
import { throttle }                             from 'throttle-debounce';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        TestHarnessComponent,
    },
  })
export default class EventLimiterPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;
    private pingDebounce: number = 0;
    private pingThrottle: number = 0;
    private delayDebounce: number = 500;
    private delayThrottle: number = 500;
    private debounceFunction: ((delta: number) => void) | undefined = undefined;
    private throttleFunction: ((delta: number) => void) | undefined = undefined;

    private mounted() {
        this.setupLimitedFunctions();
    }

    @Watch('delayDebounce')
    private onDelayDebounceChanged(value: number, oldValue: number) {
        this.setupLimitedFunctions();
    }

    @Watch('delayThrottle')
    private onDelayThrottleChanged(value: number, oldValue: number) {
        this.setupLimitedFunctions();
    }

    public onPingDebounceClicked() {
        if ( this.debounceFunction !== undefined) {
            this.debounceFunction(1);
        }
    }

    public onPingThrottleClicked() {
        if ( this.throttleFunction !== undefined ) {
            this.throttleFunction(1);
        }
    }

    private setupLimitedFunctions() {

        if (this.debounceFunction !== undefined) {
            // type definition file does not fully support the cancel command,
            (this.debounceFunction as any).cancel();
        }

        if (this.throttleFunction !== undefined) {
            // type definition file does not fully support the cancel command,
            (this.throttleFunction as any).cancel();
        }

        this.debounceFunction = debounce(this.delayDebounce, (delta: number) => {
            this.pingDebounce = this.pingDebounce + delta;
        });

        this.throttleFunction = throttle(this.delayThrottle, (delta: number) => {
            this.pingThrottle = this.pingThrottle + delta;
        });
    }

    public data(): any {
        return {};
    }

}
