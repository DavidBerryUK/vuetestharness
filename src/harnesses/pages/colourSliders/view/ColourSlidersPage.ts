import ColourSlidersComponent                   from '@/componentsGui/coloursComponents/colourSlidersComponent/ColourSlidersComponent';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        ColourSlidersComponent,
        TestHarnessComponent,
    },
  })
export default class ColourSlidersPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    public data(): any {
        return {};
    }

}
