import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import NumberSpinnerComponent                   from '@/componentsGui/numberSpinnerComponent/NumberSpinnerComponent';
import NumberSpinnerModel                       from '../models/NumberSpinnerModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        NumberSpinnerComponent,
        TestHarnessComponent,
    },
  })
export default class NumberSpinnerPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    public parameters = new NumberSpinnerModel();

    public data(): any {
        return {};
    }

}
