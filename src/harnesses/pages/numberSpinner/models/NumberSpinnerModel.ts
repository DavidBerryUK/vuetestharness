import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { PropertyRange }                        from '@/harnessFramework/decorators/PropertyDecorator';


export default class NumberSpinnerModel {

    @Property(EnumDocumentSection.model, 'value of the number' )
    @PropertyRange(0, 255)
    public value: number;

    @Property(EnumDocumentSection.attributes, 'Minimum number allowed' )
    @PropertyRange(0, 255)
    public min: number;

    @Property(EnumDocumentSection.attributes, 'Maximum number allowed' )
    @PropertyRange(0, 255)
    public max: number;

    constructor() {
        this.value = 128;
        this.min = 0;
        this.max = 255;
    }
}
