import Component                                from 'vue-class-component';
import Vue                                      from 'vue';
import { SimpleChartDataFactory } from '@/harnesses/testData/simpleChart/SimpleChartDataFactory';

@Component
export default class SimpleChartDemoComponent extends Vue {

    private on4RandomLinesClicked() {
        const model = SimpleChartDataFactory.CreateFourDatasetRandomLines();
        this.$emit('NewChartModel', model);
    }

    private onSineWaveClicked() {
        const model = SimpleChartDataFactory.CreateOneDatasetSineWave();
        this.$emit('NewChartModel', model);
    }

    private on4SegmentsClicked() {
        const model = SimpleChartDataFactory.CreateFourSegmentPieChart();
        this.$emit('NewChartModel', model);
    }

    private data(): any  {
        return {};
    }
}

Vue.component('db-simple-chart-demo-controls', SimpleChartDemoComponent);
