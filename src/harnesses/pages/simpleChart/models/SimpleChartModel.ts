import { EnumDocumentSection }                  from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Property, PropertyType }                             from '@/harnessFramework/decorators/PropertyDecorator';
import { SimpleChartDataFactory }               from '@/harnesses/testData/simpleChart/SimpleChartDataFactory';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';

export default class SimpleChartModel {


    @Property(EnumDocumentSection.model, 'ChartModel')
    @PropertyType('ChartModel')
    public chartModel: ChartModel = SimpleChartDataFactory.CreateOneDatasetSineWave();


}
