import { EnumTab }                              from '@/harnessFramework/enums/HarnessFrameworkEnums';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import SimpleChartComponent                     from '@/componentsGui/simpleChartComponent/views/SimpleChartComponent';
import SimpleChartDemoComponent                 from '../components/simpleChartDemoComponent/SimpleChartDemoComponent';
import SimpleChartModel                         from '../models/SimpleChartModel';
import TestHarnessComponent                     from '@/harnessFramework/components/testHarness/TestHarnessComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        TestHarnessComponent,
        SimpleChartComponent,
        SimpleChartDemoComponent,
    },
  })
export default class SimpleChartPage extends Vue {

    private EnumTab = EnumTab;

    public ComponentIconConstants = ComponentIconConstants;
    public parameters = new SimpleChartModel();


    private onNewChartModel(model: ChartModel) {
        this.parameters.chartModel = model;
        const simpleChart = this.$refs.simpleChart as SimpleChartComponent;
        simpleChart.redraw();
    }

    public data(): any {
        return {};
    }

}
