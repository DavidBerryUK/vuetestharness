import Vue                                      from 'vue';

export default class SubscriberRegistryItem<T> {
    public subscriber: Vue;
    public callback: T;

    constructor(subscriber: Vue, callback: T) {
        this.subscriber = subscriber;
        this.callback = callback;
    }

}
