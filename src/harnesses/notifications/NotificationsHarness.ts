import SubscriberRegistryItem                   from './SubscriberRegistryItem';
import Vue                                      from 'vue';

type IMessageWithModel<T> = (model: T) => void;
type IMessageWithBoolean = (bool: boolean) => void;


/**
 * Example of how to send messages between components, this will eventually changed to use
 * VueX when it supports strongly Typed Objects
 *
 * @export
 * @class NotificationsHarness
 */
export default class NotificationsHarness {

    private static subscriberToolbarVisibility: Array<SubscriberRegistryItem<IMessageWithBoolean>> = new Array<SubscriberRegistryItem<IMessageWithBoolean>>();
    private static subscriberHeaderVisibility: Array<SubscriberRegistryItem<IMessageWithBoolean>> = new Array<SubscriberRegistryItem<IMessageWithBoolean>>();

    public static unsubscribe(subscriber: Vue ) {
        this.subscriberToolbarVisibility = this.subscriberToolbarVisibility.filter((item: SubscriberRegistryItem<IMessageWithBoolean>) => item.subscriber !== subscriber);
        this.subscriberHeaderVisibility = this.subscriberHeaderVisibility.filter((item: SubscriberRegistryItem<IMessageWithBoolean>) => item.subscriber !== subscriber);
    }

    public onToolbarVisibilityUpdated(subscriber: Vue, callback: IMessageWithBoolean): NotificationsHarness {
        const registryEntry = new SubscriberRegistryItem<IMessageWithBoolean>(subscriber, callback);
        NotificationsHarness.subscriberToolbarVisibility.push(registryEntry);
        return this;
    }

    public onHeaderVisibilityUpdated(subscriber: Vue, callback: IMessageWithBoolean): NotificationsHarness {
        const registryEntry = new SubscriberRegistryItem<IMessageWithBoolean>(subscriber, callback);
        NotificationsHarness.subscriberHeaderVisibility.push(registryEntry);
        return this;
    }

    public static publishHeaderHide() {
        this.publishHeaderSetVisibility(false);
    }

    public static publishHeaderShow() {
        this.publishHeaderSetVisibility(true);
    }

    public static publishToolbarHide() {
        this.publishToolbarSetVisibility(false);
    }

    public static publishToolbarShow() {
        this.publishToolbarSetVisibility(true);
    }

    public static publishHeaderSetVisibility(isVisible: boolean) {
       this.subscriberHeaderVisibility.forEach((subscriber) => { subscriber.callback(isVisible); });
    }

    public static publishToolbarSetVisibility(isVisible: boolean) {
        this.subscriberToolbarVisibility.forEach((subscriber) => { subscriber.callback(isVisible); });
     }
}
