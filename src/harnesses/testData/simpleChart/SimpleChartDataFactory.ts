import { EnumChartType }                        from '@/componentsGui/simpleChartComponent/enums/SimpleChartEnums';
import ChartDataPoint                           from '@/componentsGui/simpleChartComponent/models/ChartDataPoint';
import ChartDataSet                             from '@/componentsGui/simpleChartComponent/models/ChartDataSet';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';
import ColourModel                              from '@/componentsGui/coloursComponents/models/ColourModel';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

export class SimpleChartDataFactory {

    public static CreateFourSegmentPieChart(): ChartModel {

        const chartModel = new ChartModel(EnumChartType.PieChart);

        const dataSet = new ChartDataSet('Pie Chart');
        dataSet.dataPoints.push( new ChartDataPoint(30 , 0, 'Success', WebNamedColourConstants.Green));
        dataSet.dataPoints.push( new ChartDataPoint(30  , 0, 'Error', WebNamedColourConstants.Red));
        dataSet.dataPoints.push( new ChartDataPoint(30 , 0, 'Warning', WebNamedColourConstants.Orange));
        dataSet.dataPoints.push( new ChartDataPoint(30 , 0, 'Pending', WebNamedColourConstants.BlueViolet));

        chartModel.dataSets.push(dataSet);

        return chartModel;
    }


    public static CreateFourDatasetRandomLines(): ChartModel {

        const chartModel = new ChartModel(EnumChartType.LineGraph);

        chartModel.dataSets.push(this.sampleSuccessDataSet());
        chartModel.dataSets.push(this.sampleFailureDataSet());
        chartModel.dataSets.push(this.sampleWarningsDataSet());
        chartModel.dataSets.push(this.samplePendingDataSet());

        return chartModel;
    }

    public static CreateOneDatasetSineWave(): ChartModel {
        const chartModel = new ChartModel(EnumChartType.LineGraph);
        chartModel.dataSets.push(this.datasetSineWave(0, 360, 80, 1, WebNamedColourConstants.OrangeRed, 2));
        // chartModel.dataSets.push(this.datasetSineWave(90, 270, 45, 8, WebNamedColourConstants.Purple, 18));
        return chartModel;
    }

    private static datasetSineWave(fromX: number, toX: number, amplitude: number, frequency: number, colour: ColourModel, lineWidth: number): ChartDataSet {
        const dataSet = new ChartDataSet('sine', colour);
        dataSet.lineWidth = lineWidth;

        for ( let x = fromX; x < toX; x ++) {
            const y =  Math.sin ( Math.PI / 180 *  x * frequency) * amplitude;
            dataSet.dataPoints.push(new ChartDataPoint(x, y ));
        }

        return dataSet;
    }

    private static sampleSuccessDataSet(): ChartDataSet {
        const dataSet = new ChartDataSet('Success', WebNamedColourConstants.Green);
        dataSet.dataPoints = this.randomDatePoints();
        return dataSet;
    }

    private static sampleFailureDataSet(): ChartDataSet {
        const dataSet = new ChartDataSet('Failure', WebNamedColourConstants.Red);
        dataSet.dataPoints = this.randomDatePoints();
        return dataSet;
    }

    private static sampleWarningsDataSet(): ChartDataSet {
        const dataSet = new ChartDataSet('Warning', WebNamedColourConstants.Orange);
        dataSet.dataPoints = this.randomDatePoints();
        return dataSet;
    }

    private static samplePendingDataSet(): ChartDataSet {
        const dataSet = new ChartDataSet('Pending', WebNamedColourConstants.SteelBlue);
        dataSet.dataPoints = this.randomDatePoints();
        return dataSet;
    }


    /**
     * Create random data points over 100 days
     *
     * @private
     * @static
     * @returns {Array<ChartDataPoint>}
     * @memberof SimpleChartDataFactory
     */
    private static randomDatePoints(): Array<ChartDataPoint> {
        const data = new Array<ChartDataPoint>();

        for (let i = 0; i < 50; i++) {
            const y = Math.floor(Math.random() * 500 - 250) ;
            data.push(new ChartDataPoint(i, y ));
        }

        return data;
    }

}
