import { EnumScheduleEventStatus }              from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import EventModel                               from '@/componentsGui/timeLineComponents/models/EventModel';
import moment                                   from 'moment';
import ProgramModel                             from '@/componentsGui/timeLineComponents/models/ProgramModel';


export default class ProgramEventsFactory {

    public static createHistoricEventsForPrograms(programs: Array<ProgramModel>) {
        programs.forEach((program) => this.createHistoricEventsForProgram(program));
    }

    /**
     * Create fake historic data programs, note that schedules will have
     * to have been created first
     * @static
     * @param {ProgramModel} program
     * @memberof ProgramEventsFactory
     */
    public static createHistoricEventsForProgram(program: ProgramModel) {

        const now = moment();

        program.events.forEach((event: EventModel) => {
            if ( event.startDateTime.isBefore(now)) {
                const result = Math.floor(Math.random() * 100);
                const estimatedSeconds = event.estimatedDuration.asSeconds();
                if ( result > 98) {
                    event.status = EnumScheduleEventStatus.unknown;
                    event.actualDuration = moment.duration(0, 'seconds');
                } else if ( result > 80) {
                    event.status = EnumScheduleEventStatus.failed;
                    event.actualDuration = moment.duration(Math.random() * estimatedSeconds, 'seconds');
                } else if ( result > 60 ) {
                    event.status = EnumScheduleEventStatus.warnings;
                    event.actualDuration = moment.duration(estimatedSeconds / 3 + Math.random() * estimatedSeconds * 2.0, 'seconds');
                } else if ( result > 50 ) {
                    event.status = EnumScheduleEventStatus.pending;
                    event.actualDuration = moment.duration(0, 'seconds');
                } else {

                    event.status = EnumScheduleEventStatus.success;
                    event.actualDuration = moment.duration(estimatedSeconds / 3 + Math.random() * estimatedSeconds * 0.75, 'seconds');
                }
            } else {
                event.status = EnumScheduleEventStatus.pending;
            }
        });
    }
}
