import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import MenuItem                                 from '../models/MenuItem';

export default class MenuFactory {

    public static getMenuItems(): Array<MenuItem> {

        const menuItems: Array<MenuItem> = new Array<MenuItem>();

        menuItems.push(new MenuItem('Home', 'home', `fa-2x ${ComponentIconConstants.HomeIcon}`));
        menuItems.push(new MenuItem('About', 'about', `fa-2x ${ComponentIconConstants.AboutIcon}`));
        menuItems.push(this.avatarMenu());
        menuItems.push(this.chartMenu());
        menuItems.push(this.colourMenu());
        menuItems.push(this.galleryMenu());
        menuItems.push(this.timeLineMenu());
        menuItems.push(this.directiveMenu());
        menuItems.push(this.researchMenu());
        menuItems.push(new MenuItem('Badge', 'badge', `fa-2x ${ComponentIconConstants.BadgeIcon}`));
        menuItems.push(new MenuItem('Date Picker', 'datePicker', `fa-2x ${ComponentIconConstants.DatePickerIcon}`));
        menuItems.push(new MenuItem('Loader', 'loader', `fa-2x ${ComponentIconConstants.LoaderIcon}`));
        menuItems.push(new MenuItem('Message List', 'messageList', `fa-2x ${ComponentIconConstants.MessageListIcon}`));
        menuItems.push(new MenuItem('Number Spinner', 'numberSpinner', `fa-2x ${ComponentIconConstants.NumberSpinnerIcon}`));
        menuItems.push(new MenuItem('Sample', 'sample', `fa-2x ${ComponentIconConstants.SampleIcon}`));


        return menuItems;
    }
    private static chartMenu(): MenuItem {
        const menuItem = new MenuItem('Chart', '', '');
        menuItem.subMenus.push(new MenuItem('Data List Key', 'dataListKey', `fa-2x ${ComponentIconConstants.DataListKeyIcon}`));
        menuItem.subMenus.push(new MenuItem('Simple Chart', 'simpleChart', `fa-2x ${ComponentIconConstants.SimpleChartIcon}`));
        menuItem.active = true;
        return menuItem;
    }

    private static timeLineMenu(): MenuItem {
        const menuItem = new MenuItem('Time Line', '', '');
        menuItem.subMenus.push(new MenuItem('Axis Time', 'axisTime', `fa-2x ${ComponentIconConstants.AxisRulerTimeIcon}`));
        menuItem.subMenus.push(new MenuItem('Grid', 'scheduleGrid', `fa-2x ${ComponentIconConstants.ScheduleGridIcon}`));
        menuItem.subMenus.push(new MenuItem('Grid Program', 'scheduleProgramGrid', `fa-2x ${ComponentIconConstants.ScheduleProgramGridIcon}`));
        menuItem.subMenus.push(new MenuItem('Time Line Summary', 'timeLineSummary', `fa-2x ${ComponentIconConstants.timeLineSummaryIcon}`));
        menuItem.subMenus.push(new MenuItem('Demo', 'schedulerDemo', `fa-2x ${ComponentIconConstants.DemoIcon}`));
        return menuItem;
    }

    private static directiveMenu(): MenuItem {
        const menuItem = new MenuItem('Directives', '', '');
        menuItem.subMenus.push(new MenuItem('Resize', 'resizeDirective', `fa-2x ${ComponentIconConstants.ResizeDirectiveIcon}`));

        // set this folder open by default
       // menuItem.active = true;

        return menuItem;
    }

    private static avatarMenu(): MenuItem {
        const menuItem = new MenuItem('Avatar', '', '');
        menuItem.subMenus.push(new MenuItem('Avatar', 'avatar', `fa-2x ${ComponentIconConstants.AvatarIcon}`));
        menuItem.subMenus.push(new MenuItem('Avatar List', 'avatarList', `fa-2x ${ComponentIconConstants.AvatarListIcon}`));
        return menuItem;
    }

    private static colourMenu(): MenuItem {
        const menuItem = new MenuItem('Colour', '', '');
        menuItem.subMenus.push(new MenuItem('Colour Lens', 'colourPreviewLens', `fa-2x ${ComponentIconConstants.ColourPreviewLensIcon}`));
        menuItem.subMenus.push(new MenuItem('Colour Sliders', 'colourSliders', `fa-2x ${ComponentIconConstants.ColourSlidersIcon}`));
        menuItem.subMenus.push(new MenuItem('Colour Selector', 'colourSelector', `fa-2x ${ComponentIconConstants.ColourSelectorIcon}`));

        return menuItem;
    }

    private static galleryMenu(): MenuItem {

        const menuItem = new MenuItem('Gallery', '', '');
        menuItem.subMenus.push(new MenuItem('Gallery Single ', 'singleGallery', `fa-2x ${ComponentIconConstants.GallerySingleIcon}`));
        menuItem.subMenus.push(new MenuItem('Gallery Multiple', 'multipleGallery', `fa-2x ${ComponentIconConstants.GalleryMultipleIcon}`));
        menuItem.subMenus.push(new MenuItem('Image Picker', 'imagePicker', `fa-2x ${ComponentIconConstants.ImagePickerIcon}`));
        return menuItem;
    }

    private static researchMenu(): MenuItem {
        const menuItem = new MenuItem('Research', '', '');
        menuItem.subMenus.push(new MenuItem('Event Limiters', 'eventLimiters', `fa-2x ${ComponentIconConstants.EventLimiterIcon}`));
        return menuItem;
    }
}
