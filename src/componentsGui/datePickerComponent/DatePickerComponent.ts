import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import moment                                   from 'moment';
import Vue                                      from 'vue';

@Component
export default class DatePickerComponent extends Vue {

    @Prop()
    public value!: moment.Moment;
    private datePickerValue: string = '';

    private dialog: boolean = false;

    private mounted() {
        this.ModelToView();
    }

    @Watch('datePickerValue')
    private onNumberChanged(value: string, oldValue: string) {
        this.ViewToModel();
    }

    private ViewToModel() {
        this.$emit('input', moment(this.dateFormatted, 'DD-MMM-YYYY'));
    }

    private ModelToView() {
        if ( this.value !== null && this.value !== undefined) {
            if ( this.value.isValid() ) {
                this.datePickerValue = this.value.format('YYYY-MM-DD');
            }
        }
    }

    private get dateFormatted(): string {
        const native = moment(this.datePickerValue);
        if ( native.isValid() ) {
            return native.format('DD-MMM-YYYY');
        }
        return '';
    }

    private get dateIsValid(): boolean {
        const native = moment(this.datePickerValue);
        return native.isValid();
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-date-picker', DatePickerComponent);
