import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import GalleryImageModel                        from '../imageGallery/models/GalleryImageModel';
import GalleryModel                             from '../imageGallery/models/GalleryModel';
import MultipleGalleryComponent                 from '../imageGallery/multipleGalleryComponent/MultipleGalleryComponent';
import Vue                                      from 'vue';


@Component({
    components: {
        MultipleGalleryComponent,
    },
  })
export default class ImagePickerComponent extends Vue {

    @Prop()
    public value!: GalleryImageModel;

    @Prop()
    public galleries!: Array<GalleryModel>;

    private dialog: boolean = false;

    public onSelectedNewImage(image: GalleryImageModel) {
        this.$emit('selectedNewImage', image);
        this.dialog = false;
    }

    private get isTooltipDisabled(): boolean {
        if (this.value.name === '') {
            return true;
        }
        return false;
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-image-picker', ImagePickerComponent);
