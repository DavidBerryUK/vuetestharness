import { EnumPresentationVisualScale }          from '../../../enums/ScheduleEnums';
import moment                                   from 'moment';
import VisualScaleValuesModel                   from '../models/VisualScaleValuesModel';


export default class VisualScaleValuesFactory {

    public static VisualScaleParameters(visualScale: EnumPresentationVisualScale,
                                        dateTime: moment.Moment,
                                        width: number,
                                        pixelRatio: number): VisualScaleValuesModel {

        const visualScaleValues = new VisualScaleValuesModel();

        switch (visualScale) {
            case EnumPresentationVisualScale.quarterHour:
                this.configureQuarterHour(visualScaleValues, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.halfHour:
                this.configureHalfHour(visualScaleValues, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.hour:
                this.configureHour(visualScaleValues, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.halfDay:
                this.configureHalfDay(visualScaleValues, width, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.fullDay:
                this.configureFullDay(visualScaleValues, width, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.fiveDays:
                this.configureFiveDays(visualScaleValues, width, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.week:
                this.configureOneWeek(visualScaleValues, width, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.twoWeeks:
                this.configureTwoWeeks(visualScaleValues, width, pixelRatio, dateTime);
                break;

            case EnumPresentationVisualScale.fourWeeks:
                this.configureFourWeeks(visualScaleValues, width, pixelRatio, dateTime);
                break;
        }

        return visualScaleValues;
    }


    private static configureFourWeeks(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        const units28Days = 28 * 2;
        visualScaleValues.majorSpacing = Math.floor(width / units28Days);

        visualScaleValues.minsPerMajor = 60 * 24 / 2;
        visualScaleValues.minorSpacing = visualScaleValues.majorSpacing / 6;

        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('day');
        visualScaleValues.dayOfWeekFormat = 'dd';
        visualScaleValues.timeOfDayShow = false;
    }

    private static configureTwoWeeks(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        const units14Days = 14 * 2;

        visualScaleValues.majorSpacing = Math.floor(width / units14Days);
        visualScaleValues.minsPerMajor = 60 * 24 / 2;

        visualScaleValues.minorSpacing = visualScaleValues.majorSpacing / 6;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('day');
        visualScaleValues.dayOfWeekFormat = 'dd';
        visualScaleValues.timeOfDayShow = false;
    }

    private static configureOneWeek(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        const units7Days = 7 * 4;
        visualScaleValues.majorSpacing = Math.floor(width / units7Days);
        visualScaleValues.minsPerMajor = 60 * 24 / 4;
        visualScaleValues.minorSpacing = visualScaleValues.majorSpacing / 6;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('day');
    }

    private static configureFiveDays(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        const units5Days = 5 * 4;
        visualScaleValues.majorSpacing = Math.floor(width / units5Days);
        visualScaleValues.minsPerMajor = 60 * 24 / 4;
        visualScaleValues.minorSpacing = visualScaleValues.majorSpacing / 6;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('day');
    }

    private static configureFullDay(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        visualScaleValues.minsPerMajor = 60;
        visualScaleValues.minorSpacing = Math.floor((width) / (24 * 12));
        visualScaleValues.majorSpacing = visualScaleValues.minorSpacing * 12;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('day');
    }

    private static configureHalfDay(visualScaleValues: VisualScaleValuesModel, width: number, pixelRatio: number, dateTime: moment.Moment) {
        visualScaleValues.minsPerMajor = 60;
        visualScaleValues.minorSpacing = Math.floor((width) / (12 * 12));
        visualScaleValues.majorSpacing = visualScaleValues.minorSpacing * 12;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('hour');
    }

    private static configureHour(visualScaleValues: VisualScaleValuesModel, pixelRatio: number, dateTime: moment.Moment) {
        visualScaleValues.minsPerMajor = 60;
        visualScaleValues.minorSpacing = 4 * pixelRatio;
        visualScaleValues.majorSpacing = visualScaleValues.minorSpacing * 12;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('hour');
    }

    private static configureHalfHour(visualScaleValues: VisualScaleValuesModel, pixelRatio: number, dateTime: moment.Moment) {
        visualScaleValues.minsPerMajor = 30;
        visualScaleValues.minorSpacing = 8 * pixelRatio;
        visualScaleValues.majorSpacing = visualScaleValues.minorSpacing * 6;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('hour');
    }

    private static configureQuarterHour(visualScaleValues: VisualScaleValuesModel, pixelRatio: number, dateTime: moment.Moment) {
        visualScaleValues.minsPerMajor = 15;
        visualScaleValues.minorSpacing = 5 * pixelRatio;
        visualScaleValues.majorSpacing = visualScaleValues.minorSpacing * 15;
        visualScaleValues.minorHeight = 8 * pixelRatio;
        visualScaleValues.majorHeight = 16 * pixelRatio;
        visualScaleValues.baseDateTime = dateTime.startOf('hour');
    }
}


