import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import moment                                   from 'moment';
import VisualScaleValuesFactory                 from '@/componentsGui/timeLineComponents/views/axisRulerTimeComponent/factory/VisualScaleValuesFactory';
import VisualScaleValuesModel                   from '../models/VisualScaleValuesModel';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';


export default class RulerRenderer  extends HtmlCanvas {

    constructor(canvasElement: HTMLCanvasElement) {
        super(canvasElement);
    }

    public render(  dateTime: moment.Moment,
                    visualScale: EnumPresentationVisualScale,
                    pixelOffsetX: number) {

        if (this.context == null) {
            return;
        }

        //
        // get layout settings for different 'zoom' settings
        //
        const visualScaleValues = VisualScaleValuesFactory.VisualScaleParameters(visualScale, dateTime, this.width, this.pixelRatio);

        const rulerTopY = 44 * this.pixelRatio;
        const textHoursAndMinutesY = rulerTopY + 28 * this.pixelRatio;
        const textDaysOfWeekY = rulerTopY - 4 * this.pixelRatio;
        const textMonthOfYearY = 20 * this.pixelRatio;

        this.clearCanvas();
        this.drawTicks( this.context, rulerTopY, visualScaleValues.minorSpacing, visualScaleValues.minorHeight, pixelOffsetX);
        this.drawTicks( this.context, rulerTopY, visualScaleValues.majorSpacing, visualScaleValues.majorHeight, pixelOffsetX);

        if (visualScaleValues.timeOfDayShow) {
            this.drawAxisHoursAndMinutesText(   this.context, textHoursAndMinutesY, visualScaleValues, pixelOffsetX);
        }

        this.drawDayOfWeekText( this.context, visualScaleValues, rulerTopY, textDaysOfWeekY, pixelOffsetX);
        this.drawMonthText(     this.context, textMonthOfYearY, visualScaleValues, pixelOffsetX);
    }

    private drawMonthText(  context: CanvasRenderingContext2D,
                            textY: number,
                            visualScaleValues: VisualScaleValuesModel,
                            pixelOffsetX: number) {

        let axisDay = visualScaleValues.baseDateTime.clone();
        let x = 0;
        let firstX = 0;

        const minsInDay: number = 1440;
        const spacingPerDay =  visualScaleValues.majorSpacing *  minsInDay / visualScaleValues.minsPerMajor ;
        const mod = pixelOffsetX % spacingPerDay;
        const div = Math.floor(pixelOffsetX / spacingPerDay);

        const firstHour = visualScaleValues.baseDateTime.hour();
        axisDay = axisDay.add(div, 'days');

        x = x - mod;
        x = x + spacingPerDay * ( 1 + axisDay.daysInMonth() - axisDay.date());
        x = x - firstHour * spacingPerDay / 24;

        const distanceBeforeMovingFirstOffScreen = 130;

        if ( x < ( distanceBeforeMovingFirstOffScreen * this.pixelRatio )) {
            firstX = firstX - (( distanceBeforeMovingFirstOffScreen * this.pixelRatio ) - x);
        }

        context.font = `${20 * this.pixelRatio}px Arial`;
        context.textAlign = 'left';

        context.fillText(axisDay.format('MMMM YYYY'), firstX, textY);
        axisDay.startOf('month');
        axisDay.add(1, 'day');

        while ( x < this.width) {

            axisDay = axisDay.add(1, 'month');
            context.fillText(axisDay.format('MMMM YYYY'), x, textY);

            x = x + spacingPerDay * axisDay.daysInMonth();
        }
    }

    private drawDayOfWeekText(  context: CanvasRenderingContext2D,
                                visualScaleValues: VisualScaleValuesModel,
                                rulerTopY: number,
                                textDaysOfWeekY: number,
                                pixelOffsetX: number) {

        context.fillStyle = '#000000';
        let axisDay = visualScaleValues.baseDateTime.clone();
        let x = 0;
        let firstX = 0;

        const minsInDay: number = 1440;

        const spacingPerDay =  visualScaleValues.majorSpacing *  minsInDay / visualScaleValues.minsPerMajor ;

        const mod = pixelOffsetX % spacingPerDay;
        const div = Math.floor(pixelOffsetX / spacingPerDay);

        const firstHour = visualScaleValues.baseDateTime.hour();
        axisDay = axisDay.add(div, 'days');

        x = x - mod;
        x = x + spacingPerDay;
        x = x - firstHour * spacingPerDay / 24;

        const distanceBeforeMovingFirstOffScreen = visualScaleValues.dayOfWeekFormat.length <= 3 ? 40 : 110;
        if ( x < ( distanceBeforeMovingFirstOffScreen * this.pixelRatio )) {
            firstX = firstX - (( distanceBeforeMovingFirstOffScreen * this.pixelRatio ) - x);
        }


        context.textAlign = 'left';

        this.drawDayOfWeekAndDayOfMonth(context, visualScaleValues, axisDay, firstX, textDaysOfWeekY);


        context.beginPath();
        while ( x < this.width) {

            axisDay = axisDay.add(1, 'days');
            context.fillRect(x, 20, x + 40 , 0);

            this.drawDayOfWeekAndDayOfMonth(context, visualScaleValues, axisDay, x + 3 * this.pixelRatio, textDaysOfWeekY);

            context.moveTo(x, textDaysOfWeekY - ( 12 * this.pixelRatio));
            context.lineTo(x , rulerTopY + visualScaleValues.majorHeight);

            x = x + spacingPerDay;
        }
        context.lineWidth = 2 * this.pixelRatio;
        context.strokeStyle = '#000000';
        context.stroke();
    }

    private drawDayOfWeekAndDayOfMonth( context: CanvasRenderingContext2D,
                                        visualScaleValues: VisualScaleValuesModel,
                                        date: moment.Moment,
                                        x: number,
                                        y: number) {

        context.font = `${visualScaleValues.dayOfWeekFontSize * this.pixelRatio}px Arial`;
        const textDayOfWeek = date.format(visualScaleValues.dayOfWeekFormat);
        context.fillText(textDayOfWeek, x + 0 * this.pixelRatio, y);
        const widthTextDayOfWeek = context.measureText(textDayOfWeek).width;

        context.font = `${visualScaleValues.dateOfMonthFontSize * this.pixelRatio}px Arial`;
        context.fillText(date.format(visualScaleValues.dateOfMonthFormat),
                            x + widthTextDayOfWeek + visualScaleValues.dayOfMonthOffsetX,
                            y + visualScaleValues.dayOfMonthOffsetY);
    }

    private drawAxisHoursAndMinutesText(    context: CanvasRenderingContext2D,
                                            textY: number,
                                            visualScaleValues: VisualScaleValuesModel,
                                            pixelOffsetX: number) {

        context.fillStyle = '#000000';
        context.font = `${10 * this.pixelRatio}px Arial`;
        context.textAlign = 'center';

        let axisTime = visualScaleValues.baseDateTime.clone();

        const mod = pixelOffsetX % visualScaleValues.majorSpacing;
        const div = Math.floor(pixelOffsetX / visualScaleValues.majorSpacing);

        let x: number = 0 -  mod;
        axisTime = axisTime.add(div * visualScaleValues.minsPerMajor, 'minutes');


        while ( x < this.width) {
            context.fillText(axisTime.format('HH:mm'), x, textY);
            axisTime = axisTime.add(visualScaleValues.minsPerMajor, 'minutes');
            x = x + visualScaleValues.majorSpacing;
        }
    }

    private drawTicks(  context: CanvasRenderingContext2D,
                        rulerBase: number,
                        spacing: number,
                        tickHeight: number,
                        pixelOffsetX: number) {

        context.beginPath();
        const mod = pixelOffsetX % spacing;
        let x = 0 - mod;
        while ( x < this.width) {
            context.moveTo(x, rulerBase);
            context.lineTo(x, rulerBase + tickHeight);
            x = x + spacing;
        }
        context.lineWidth = 1 * this.pixelRatio;
        context.strokeStyle = '#000000';
        context.stroke();
    }

}
