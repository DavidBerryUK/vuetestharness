import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import moment                                   from 'moment';
import RulerRenderer                            from './RulerRenderer';
import Vue                                      from 'vue';


@Component
export default class AxisRulerTimeComponent extends Vue {

    @Prop()
    private startTime!: moment.Moment;

    @Prop()
    public presentationScale!: EnumPresentationVisualScale;

    @Prop()
    public pixelOffsetX!: number;

    private renderer!: RulerRenderer;

    private mounted() {

        // need to render on 'next tick' as screen may not be rendered, thus
        // the size of the parent would not be known. this 'bug' only happens on a screen
        // refresh
        //
        const instance = this;
        this.$nextTick(() => {
            this.renderer = new RulerRenderer(this.$refs.canvas as HTMLCanvasElement);
            instance.renderer.setupCanvas();
            instance.renderer.render(this.startTime, this.presentationScale , this.pixelOffsetX);
        });

    }

    @Watch('startTime')
    private onStartTimeChanged(value: moment.Moment, oldValue: moment.Moment) {
        this.renderer.render(this.startTime, this.presentationScale, this.pixelOffsetX);
    }

    @Watch('pixelOffsetX')
    private onPixelOffsetXChanged(value: number, oldValue: number) {
        this.renderer.render(this.startTime, this.presentationScale, this.pixelOffsetX);
    }

    @Watch('presentationScale')
    private onPresentationScaleChanged(value: EnumPresentationVisualScale, oldValue: EnumPresentationVisualScale) {
        this.renderer.render(this.startTime, this.presentationScale, this.pixelOffsetX);
    }
    private onResize(event: CustomEvent) {
        this.renderer.setupCanvas();
        this.renderer.render(this.startTime, this.presentationScale , this.pixelOffsetX);
      }

    public data(): any {
        return {};
    }
}

Vue.component('db-axis-ruler-time', AxisRulerTimeComponent);
