import moment                                   from 'moment';

export default class VisualScaleValuesModel {
    public minsPerMajor: number = 0;
    public minorSpacing: number = 0;
    public majorSpacing: number = 0;
    public minorHeight: number = 0;
    public majorHeight: number = 0;
    public textHoursAndMinsY: number = 0;
    public baseDateTime!: moment.Moment;

    public dayOfWeekShow: boolean = true;
    public timeOfDayShow: boolean = true;
    public dayOfWeekFormat: string = 'dddd';
    public dateOfMonthFormat: string = 'DD';

    public dayOfMonthOffsetX: number = 8;
    public dayOfMonthOffsetY: number = -8;

    public dayOfWeekFontSize: number = 16;
    public dateOfMonthFontSize: number = 12;

}
