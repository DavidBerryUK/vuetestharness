import { EnumPresentationVisualScale }          from '../../enums/ScheduleEnums';
import { ITimeLineEnvironmentModel }            from '../../interfaces/ITimeLineEnvironmentModel';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';
import Vue                                      from 'vue';


@Component
export default class TimelineGestureRecognizerComponent extends Vue {

  @Prop()
  public value!: ITimeLineEnvironmentModel;

  private isMouseDown: boolean = false;

  @Watch('value.presentationScale')
  private onNumberChanged(value: EnumPresentationVisualScale, oldValue: EnumPresentationVisualScale) {
    // console.log('presentation scale changed');
  }

  private pixelRatio: number = HtmlCanvas.calculatePixelRatio();

  private onMouseEnter(event: MouseEvent) {
    // console.log('mouse enter');
  }
  private onMouseLeave(event: MouseEvent) {
    // console.log('mouse leave');
  }
  private onMouseMove(event: MouseEvent) {
    if (this.isMouseDown) {
      // console.log(`mouse move ${event.movementX} ${event.movementY}`);
      this.value.pixelOffsetX = this.value.pixelOffsetX - event.movementX * this.pixelRatio;
    }
  }

  private onMouseDown(event: MouseEvent) {
    // console.log('mouse down');
    this.isMouseDown = true;
  }

  private onMouseUp(event: MouseEvent) {
    // console.log('mouse up');
    this.isMouseDown = false;
  }

  public data(): any {
    return {};
  }
}

Vue.component('db-time-line-gesture-recognizer', TimelineGestureRecognizerComponent);
