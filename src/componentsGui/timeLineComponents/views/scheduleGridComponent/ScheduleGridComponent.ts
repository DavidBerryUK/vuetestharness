import { EnumPresentationVisualScale }          from '../../enums/ScheduleEnums';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import moment                                   from 'moment';
import TimeTableModel                           from '../../models/TimeTableModel';
import VisualScaleValuesFactory                 from '../axisRulerTimeComponent/factory/VisualScaleValuesFactory';
import VisualScaleValuesModel                   from '../axisRulerTimeComponent/models/VisualScaleValuesModel';
import Vue                                      from 'vue';

@Component
export default class ScheduleGridComponent extends Vue {

    @Prop()
    private startTime!: moment.Moment;

    @Prop()
    public presentationScale!: EnumPresentationVisualScale;

    @Prop()
    public pixelOffsetX!: number;

    private gridLinesMinor: Array<number> = new Array<number>();
    private gridLinesMajor: Array<number> = new Array<number>();

    private timeTable = new TimeTableModel();

    private mounted() {

        const instance = this;
        this.$nextTick(() => {
            instance.calculateGrid(instance.startTime, instance.presentationScale , instance.pixelOffsetX);
        });
    }

    @Watch('startTime')
    private onStartTimeChanged(value: moment.Moment, oldValue: moment.Moment) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    @Watch('pixelOffsetX')
    private onPixelOffsetXChanged(value: number, oldValue: number) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    @Watch('presentationScale')
    private onPresentationScaleChanged(value: EnumPresentationVisualScale, oldValue: EnumPresentationVisualScale) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    private calculateGrid( dateTime: moment.Moment,
                           visualScale: EnumPresentationVisualScale,
                           pixelOffsetX: number) {

        const containerElement = this.$refs.gridContainer as HTMLDivElement;
        const width = containerElement.getBoundingClientRect().width;

        const visualScaleValues = VisualScaleValuesFactory.VisualScaleParameters(
            visualScale,
            dateTime,
            width,
            1);

        this.calculateMinorLines(pixelOffsetX, width, visualScaleValues);
        this.calculateMajorLines(pixelOffsetX, width, visualScaleValues);
    }

    private calculateMajorLines(pixelOffsetX: number, width: number, visualScaleValues: VisualScaleValuesModel) {
        this.gridLinesMajor = new Array<number>();
        const mod = pixelOffsetX % visualScaleValues.majorSpacing;
        let x = 0 - mod;
        while ( x < width) {
            if ( x >= 0) {
                this.gridLinesMajor.push(x);
            }
            x = x + visualScaleValues.majorSpacing;
        }
    }

    private calculateMinorLines(pixelOffsetX: number, width: number, visualScaleValues: VisualScaleValuesModel) {
        this.gridLinesMinor = new Array<number>();
        const mod = pixelOffsetX % visualScaleValues.minorSpacing;
        let x = 0 - mod;
        while ( x < width) {
            if ( x >= 0) {
                this.gridLinesMinor.push(x);
            }
            x = x + visualScaleValues.minorSpacing;
        }
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-schedule-grid', ScheduleGridComponent);
