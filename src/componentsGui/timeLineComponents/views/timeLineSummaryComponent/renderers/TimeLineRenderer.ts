import DateCalculations                         from '@/utilities/date/DateCalculations';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';
import moment                                   from 'moment';


export default class TimeLineRenderer extends HtmlCanvas {


    constructor(canvasElement: HTMLCanvasElement) {
        super( canvasElement);
    }

    public render(  dateFrom: moment.Moment,
                    dateTo: moment.Moment,
                    lineWidth: number,
                    nodeRadius: number) {

        if (this.context == null) {
            return;
        }

        if (dateFrom === undefined ) {
            return;
        }

        if (dateTo === undefined ) {
            return;
        }

        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        const segmentWidth =  Math.floor(this.width / monthCount);
        const offsetX = Math.floor(( this.width - (segmentWidth * monthCount)) / 2);
        this.drawPlaceholderSquares(offsetX, monthCount, segmentWidth);
        this.drawDateLabels(dateFrom, offsetX, monthCount, segmentWidth);
        this.drawTimeLineWithNodes(offsetX, monthCount, segmentWidth, lineWidth, nodeRadius);
    }

    private drawTimeLineWithNodes(offsetX: number, segmentCount: number, segmentWidth: number, lineWidth: number, nodeRadius: number) {
        if ( this.context === undefined ||  this.context === null) {
            return;
        }

        let x = offsetX + segmentWidth / 2;
        const y = 46 * this.pixelRatio;
        const radius = nodeRadius * this.pixelRatio;

        this.context.beginPath();
        for (let i = 0; i < segmentCount; i++) {

            this.context.moveTo(x + Math.cos(0) * radius, y + Math.sin(0) * radius);
            this.context.arc(x , y, radius, 0, 2 * Math.PI);
            if ( (i + 1 ) < segmentCount) {
                this.context.moveTo(x +  radius, y );
                this.context.lineTo(x + segmentWidth + - radius, y );
            } else {
                 // last line
                this.context.moveTo(x + radius , y);
                this.context.lineTo(x + segmentWidth / 2 , y );
            }
            x = x + segmentWidth;
        }


        // first line
        this.context.moveTo(offsetX , y);
        this.context.lineTo(offsetX + segmentWidth / 2 - radius, y );
        this.context.lineCap = 'round';
        this.context.lineWidth = lineWidth * this.pixelRatio;
        this.context.strokeStyle = '#000000';
        this.context.stroke();
    }

    private drawDateLabels(dateFrom: moment.Moment, offsetX: number, segmentCount: number, segmentWidth: number) {
        if ( this.context === undefined ||  this.context === null) {
            return;
        }

        let date = dateFrom.clone();
        let x = offsetX;
        const y = 30 * this.pixelRatio;

        this.context.fillStyle = '#000000';
        this.context.font = `${14 * this.pixelRatio}px Arial`;
        this.context.textAlign = 'center';

        // months
        for (let i = 0; i < segmentCount; i++) {
            this.context.fillText(date.format('MMM'), x + segmentWidth / 2 , y);
            x = x + segmentWidth;
            date.add(1, 'month');
        }


        this.context.font = `${10 * this.pixelRatio}px Arial`;

        x = offsetX;
        const dateY = 14 * this.pixelRatio;
        date = dateFrom.clone();
        // years
        for (let i = 0; i < segmentCount; i++) {
            this.context.fillText(date.format('YYYY'), x + segmentWidth / 2, dateY);
            x = x + segmentWidth;
            date.add(1, 'month');
        }
    }

    private drawPlaceholderSquares(offsetX: number, segmentCount: number, segmentWidth: number) {

        if ( this.context === undefined ||  this.context === null) {
            return;
        }

        let x = offsetX;
        const y = 2;
        const segmentHeight = 80 * this.pixelRatio;

        this.context.beginPath();
        for (let i = 0; i < segmentCount; i++) {
            this.context.rect(x, y, segmentWidth, segmentHeight);
            x = x + segmentWidth;
        }
        this.context.lineWidth = 1 * this.pixelRatio;
        this.context.strokeStyle = '#ddd';
        this.context.stroke();
    }

}
