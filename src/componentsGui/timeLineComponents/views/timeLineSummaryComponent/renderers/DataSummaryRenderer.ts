import { EnumChartType }                        from '@/componentsGui/simpleChartComponent/enums/SimpleChartEnums';
import { EnumSummaryTimeLineSummaryStyle }      from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import ChartDataPoint                           from '@/componentsGui/simpleChartComponent/models/ChartDataPoint';
import ChartDataSet                             from '@/componentsGui/simpleChartComponent/models/ChartDataSet';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';
import DateCalculations                         from '@/utilities/date/DateCalculations';
import DrawLineGraph                            from '@/componentsModules/graphicLibrary/canvasOperations/DrawLineGraph';
import DrawPieChart                             from '@/componentsModules/graphicLibrary/canvasOperations/DrawPieChart';
import EventSummaryByTimeModel                  from '@/componentsGui/timeLineComponents/models/EventSummaryByTimeModel';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';
import moment                                   from 'moment';
import Point                                    from '@/componentsModules/trigonometryLibrary/models/Point';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';
import Size                                     from '@/componentsModules/trigonometryLibrary/models/Size';
import TimeLineConstants                        from '@/componentsGui/timeLineComponents/constants/TimeLineConstants';



/**
 * draw infographic on the time line summary view
 *
 * @export
 * @class DataSummaryRenderer
 * @extends {HtmlCanvas}
 */
export default class DataSummaryRenderer  extends HtmlCanvas {

    constructor(canvasElement: HTMLCanvasElement) {
        super( canvasElement);
    }

    public render(  dateFrom: moment.Moment,
                    dateTo: moment.Moment,
                    rect: Rect,
                    summaryStyle: EnumSummaryTimeLineSummaryStyle,
                    summaryByMonth: Array<EventSummaryByTimeModel>,
                    summaryByDay: Array<EventSummaryByTimeModel>) {

        if (this.context == null) {
            return;
        }

        if (dateFrom === undefined ) {
            return;
        }

        if (dateTo === undefined ) {
            return;
        }

        //
        // collate info to draw line graphs
        //

        if ( summaryStyle === EnumSummaryTimeLineSummaryStyle.lineGraph ) {
            const chartModel =  this.createLineGraphDataSets(dateFrom, dateTo, summaryByDay);
            DrawLineGraph.draw(this, rect, chartModel);
        }

        if ( summaryStyle === EnumSummaryTimeLineSummaryStyle.pieChartPerMonth ) {
            this.createPieChartPerMonth(dateFrom, dateTo, rect, summaryByMonth);
        }
    }

    private createPieChartPerMonth(dateFrom: moment.Moment, dateTo: moment.Moment, rect: Rect, summaryByMonth: Array<EventSummaryByTimeModel>): void {
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        const segmentWidth =  Math.floor(this.width / monthCount);
        const offsetX = Math.floor(( this.width - (segmentWidth * monthCount)) / 2);
        let xBase = offsetX;

        const date = dateFrom.clone().startOf('month');
        for (let i = 0; i < monthCount; i++) {
            const itemsInMonth = summaryByMonth.filter((summary: EventSummaryByTimeModel) => summary.timePeriodStart.isSame(date, 'month') );

            if ( itemsInMonth.length === 1) {
                //
                // create dataset for pie chart
                //
                const chartModel = new ChartModel(EnumChartType.PieChart);
                const dataSet = new ChartDataSet('Pie Chart');
                if (itemsInMonth[0].failedCount > 0) {
                    dataSet.dataPoints.push( new ChartDataPoint(itemsInMonth[0].failedCount , 0, 'Failure', TimeLineConstants.ColourFailureDark));
                }
                if (itemsInMonth[0].pendingCount > 0) {
                    dataSet.dataPoints.push( new ChartDataPoint(itemsInMonth[0].pendingCount  , 0, 'Pending', TimeLineConstants.ColourPendingDark));
                }
                if (itemsInMonth[0].successCount > 0 ) {
                    dataSet.dataPoints.push( new ChartDataPoint(itemsInMonth[0].successCount , 0, 'Success', TimeLineConstants.ColourSuccessDark));
                }
                if (itemsInMonth[0].unknownCount > 0) {
                    dataSet.dataPoints.push( new ChartDataPoint(itemsInMonth[0].unknownCount , 0, 'Unknown', TimeLineConstants.ColourUnknownDark));
                }
                if (itemsInMonth[0].warningsCount > 0 ) {
                    dataSet.dataPoints.push( new ChartDataPoint(itemsInMonth[0].warningsCount , 0, 'Warning', TimeLineConstants.ColourWarningDark));
                }
                chartModel.dataSets.push(dataSet);
                const rectPie = new Rect(new Point( xBase, rect.top), new Size(segmentWidth, rect.size.height));
                DrawPieChart.draw(this, rectPie, chartModel);

            }

            date.add(1, 'month');
            xBase = xBase + segmentWidth;
        }
    }

    private createLineGraphDataSets(dateFrom: moment.Moment, dateTo: moment.Moment, summaryByDay: Array<EventSummaryByTimeModel>): ChartModel {

        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        const segmentWidth =  Math.floor(this.width / monthCount);
        const offsetX = Math.floor(( this.width - (segmentWidth * monthCount)) / 2);

        const date = dateFrom.clone().startOf('month');
        let xBase = offsetX;
        //
        // Create DataSets
        //
        const chartModel = new ChartModel(EnumChartType.LineGraph);
        const datasetFailed = new ChartDataSet('Failed', TimeLineConstants.ColourFailureDark);
        const datasetPending = new ChartDataSet('Pending', TimeLineConstants.ColourPendingDark);
        const datasetSuccess = new ChartDataSet('Success', TimeLineConstants.ColourSuccessDark);
        const datasetUnknown = new ChartDataSet('Unknown', TimeLineConstants.ColourUnknownDark);
        const datasetWarnings = new ChartDataSet('Warning', TimeLineConstants.ColourWarningDark);

        chartModel.dataSets.push(datasetFailed);
        chartModel.dataSets.push(datasetPending);
        chartModel.dataSets.push(datasetSuccess);
        chartModel.dataSets.push(datasetUnknown);
        chartModel.dataSets.push(datasetWarnings);


        //
        // Create DataSet Points
        //
        for (let i = 0; i < monthCount; i++) {

            // get items in the same month
            const itemsInMonth = summaryByDay.filter((summary: EventSummaryByTimeModel) => summary.timePeriodStart.isSame(date, 'month') );

            const daysInMonth = date.daysInMonth();

            if ( itemsInMonth.length === 0) {
                const xFrom = xBase;

                datasetFailed.dataPoints.push( new ChartDataPoint(xFrom, 0));
                datasetPending.dataPoints.push( new ChartDataPoint(xFrom, 0));
                datasetSuccess.dataPoints.push( new ChartDataPoint(xFrom, 0));
                datasetUnknown.dataPoints.push( new ChartDataPoint(xFrom, 0));
                datasetWarnings.dataPoints.push( new ChartDataPoint(xFrom, 0));

                const xTo = xBase + segmentWidth;

                datasetFailed.dataPoints.push( new ChartDataPoint(xTo, 0));
                datasetPending.dataPoints.push( new ChartDataPoint(xTo, 0));
                datasetSuccess.dataPoints.push( new ChartDataPoint(xTo, 0));
                datasetUnknown.dataPoints.push( new ChartDataPoint(xTo, 0));
                datasetWarnings.dataPoints.push( new ChartDataPoint(xTo, 0));
            } else {
                itemsInMonth.forEach((summary: EventSummaryByTimeModel) => {
                    const x = xBase + summary.timePeriodStart.date() / daysInMonth * segmentWidth;

                    datasetFailed.dataPoints.push( new ChartDataPoint(x, summary.failedCount));
                    datasetPending.dataPoints.push( new ChartDataPoint(x, summary.pendingCount));
                    datasetSuccess.dataPoints.push( new ChartDataPoint(x, summary.successCount));
                    datasetUnknown.dataPoints.push( new ChartDataPoint(x, summary.unknownCount));
                    datasetWarnings.dataPoints.push( new ChartDataPoint(x, summary.warningsCount));

                });
            }


            date.add(1, 'month');
            xBase = xBase + segmentWidth;
        }

        return chartModel;
    }
}
