import moment                                   from 'moment';

export default class MonthStatisticsModel {

    public date: moment.Moment;
    public successCount: number = 0;
    public warningCount: number = 0;
    public errorCount: number = 0;

    public pendingCount: number = 0;

    constructor(date: moment.Moment, pendingCount: number, successCount: number, warningCount: number, errorCount: number) {
        this.date = date;
        this.pendingCount = pendingCount;
        this.successCount = successCount;
        this.warningCount = warningCount;
        this.errorCount = errorCount;
    }

    public get totalCount(): number {
        return this.successCount + this.warningCount + this.errorCount + this.pendingCount;
    }

    public get pendingPercentage(): number {
        return this.pendingCount / this.totalCount;
    }

    public get successPercentage(): number {
        return this.successCount / this.totalCount;
    }

    public get warningPercentage(): number {
        return this.warningCount / this.totalCount;
    }

    public get errorPercentage(): number {
        return this.errorCount / this.totalCount;
    }
}
