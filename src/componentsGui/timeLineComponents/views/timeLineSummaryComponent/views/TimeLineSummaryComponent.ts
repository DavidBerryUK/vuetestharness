import { EnumPresentationVisualScale }          from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { EnumSummaryTimeLineSummaryStyle }      from '@/componentsGui/timeLineComponents/enums/ScheduleEnums';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import AggregateEventStatusByDay                from '@/componentsGui/timeLineComponents/aggregators/AggregateEventStatusByDay';
import AggregateEventStatusByMonth              from '@/componentsGui/timeLineComponents/aggregators/AggregateEventStatusByMonth';
import AggregatorEngine                         from '@/utilities/aggregator/AggregateEngine';
import Component                                from 'vue-class-component';
import DataKeyItemModel                         from '@/componentsGui/dataKeyListComponent/models/DataKeyItemModel';
import DataKeyListComponent                     from '@/componentsGui/dataKeyListComponent/views/DataKeyListComponent';
import DataSummaryRenderer                      from '../renderers/DataSummaryRenderer';
import DateRangeModel                           from '@/componentsGui/timeLineComponents/models/DateRangeModel';
import EventSummaryByTimeModel                  from '@/componentsGui/timeLineComponents/models/EventSummaryByTimeModel';
import moment                                   from 'moment';
import Point                                    from '@/componentsModules/trigonometryLibrary/models/Point';
import ProgramModel                             from '@/componentsGui/timeLineComponents/models/ProgramModel';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';
import Size                                     from '@/componentsModules/trigonometryLibrary/models/Size';
import TimeLineConstants                        from '@/componentsGui/timeLineComponents/constants/TimeLineConstants';
import TimeLineRenderer                         from '../renderers/TimeLineRenderer';
import VisualScaleHelper                        from '@/componentsGui/timeLineComponents/services/VisualScaleHelper';
import Vue                                      from 'vue';



@Component({
    components: {
        DataKeyListComponent,
    },
  })
export default class TimeLineSummaryComponent extends Vue {

    private summaryByMonth: Array<EventSummaryByTimeModel> = new Array<EventSummaryByTimeModel>();
    private summaryByDay: Array<EventSummaryByTimeModel> = new Array<EventSummaryByTimeModel>();


    @Prop()
    public overviewDateFrom!: moment.Moment;

    @Prop()
    public overviewDateTo!: moment.Moment;

    @Prop()
    public selectedDate!: moment.Moment;

    @Prop()
    public nodeRadius!: number;

    @Prop()
    public lineWidth!: number;

    @Prop()
    public presentationScale!: EnumPresentationVisualScale;

    @Prop()
    public programs!: Array<ProgramModel>;

    @Prop()
    public summaryStyle!: EnumSummaryTimeLineSummaryStyle;
    private timeLineRenderer!: TimeLineRenderer;
    private dataSummaryRenderer!: DataSummaryRenderer;



    //
    // These are updated automatically if the date range changes
    //
    private overviewDateRangeFirstDay!: moment.Moment;
    private overviewDateRangeLastDay!: moment.Moment;
    private overviewDateRangeNumberOfDays!: number;
    //
    // These are updated automatically if the date range changes
    //


    public dataKeys = this.dataKeyFactory();

    @Watch('summaryStyle')
    private onSummaryStyleChanged(value: EnumSummaryTimeLineSummaryStyle, oldValue: EnumSummaryTimeLineSummaryStyle) {
        this.renderCanvas();
    }

    @Watch('overviewDateFrom')
    private onOverviewDateFromChanged(value: moment.Moment, oldValue: moment.Moment) {
        this.calculateOverviewDateRanges();
        this.calculateEventSummary();
        this.renderCanvas();
    }

    @Watch('overviewDateTo')
    private onOverviewDateToChanged(value: moment.Moment, oldValue: moment.Moment) {
        this.calculateOverviewDateRanges();
        this.calculateEventSummary();
        this.renderCanvas();
    }

    @Watch('nodeRadius')
    private onNodeRadiusChanged(value: number, oldValue: number) {
        this.renderCanvas();
    }

    @Watch('lineWidth')
    private onLineWidthChanged(value: number, oldValue: number) {
        this.renderCanvas();
    }

    @Watch('programs', {deep: true})
    private onProgramsChanged(value: Array<ProgramModel>, oldValue: Array<ProgramModel>) {
        if ( this.programs) {
            this.calculateEventSummary();
            this.renderCanvas();
        }
    }

    private mounted() {
        // need to render on 'next tick' as screen may not be rendered, thus
        // the size of the parent would not be known. this 'bug' only happens on a screen
        // refresh
        //
        const instance = this;
        this.$nextTick(() => {
            instance.calculateOverviewDateRanges();
            instance.calculateEventSummary();
            instance.setupCanvas();
            instance.renderCanvas();

        });
    }

    private calculateOverviewDateRanges() {
        this.overviewDateRangeFirstDay = this.overviewDateFrom.clone().startOf('month');
        this.overviewDateRangeLastDay = this.overviewDateTo.clone().endOf('month');
        this.overviewDateRangeNumberOfDays = this.overviewDateRangeLastDay.diff(this.overviewDateRangeFirstDay, 'days');
    }

    private calculateEventSummary() {
        // collate all events into a single list
        const eventList = this.programs.map((program) => program.events).reduce((a, b) => a.concat(b));
        this.summaryByMonth = AggregatorEngine.aggregate(eventList, new AggregateEventStatusByMonth());
        this.summaryByDay = AggregatorEngine.aggregate(eventList, new AggregateEventStatusByDay());
    }

    private renderCanvas() {

        if (this.timeLineRenderer && this.dataSummaryRenderer) {

            this.timeLineRenderer.clearCanvas();

            this.timeLineRenderer.render(this.overviewDateFrom, this.overviewDateTo, this.lineWidth, this. nodeRadius);

            const summaryGraphAreaHeight = 55 * this.dataSummaryRenderer.pixelRatio;
            const rect = new Rect(new Point(0, this.dataSummaryRenderer.height - summaryGraphAreaHeight ), new Size(this.dataSummaryRenderer.width, summaryGraphAreaHeight));

            this.dataSummaryRenderer.render(this.overviewDateFrom, this.overviewDateTo, rect, this.summaryStyle, this.summaryByMonth, this.summaryByDay );
            this.$emit('rendered');
        }
    }

    private setupCanvas() {
        const canvasElement = this.$refs.canvas as HTMLCanvasElement;
        this.timeLineRenderer = new TimeLineRenderer(canvasElement);
        this.dataSummaryRenderer = new DataSummaryRenderer(canvasElement);
    }

    private onResize(event: CustomEvent) {
        this.timeLineRenderer.setupCanvas();
        this.renderCanvas();
    }

    private onMouseDown(event: MouseEvent) {
        // console.log(`mouse:down`);
    }

    private onMouseUp(event: MouseEvent) {
        // console.log(`mouse:up`);
    }

    private onMouseEnter(event: MouseEvent) {
        // console.log(`mouse:enter`);
    }

    private onMouseMove(event: MouseEvent) {
        this.calculateSelectedRangeFromMousePosition(event.offsetX);
    }

    private onMouseLeave(event: MouseEvent) {
       //  console.log(`mouse:leave`);
    }


    /**
     * calculate the new date range after the user has selected a new
     * area on the summary overview graphic
     * @private
     * @param {number} requestedPositionPixels
     * @param {number} totalRangePixels
     * @param {moment.Moment} dateFrom
     * @param {moment.Moment} dateTo
     * @memberof TimeLineSummaryComponent
     */
    private calculateSelectedRangeFromMousePosition(requestedPositionPixels: number): void {

        const totalRangePixels = this.timeLineRenderer.width / this.timeLineRenderer.pixelRatio;

        const targetDate = this.overviewDateRangeFirstDay.clone().add(requestedPositionPixels / totalRangePixels * this.overviewDateRangeNumberOfDays, 'days');

        //
        // calculate days before and after target point
        //

        const rangeDays = VisualScaleHelper.CalculateDateRange(this.presentationScale, targetDate, this.overviewDateRangeFirstDay, this.overviewDateRangeLastDay);
        // convert days and range back to screen coords
        //

        this.positionSelectedDateRangeElement(rangeDays);
    }

    private positionSelectedDateRangeElement( rangeDays: DateRangeModel) {
        const element = this.$refs.selectedRange as HTMLDivElement;

        if ( element !== undefined) {
            const totalRangePixels = this.timeLineRenderer.width / this.timeLineRenderer.pixelRatio;
            const days = VisualScaleHelper.DaysInVisualScale(this.presentationScale);

            const daysFromStart = rangeDays.start.diff(this.overviewDateRangeFirstDay, 'days');
            const x = daysFromStart / this.overviewDateRangeNumberOfDays * totalRangePixels;
            const w = days / this.overviewDateRangeNumberOfDays * totalRangePixels;
            element.style.left = `${x}px`;
            element.style.width = `${w}px`;
        }
    }


    private dataKeyFactory(): Array<DataKeyItemModel> {
        const keys = new Array<DataKeyItemModel>();

        keys.push( new DataKeyItemModel('Success', TimeLineConstants.ColourSuccessDark));
        keys.push( new DataKeyItemModel('Warning', TimeLineConstants.ColourWarningDark));
        keys.push( new DataKeyItemModel('Error', TimeLineConstants.ColourFailureDark));
        keys.push( new DataKeyItemModel('Pending', TimeLineConstants.ColourPendingDark));
        keys.push( new DataKeyItemModel('Unknown', TimeLineConstants.ColourUnknownDark));

        return keys;
    }
    public data(): any {
        return {};
    }
}

Vue.component('db-time-line-summary', TimeLineSummaryComponent);
