import { EnumPresentationVisualScale }          from '../../enums/ScheduleEnums';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import moment                                   from 'moment';
import ProgramFactory                           from '../../../../harnesses/testData/schedules/ProgramFactory';
import TimeTableModel                           from '../../models/TimeTableModel';
import VisualScaleValuesFactory                 from '../axisRulerTimeComponent/factory/VisualScaleValuesFactory';
import Vue                                      from 'vue';


@Component
export default class ScheduleProgramGridComponent extends Vue {

    @Prop()
    private startTime!: moment.Moment;

    @Prop()
    public presentationScale!: EnumPresentationVisualScale;

    @Prop()
    public pixelOffsetX!: number;

    private gridLinesMinor: Array<number> = new Array<number>();
    private gridLinesMajor: Array<number> = new Array<number>();

    private timeTable = new TimeTableModel();

    private mounted() {
        this.timeTable.addProgram(ProgramFactory.demoPrograms);

        const instance = this;
        this.$nextTick(() => {
            console.log(instance);
            instance.calculateGrid(instance.startTime, instance.presentationScale , instance.pixelOffsetX);
        });
    }

    @Watch('startTime')
    private onStartTimeChanged(value: moment.Moment, oldValue: moment.Moment) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    @Watch('pixelOffsetX')
    private onPixelOffsetXChanged(value: number, oldValue: number) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    @Watch('presentationScale')
    private onPresentationScaleChanged(value: EnumPresentationVisualScale, oldValue: EnumPresentationVisualScale) {
        this.calculateGrid(this.startTime, this.presentationScale , this.pixelOffsetX);
    }

    private calculateGrid( dateTime: moment.Moment,
                           visualScale: EnumPresentationVisualScale,
                           pixelOffsetX: number) {

        const containerElement = this.$refs.gridContainer as HTMLDivElement;
        const width = containerElement.getBoundingClientRect().width;

        const visualScaleValues = VisualScaleValuesFactory.VisualScaleParameters(
            visualScale,
            dateTime,
            width,
            1);
    }


    public data(): any {
        return {};
    }
}

Vue.component('db-schedule-program-grid', ScheduleProgramGridComponent);
