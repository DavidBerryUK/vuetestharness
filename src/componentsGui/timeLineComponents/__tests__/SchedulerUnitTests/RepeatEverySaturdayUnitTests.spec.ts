import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat Every Saturday', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat every saturday for 6 weeks', () => {
        const dateFrom = moment('2019-10-01 00:00:00');
        const dateTo = moment('2019-11-10 23:59:59');
        const timeOfDay = duration('20:30:00');
        const schedule = ScheduleFactory.everySaturdayAtTime(timeOfDay);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(6);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2019-10-05 20:30:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2019-10-12 20:30:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2019-10-19 20:30:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2019-10-26 20:30:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2019-11-02 20:30:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2019-11-09 20:30:00').toJSON());

    });

});
