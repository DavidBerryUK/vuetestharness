import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat weekend days', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat weekend day for january 2019', () => {
        const dateFrom = moment('2019-01-01 00:00:00');
        const dateTo = moment('2019-01-31 23:59:59');
        const minutesPastHour = duration('18:00:00');
        const schedule = ScheduleFactory.everyWeekendDayAtTime(minutesPastHour);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(8);
        expect(events[0].startDateTime.toJSON()).toBe(moment('2019-01-05 18:00:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2019-01-06 18:00:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2019-01-12 18:00:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2019-01-13 18:00:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2019-01-19 18:00:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2019-01-20 18:00:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2019-01-26 18:00:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2019-01-27 18:00:00').toJSON());

    });

});
