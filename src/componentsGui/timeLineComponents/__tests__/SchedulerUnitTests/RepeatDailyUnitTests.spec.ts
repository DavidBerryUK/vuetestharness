import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat Hourly', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat daily for 11 day', () => {
        const dateFrom = moment('2020-05-15 00:00:00');
        const dateTo = moment('2020-05-25 23:59:59');
        const timeOfDay = duration('18:30:00');
        const schedule = ScheduleFactory.everyDayAtTime(timeOfDay);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(11);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2020-05-15 18:30:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2020-05-16 18:30:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2020-05-17 18:30:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2020-05-18 18:30:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2020-05-19 18:30:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2020-05-20 18:30:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2020-05-21 18:30:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2020-05-22 18:30:00').toJSON());
        expect(events[8].startDateTime.toJSON()).toBe(moment('2020-05-23 18:30:00').toJSON());
        expect(events[9].startDateTime.toJSON()).toBe(moment('2020-05-24 18:30:00').toJSON());
        expect(events[10].startDateTime.toJSON()).toBe(moment('2020-05-25 18:30:00').toJSON());

    });

});
