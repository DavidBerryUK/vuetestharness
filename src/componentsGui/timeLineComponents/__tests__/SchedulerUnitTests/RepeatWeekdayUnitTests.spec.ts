import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat Weekday', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat weekday for 2 weeks', () => {
        const dateFrom = moment('2019-03-02 00:00:00');
        const dateTo = moment('2019-03-16 23:59:59');
        const minutesPastHour = duration('17:30:00');
        const schedule = ScheduleFactory.everyWeekdayAtTime(minutesPastHour);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(10);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2019-03-04 17:30:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2019-03-05 17:30:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2019-03-06 17:30:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2019-03-07 17:30:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2019-03-08 17:30:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2019-03-11 17:30:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2019-03-12 17:30:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2019-03-13 17:30:00').toJSON());
        expect(events[8].startDateTime.toJSON()).toBe(moment('2019-03-14 17:30:00').toJSON());
        expect(events[9].startDateTime.toJSON()).toBe(moment('2019-03-15 17:30:00').toJSON());



    });

});
