import { duration }                             from 'moment';
import { EnumScheduleDayOfWeek }                from '../../enums/ScheduleEnums';
import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';
import SchedulerCommonFunctions                 from '../../scheduler/helpers/SchedulerCommonFunctions';

describe('Calculate first date in every week period', () => {

    test('calculate first every week in period (monday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.monday;

        const expectedResult = moment('2000-01-03 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (tuesday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.tuesday;

        const expectedResult = moment('2000-01-04 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (wednesday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.wednesday;

        const expectedResult = moment('2000-01-05 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (thursday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.thursday;

        const expectedResult = moment('2000-01-06 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (friday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.friday;

        const expectedResult = moment('2000-01-07 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (saturday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.saturday;

        const expectedResult = moment('2000-01-01 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first every week in period (sunday)', () => {
        const dateFrom = moment('2000-01-01 00:00:00');
        const dateTo = moment('2000-01-20 00:00:00');
        const timeOfDay = duration('09:30:00');
        const dayOfWeek = EnumScheduleDayOfWeek.sunday;

        const expectedResult = moment('2000-01-02 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.dayOfWeek, dateFrom, dateTo, timeOfDay, dayOfWeek);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

});
