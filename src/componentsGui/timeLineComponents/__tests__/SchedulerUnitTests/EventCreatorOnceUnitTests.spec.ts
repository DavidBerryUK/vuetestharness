import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleModel                            from '../../models/ScheduleModel';
import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';

describe('Test Event Creator - Schedule Type = Once', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('Program in schedule range date range', () => {
        //
        // Create single schedule
        //
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.once;
        schedule.specificDateTime = moment('2019-10-05 01:10:00');

        //
        // Create events from schedule
        //
        const svc = new EventFromScheduleService();
        const scheduleStartDate = moment('2019-10-05 00:00:00');
        const scheduleStartEnd = moment('2019-10-06 00:00:00');
        const events = svc.createEvents(programGardenersWorld, schedule, scheduleStartDate, scheduleStartEnd);

        expect(events).toBeDefined();
        expect(events.length).toBe(1);

    });

    // test('Program before schedule range date range', () => {
    //     //
    //     // Create single schedule
    //     //
    //     const schedule = new ScheduleModel();
    //     schedule.schedulePeriod = EnumSchedulePeriod.once;
    //     schedule.specificDateTime = moment('2019-10-05 01:10:00');

    //     //
    //     // Create events from schedule
    //     //
    //     const svc = new EventFromScheduleService();
    //     const scheduleStartDate = moment('2019-10-05 00:00:00');
    //     const scheduleStartEnd = moment('2019-10-05 01:09:00');
    //     const events = svc.createEvents(programGardenersWorld, schedule, scheduleStartDate, scheduleStartEnd);

    //     expect(events).toBeDefined();
    //     expect(events.length).toBe(0);

    // });

});
