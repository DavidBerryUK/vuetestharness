import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';
import SchedulerCommonFunctions                 from '../../scheduler/helpers/SchedulerCommonFunctions';

describe('Calculate first date in period', () => {

    test('calculate first hour in period - start of day', () => {
        const dateFrom = moment('2000-01-10 00:00:00');
        const dateTo = moment('2000-01-11 00:00:00');
        const expectedResult = moment('2000-01-10 00:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyHour, dateFrom, dateTo);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first hour in period - midday', () => {
        const dateFrom = moment('2000-01-10 12:00:00');
        const dateTo = moment('2000-01-11 12:00:00');
        const expectedResult = moment('2000-01-10 12:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyHour, dateFrom, dateTo);


        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first hour in period - first available hour', () => {
        const dateFrom = moment('2000-01-10 12:30:00');
        const dateTo = moment('2000-01-11 12:00:00');
        const expectedResult = moment('2000-01-10 13:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyHour, dateFrom, dateTo);


        if ( result === null) {
            fail('result not defined');
        } else {
            expect(expectedResult.isSame(result)).toBeTruthy();
        }

    });

    test('calculate first hour in period - when period does not allow for it..', () => {
        const dateFrom = moment('2000-01-10 12:30:00');
        const dateTo = moment('2000-01-10 12:45:00');

        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyHour, dateFrom, dateTo);

        expect(result).toBeNull();

    });
});
