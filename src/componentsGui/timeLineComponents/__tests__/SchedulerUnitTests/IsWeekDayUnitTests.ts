import { EnumScheduleDayOfWeek }                from '../../enums/ScheduleEnums';
import ScheduleEnumHelper                       from '../../scheduler/helpers/ScheduleEnumHelper';

describe('Is Weekday Unit Tests', () => {

    test('Test Monday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.monday)).toBeTruthy();
    });

    test('Test Tuesday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.tuesday)).toBeTruthy();
    });

    test('Test Wednesday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.wednesday)).toBeTruthy();
    });

    test('Test Thursday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.thursday)).toBeTruthy();
    });

    test('Test Friday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.friday)).toBeTruthy();
    });

    test('Test Saturday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.saturday)).toBeFalsy();
    });

    test('Test Sunday', () => {
        expect(ScheduleEnumHelper.isWeekDay(EnumScheduleDayOfWeek.sunday)).toBeFalsy();
    });
});
