import { EnumScheduleDayOfWeek }                from '../../enums/ScheduleEnums';
import ScheduleEnumHelper                       from '../../scheduler/helpers/ScheduleEnumHelper';

describe('Is Weekend Unit Tests', () => {

    test('Test Monday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.monday)).toBeFalsy();
    });

    test('Test Tuesday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.tuesday)).toBeFalsy();
    });

    test('Test Wednesday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.wednesday)).toBeFalsy();
    });

    test('Test Thursday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.thursday)).toBeFalsy();
    });

    test('Test Friday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.friday)).toBeFalsy();
    });

    test('Test Saturday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.saturday)).toBeTruthy();
    });

    test('Test Sunday', () => {
        expect(ScheduleEnumHelper.isWeekendDay(EnumScheduleDayOfWeek.sunday)).toBeTruthy();
    });
});
