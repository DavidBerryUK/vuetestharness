import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment, { duration }                                   from 'moment';
import SchedulerCommonFunctions                 from '../../scheduler/helpers/SchedulerCommonFunctions';

describe('Calculate first date in period', () => {

    test('calculate first day in period - basic test', () => {
        const dateFrom = moment('2000-01-10 00:00:00');
        const dateTo = moment('2000-01-11 00:00:00');
        const timeOfDay = duration('15:00:00');
        const expectedResult = moment('2000-01-10 15:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyDay, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first day in period - roll over to next day', () => {
        const dateFrom = moment('2000-01-10 16:00:00');
        const dateTo = moment('2000-01-12 00:00:00');
        const timeOfDay = duration('15:00:00');
        const expectedResult = moment('2000-01-11 15:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyDay, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first day in period - no period in date range', () => {
        const dateFrom = moment('2000-01-10 16:00:00');
        const dateTo = moment('2000-01-11 12:00:00');
        const timeOfDay = duration('15:00:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyDay, dateFrom, dateTo, timeOfDay);

        expect(result).toBeNull();
    });
});
