import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat Every Monday', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat every monday for 2 months', () => {
        const dateFrom = moment('2018-02-01 00:00:00');
        const dateTo = moment('2018-03-31 23:59:59');
        const timeOfDay = duration('23:10:00');
        const schedule = ScheduleFactory.everyMondayAtTime(timeOfDay);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(8);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2018-02-05 23:10:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2018-02-12 23:10:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2018-02-19 23:10:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2018-02-26 23:10:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2018-03-05 23:10:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2018-03-12 23:10:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2018-03-19 23:10:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2018-03-26 23:10:00').toJSON());

    });

});
