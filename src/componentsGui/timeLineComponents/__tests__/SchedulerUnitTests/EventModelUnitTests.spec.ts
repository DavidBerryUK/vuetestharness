import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventModel                               from '../../models/EventModel';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';

describe('Event Model Unit Tests', () => {

    test('Test end date', () => {
        const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
        const programGardenersWorld = new ProgramModel(
            'Gardeners World',
            'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
            moment.duration('00:05:00'),
            categoryHomeImprovement);

        const event = new EventModel(   programGardenersWorld,
                                        moment('2020-01-14 17:30:00'),
                                        duration('00:45:00'));

        // start date
        expect(event.startDateTime.year()).toBe(2020);
        expect(event.startDateTime.month()).toBe(0);
        expect(event.startDateTime.date()).toBe(14);
        expect(event.startDateTime.hours()).toBe(17);
        expect(event.startDateTime.minutes()).toBe(30);

        // duration
        expect(event.estimatedDuration.minutes()).toBe(45);

        // estimated end date
        expect(event.estimatedEndDateTime.year()).toBe(2020);
        expect(event.estimatedEndDateTime.month()).toBe(0);
        expect(event.estimatedEndDateTime.date()).toBe(14);
        expect(event.estimatedEndDateTime.hours()).toBe(18);
        expect(event.estimatedEndDateTime.minutes()).toBe(15);

    });
});
