import { duration }                             from 'moment';
import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';
import SchedulerCommonFunctions                 from '../../scheduler/helpers/SchedulerCommonFunctions';

describe('Calculate weekend date in period', () => {

    test('calculate first weekend period - sunday', () => {
        // sunday
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // sunday
        const expectedResult = moment('2018-12-01 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(
                            EnumSchedulePeriod.everyWeekendDay,
                            dateFrom,
                            dateTo,
                            timeOfDay);

        expect(result).not.toBeNull();
        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate first weekend period - saturday', () => {
        // saturday
        const dateFrom = moment('2018-12-02 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-03 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(
                EnumSchedulePeriod.everyWeekday,
                dateFrom,
                dateTo,
                timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - monday - all day', () => {
        // monday
        const dateFrom = moment('2018-12-03 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-03 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - monday - roll over', () => {
        // monday afternoon
        const dateFrom = moment('2018-12-03 12:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-04 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - tuesday - all day', () => {
        // monday
        const dateFrom = moment('2018-12-04 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-04 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - wednesday - all day', () => {
        // monday
        const dateFrom = moment('2018-12-05 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-05 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - thursday - all day', () => {
        // monday
        const dateFrom = moment('2018-12-06 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-06 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

    test('calculate first weekend period - friday - all day', () => {
        // monday
        const dateFrom = moment('2018-12-07 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');
        const timeOfDay = duration('09:30:00');

        // monday
        const expectedResult = moment('2018-12-07 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(EnumSchedulePeriod.everyWeekday, dateFrom, dateTo, timeOfDay);

        expect(result).toBeDefined();

        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }

    });

});
