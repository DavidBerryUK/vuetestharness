import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';


describe('Repeat Hourly', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat hourly for 1 day', () => {
        const dateFrom = moment('2000-01-10 00:00:00');
        const dateTo = moment('2000-01-10 23:59:59');
        const minutesPastHour = duration('00:30:00');
        const schedule = ScheduleFactory.everyHourAtTime(minutesPastHour);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(24);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2000-01-10 00:30:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2000-01-10 01:30:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2000-01-10 02:30:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2000-01-10 03:30:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2000-01-10 04:30:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2000-01-10 05:30:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2000-01-10 06:30:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2000-01-10 07:30:00').toJSON());
        expect(events[8].startDateTime.toJSON()).toBe(moment('2000-01-10 08:30:00').toJSON());
        expect(events[9].startDateTime.toJSON()).toBe(moment('2000-01-10 09:30:00').toJSON());
        expect(events[10].startDateTime.toJSON()).toBe(moment('2000-01-10 10:30:00').toJSON());
        expect(events[11].startDateTime.toJSON()).toBe(moment('2000-01-10 11:30:00').toJSON());
        expect(events[12].startDateTime.toJSON()).toBe(moment('2000-01-10 12:30:00').toJSON());

        expect(events[13].startDateTime.toJSON()).toBe(moment('2000-01-10 13:30:00').toJSON());
        expect(events[14].startDateTime.toJSON()).toBe(moment('2000-01-10 14:30:00').toJSON());
        expect(events[15].startDateTime.toJSON()).toBe(moment('2000-01-10 15:30:00').toJSON());
        expect(events[16].startDateTime.toJSON()).toBe(moment('2000-01-10 16:30:00').toJSON());
        expect(events[17].startDateTime.toJSON()).toBe(moment('2000-01-10 17:30:00').toJSON());
        expect(events[18].startDateTime.toJSON()).toBe(moment('2000-01-10 18:30:00').toJSON());
        expect(events[19].startDateTime.toJSON()).toBe(moment('2000-01-10 19:30:00').toJSON());
        expect(events[20].startDateTime.toJSON()).toBe(moment('2000-01-10 20:30:00').toJSON());
        expect(events[21].startDateTime.toJSON()).toBe(moment('2000-01-10 21:30:00').toJSON());
        expect(events[22].startDateTime.toJSON()).toBe(moment('2000-01-10 22:30:00').toJSON());
        expect(events[23].startDateTime.toJSON()).toBe(moment('2000-01-10 23:30:00').toJSON());


    });

});
