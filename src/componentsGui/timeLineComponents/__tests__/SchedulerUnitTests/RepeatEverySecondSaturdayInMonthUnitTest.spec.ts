import { duration }                             from 'moment';
import CategoryModel                            from '../../models/CategoryModel';
import EventFromScheduleService                 from '../../scheduler/EventFromScheduleService';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleFactory                          from '../../factories/ScheduleFactory';
import { EnumScheduleDayOfWeek, EnumScheduleDayOccurrenceInMonth } from '../../enums/ScheduleEnums';


describe('Repeat Every Second Saturday in Month', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    test('repeat every saturday in Month in 2019', () => {
        const dateFrom = moment('2019-01-01 00:00:00');
        const dateTo = moment('2019-12-31 23:59:59');
        const timeOfDay = duration('23:00:00');
        const schedule = ScheduleFactory.everyMonthOnDayAtTime( EnumScheduleDayOfWeek.saturday,
                                                                EnumScheduleDayOccurrenceInMonth.second,
                                                                timeOfDay);

        const svc = new EventFromScheduleService();
        const events = svc.createEvents(programGardenersWorld, schedule, dateFrom, dateTo);

        expect(events).toBeDefined();
        expect(events.length).toBe(12);

        expect(events[0].startDateTime.toJSON()).toBe(moment('2019-01-12 23:00:00').toJSON());
        expect(events[1].startDateTime.toJSON()).toBe(moment('2019-02-09 23:00:00').toJSON());
        expect(events[2].startDateTime.toJSON()).toBe(moment('2019-03-09 23:00:00').toJSON());
        expect(events[3].startDateTime.toJSON()).toBe(moment('2019-04-13 23:00:00').toJSON());
        expect(events[4].startDateTime.toJSON()).toBe(moment('2019-05-11 23:00:00').toJSON());
        expect(events[5].startDateTime.toJSON()).toBe(moment('2019-06-08 23:00:00').toJSON());
        expect(events[6].startDateTime.toJSON()).toBe(moment('2019-07-13 23:00:00').toJSON());
        expect(events[7].startDateTime.toJSON()).toBe(moment('2019-08-10 23:00:00').toJSON());
        expect(events[8].startDateTime.toJSON()).toBe(moment('2019-09-14 23:00:00').toJSON());
        expect(events[9].startDateTime.toJSON()).toBe(moment('2019-10-12 23:00:00').toJSON());
        expect(events[10].startDateTime.toJSON()).toBe(moment('2019-11-09 23:00:00').toJSON());
        expect(events[11].startDateTime.toJSON()).toBe(moment('2019-12-14 23:00:00').toJSON());

    });

});
