import { duration }                             from 'moment';
import { EnumScheduleDayOccurrenceInMonth }     from '../../enums/ScheduleEnums';
import { EnumScheduleDayOfWeek }                from '../../enums/ScheduleEnums';
import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';
import SchedulerCommonFunctions                 from '../../scheduler/helpers/SchedulerCommonFunctions';

describe('Calculate specific day of month', () => {

    test('calculate first monday in month', () => {
        // sunday
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');

        const period = EnumSchedulePeriod.everyMonth;
        const dayOfWeek = EnumScheduleDayOfWeek.monday;
        const occurrence = EnumScheduleDayOccurrenceInMonth.first;
        const timeOfDay = duration('09:30:00');

        // sunday
        const expectedResult = moment('2018-12-03 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(period, dateFrom, dateTo, timeOfDay, dayOfWeek, undefined, occurrence);

        expect(result).not.toBeNull();
        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate second saturday in month', () => {
        // sunday
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');

        const period = EnumSchedulePeriod.everyMonth;
        const dayOfWeek = EnumScheduleDayOfWeek.saturday;
        const occurrence = EnumScheduleDayOccurrenceInMonth.second;
        const timeOfDay = duration('09:30:00');

        // sunday
        const expectedResult = moment('2018-12-08 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(period, dateFrom, dateTo, timeOfDay, dayOfWeek, undefined, occurrence);

        expect(result).not.toBeNull();
        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });

    test('calculate last thursday in month', () => {
        // sunday
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2018-12-31 23:59:59');

        const period = EnumSchedulePeriod.everyMonth;
        const dayOfWeek = EnumScheduleDayOfWeek.thursday;
        const occurrence = EnumScheduleDayOccurrenceInMonth.last;
        const timeOfDay = duration('09:30:00');

        // sunday
        const expectedResult = moment('2018-12-27 09:30:00');
        const result = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(period, dateFrom, dateTo, timeOfDay, dayOfWeek, undefined, occurrence);

        expect(result).not.toBeNull();
        if ( result !== null) {
            expect(result.toJSON()).toBe(expectedResult.toJSON());
        }
    });


});
