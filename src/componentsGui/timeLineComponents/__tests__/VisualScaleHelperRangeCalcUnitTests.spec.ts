import { EnumPresentationVisualScale }          from '../enums/ScheduleEnums';
import moment                                   from 'moment';
import VisualScaleHelper                        from '../services/VisualScaleHelper';


describe('Visual Scale - calc days for target and scale Unit tests', () => {

    test('get days in quarter hour', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.quarterHour;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in half hour', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.halfHour;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in half day', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.halfDay;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in full day', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.fullDay;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('10-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in five day', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.fiveDays;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('08-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('12-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in 1 week', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.week;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);
        expect(result.start.isSame(moment('07-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
        expect(result.end.isSame(moment('13-05-2020', 'DD-MM-YYYY'))).toBeTruthy();
    });

    test('get days in 2 weeks', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.twoWeeks;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);

        expect(result.start.format('DD-MM-YYYY')).toBe('04-05-2020');
        expect(result.end.format('DD-MM-YYYY')).toBe('17-05-2020');
    });

    test('get days in 4 weeks', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('10-05-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.fourWeeks;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);

        expect(result.start.format('DD-MM-YYYY')).toBe('27-04-2020');
        expect(result.end.format('DD-MM-YYYY')).toBe('24-05-2020');
    });

    test('limit to start range', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('05-01-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.fourWeeks;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);

        expect(result.start.format('DD-MM-YYYY')).toBe('01-01-2020');
        expect(result.end.format('DD-MM-YYYY')).toBe('28-01-2020');
    });

    test('limit to end range', () => {
        const dateFrom = moment('01-01-2020', 'DD-MM-YYYY');
        const dateTo = moment('01-01-2021', 'DD-MM-YYYY');
        const targetDate = moment('25-12-2020', 'DD-MM-YYYY');
        const scale = EnumPresentationVisualScale.fourWeeks;

        const result = VisualScaleHelper.CalculateDateRange(scale, targetDate, dateFrom, dateTo);

        expect(result.start.format('DD-MM-YYYY')).toBe('05-12-2020');
        expect(result.end.format('DD-MM-YYYY')).toBe('01-01-2021');
    });

});
