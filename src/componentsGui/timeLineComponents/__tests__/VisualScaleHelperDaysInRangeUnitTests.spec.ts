import { EnumPresentationVisualScale }          from '../enums/ScheduleEnums';
import VisualScaleHelper                        from '../services/VisualScaleHelper';


describe('Visual Scale - calc days in range Unit tests', () => {

    test('get days in quarter hour', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.quarterHour);
        expect(result).toBe(1);
    });

    test('get days in half hour', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.halfHour);
        expect(result).toBe(1);
    });

    test('get days in half day', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.halfDay);
        expect(result).toBe(1);
    });

    test('get days in five days', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.fiveDays);
        expect(result).toBe(5);
    });

    test('get days in 1 week', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.week);
        expect(result).toBe(7);
    });

    test('get days in two weeks', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.twoWeeks);
        expect(result).toBe(14);
    });

    test('get days in four weeks', () => {
        const result = VisualScaleHelper.DaysInVisualScale(EnumPresentationVisualScale.fourWeeks);
        expect(result).toBe(28);
    });

});
