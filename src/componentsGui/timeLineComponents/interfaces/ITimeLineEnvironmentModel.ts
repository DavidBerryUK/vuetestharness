import { EnumPresentationVisualScale }          from '../enums/ScheduleEnums';
import moment                                   from 'moment';

export interface ITimeLineEnvironmentModel {

    startTime: moment.Moment;

    presentationScale: EnumPresentationVisualScale;

    pixelOffsetX: number;
}
