import EventFromScheduleService                 from './EventFromScheduleService';
import EventModel                               from '../models/EventModel';
import moment                                   from 'moment';
import ProgramModel                             from '../models/ProgramModel';
import ScheduleModel                            from '../models/ScheduleModel';

export default class ProgramEventsService {

    public static createEventsForPrograms(  programs: Array<ProgramModel>,
                                            dateFrom: moment.Moment,
                                            dateTo: moment.Moment) {
        programs.forEach((program: ProgramModel) => {
            this.createEventsForProgram(program, dateFrom, dateTo);
        });
    }

    public static createEventsForProgram(   program: ProgramModel,
                                            dateFrom: moment.Moment,
                                            dateTo: moment.Moment) {

        const svc = new EventFromScheduleService();
        let events = new Array<EventModel>();

        program.schedules.forEach((schedule: ScheduleModel) => {
            const scheduleEvents = svc.createEvents(program, schedule, dateFrom, dateTo);
            events = events.concat(scheduleEvents);
        });

        // sort into date order
        events =  events.sort((a: EventModel, b: EventModel) => {
            return a.startDateTime.diff(b.startDateTime);
        });

        // deduplicate duplicated date
        events = events.filter((element, index, collection) => {
            return events.map((mapObj) => mapObj.startDateTime).indexOf(element.startDateTime) === index;
        });

        // assign to program
        program.events = events;


    }
}
