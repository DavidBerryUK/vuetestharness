import { EnumScheduleDayOfWeek } from '../../enums/ScheduleEnums';

export default class ScheduleEnumHelper {

    public static isWeekDay(dayOfWeek: EnumScheduleDayOfWeek) {
        if (    dayOfWeek === EnumScheduleDayOfWeek.monday ||
                dayOfWeek === EnumScheduleDayOfWeek.tuesday ||
                dayOfWeek === EnumScheduleDayOfWeek.wednesday ||
                dayOfWeek === EnumScheduleDayOfWeek.thursday ||
                dayOfWeek === EnumScheduleDayOfWeek.friday) {
            return true;
        }
        return false;
    }

    public static isWeekendDay(dayOfWeek: EnumScheduleDayOfWeek) {
        if (    dayOfWeek === EnumScheduleDayOfWeek.saturday ||
                dayOfWeek === EnumScheduleDayOfWeek.sunday) {
            return true;
        }
        return false;
    }
}
