import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleEnumHelper                       from './ScheduleEnumHelper';
import ScheduleModel                            from '../../models/ScheduleModel';
import SchedulerCommonFunctions                 from './SchedulerCommonFunctions';

export default class EventSchedulerRepeater {

    public static createDatesForSchedule(   program: ProgramModel,
                                            schedule: ScheduleModel,
                                            dateFrom: moment.Moment,
                                            dateTo: moment.Moment): Array<moment.Moment> {

        const schedules = new Array<moment.Moment>();

        const scheduleDate = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(
            schedule.schedulePeriod,
            dateFrom,
            dateTo,
            schedule.startTime,
            schedule.scheduledDayOfWeek,
            schedule.dateOfMonth,
            schedule.scheduleDayOccurrenceInMonth);

        const periodDuration = SchedulerCommonFunctions.getPeriodDuration(schedule.schedulePeriod);

        if ( scheduleDate !== null && periodDuration !== null) {
            while ( scheduleDate.isSameOrBefore(dateTo) ) {

                const dayOfWeek = scheduleDate.isoWeekday();
                let process = true;

                if (    schedule.schedulePeriod === EnumSchedulePeriod.everyWeekday &&
                        ScheduleEnumHelper.isWeekendDay(dayOfWeek)) {
                    process = false;
                }

                if (    schedule.schedulePeriod === EnumSchedulePeriod.everyWeekendDay &&
                        ScheduleEnumHelper.isWeekDay(dayOfWeek)) {
                        process = false;
                }

                if (process) {
                    schedules.push( scheduleDate.clone() );
                }
                scheduleDate.add( periodDuration );
            }
        }

        return schedules;
    }

}
