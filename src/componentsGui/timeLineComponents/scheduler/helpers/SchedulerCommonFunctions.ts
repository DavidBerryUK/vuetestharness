import { duration }                             from 'moment';
import { EnumScheduleDayOccurrenceInMonth }     from '../../enums/ScheduleEnums';
import { EnumScheduleDayOfWeek }                from '../../enums/ScheduleEnums';
import { EnumSchedulePeriod }                   from '../../enums/ScheduleEnums';
import moment                                   from 'moment';


export default class SchedulerCommonFunctions {

    public static calculateFirstDateTimeInPeriod(   schedulePeriod: EnumSchedulePeriod,
                                                    dateFrom: moment.Moment,
                                                    dateTo: moment.Moment,
                                                    timeOfDay?: moment.Duration,
                                                    dayOfWeek?: EnumScheduleDayOfWeek,
                                                    dayOfMonth?: number | null,
                                                    occurrence?: EnumScheduleDayOccurrenceInMonth): moment.Moment | null {

        let date: moment.Moment | null = null;

        switch ( schedulePeriod ) {
            case EnumSchedulePeriod.everyMonth:
                if ( timeOfDay !== undefined && dayOfWeek !== undefined && occurrence !== undefined ) {
                    date = this.calculateDayOfMonth(dateFrom, dateTo, timeOfDay, dayOfWeek, occurrence);
                    return date;
                }
                break;

            case EnumSchedulePeriod.dayOfWeek:
                if ( timeOfDay !== undefined && dayOfWeek !== undefined ) {
                    date = this.calculateDayOfWeek(dateFrom, dateTo, timeOfDay, dayOfWeek);
                    return date;
                }
                break;

            case EnumSchedulePeriod.everyHour:
                date = dateFrom.clone().startOf('hour').add(timeOfDay);
                break;

            case EnumSchedulePeriod.everyDay:
                date = dateFrom.clone().startOf('day').add(timeOfDay);
                break;

            case EnumSchedulePeriod.everyWeekday:
                date = dateFrom.clone().startOf('day').add(timeOfDay);
                const dayOfWeek1 = date.isoWeekday();
                if (dayOfWeek1 === EnumScheduleDayOfWeek.saturday) {
                    date.add(2, 'day');
                } else if ( dayOfWeek1 === EnumScheduleDayOfWeek.sunday) {
                    date.add(1, 'day');
                }
                break;

            case EnumSchedulePeriod.everyWeekendDay:
                date = dateFrom.clone();
                date.startOf('day');
                date.add(timeOfDay);
                const dayOfWeek2 = date.isoWeekday();
                if (dayOfWeek2 === EnumScheduleDayOfWeek.monday) {
                    date.add(5, 'day');
                } else if ( dayOfWeek2 === EnumScheduleDayOfWeek.tuesday) {
                    date.add(4, 'day');
                } else if ( dayOfWeek2 === EnumScheduleDayOfWeek.wednesday) {
                    date.add(3, 'day');
                } else if ( dayOfWeek2 === EnumScheduleDayOfWeek.thursday) {
                    date.add(2, 'day');
                } else if ( dayOfWeek2 === EnumScheduleDayOfWeek.friday) {
                    date.add(1, 'day');
                }
                break;

            case EnumSchedulePeriod.everyWeek:
                date = dateFrom.clone().startOf('week').add(timeOfDay);
                if ( dayOfWeek !== null) {
                    while ( date.isoWeekday() !== dayOfWeek) {
                        date.add(1, 'day');
                    }
                }
                break;
        }

        if ( date !== null) {
            const periodDuration = SchedulerCommonFunctions.getPeriodDuration(schedulePeriod);
            if ( periodDuration !== null) {
                while ( date.isBefore(dateFrom) ) {
                    date.add(periodDuration);
                }
            }
            if ( date.isAfter(dateTo)) {
                date = null;
            }
        }
        return date;
    }

    public static getPeriodDuration( schedulePeriod: EnumSchedulePeriod): moment.Duration | null {

        switch ( schedulePeriod ) {

            case EnumSchedulePeriod.everyHour:
                return duration(1, 'hour');

            case EnumSchedulePeriod.everyDay:
                return duration(1, 'day');

            case EnumSchedulePeriod.everyWeekday:
                return duration(1, 'day');

            case EnumSchedulePeriod.everyWeekendDay:
                return duration(1, 'day');

            case EnumSchedulePeriod.everyWeek:
                return duration(1, 'week');

            case EnumSchedulePeriod.dayOfWeek:
                return duration(1, 'week');

            case EnumSchedulePeriod.everyMonth:
                return duration(1, 'month');
        }

        return null;
    }

    private static calculateDayOfWeek(  dateFrom: moment.Moment,
                                        dateTo: moment.Moment,
                                        timeOfDay: moment.Duration,
                                        dayOfWeek: EnumScheduleDayOfWeek): moment.Moment | null {

        const date = dateFrom.clone().add(timeOfDay);

        while (  (date.isoWeekday() !== dayOfWeek) ) {
            date.add(1, 'day');
        }

        return date;
    }

    private static calculateDayOfMonth( dateFrom: moment.Moment,
                                        dateTo: moment.Moment,
                                        timeOfDay: moment.Duration,
                                        dayOfWeek: EnumScheduleDayOfWeek,
                                        occurrence: EnumScheduleDayOccurrenceInMonth): moment.Moment | null {

        // console.log('--------------');

        let date = dateFrom.clone().add(timeOfDay);
        let occurrenceRequired = 0;
        let dayAdjustment = 1;

        switch ( occurrence ) {
            case EnumScheduleDayOccurrenceInMonth.first:
                occurrenceRequired = 1;
                break;

            case EnumScheduleDayOccurrenceInMonth.second:
                occurrenceRequired = 2;
                break;

            case EnumScheduleDayOccurrenceInMonth.third:
                occurrenceRequired = 3;
                break;

            case EnumScheduleDayOccurrenceInMonth.fourth:
                occurrenceRequired = 4;
                break;

            case EnumScheduleDayOccurrenceInMonth.last:
                date = dateTo.clone().startOf('day').add(timeOfDay);
                occurrenceRequired = 1;
                dayAdjustment = -1;
                break;
        }

        let dayOccurrenceCount = 0;
        if ((date.isoWeekday() === dayOfWeek)) {
            dayOccurrenceCount++;
        }

        while ( (dayOccurrenceCount < occurrenceRequired) || (date.isoWeekday() !== dayOfWeek) ) {
          //  console.log(`[1] date:${date.format('DD-MM-YYYY')} occurrenceRequired${occurrenceRequired} occurrence:${dayOccurrenceCount}  dayRequired${dayOfWeek} day:${date.isoWeekday()} `);

            if (date.isoWeekday() === dayOfWeek) {
                date.add(dayAdjustment, 'week');
            } else {
                date.add(dayAdjustment, 'day');
            }

            if ((date.isoWeekday() === dayOfWeek)) {
                dayOccurrenceCount++;
            }
        }

     //   console.log(`[2] date:${date.format('DD-MM-YYYY')} occurrenceRequired${occurrenceRequired} occurrence:${dayOccurrenceCount}  dayRequired${dayOfWeek} day:${date.isoWeekday()} `);

        return date;
    }
}
