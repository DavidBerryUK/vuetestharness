import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleModel                            from '../../models/ScheduleModel';

export default class EventSchedulerOnce {

    public static createDatesForSchedule(   program: ProgramModel,
                                            schedule: ScheduleModel,
                                            dateFrom: moment.Moment,
                                            dateTo: moment.Moment): Array<moment.Moment> {
        const schedules = new Array<moment.Moment>();

        const scheduleDate = this.getStartDateOnce(program, schedule, dateFrom, dateTo);
        if (scheduleDate !== null) {
            schedules.push(scheduleDate);
        }

        // console.log('returning dates');
        // console.log(schedules);
        return schedules;
    }


    private static getStartDateOnce(    program: ProgramModel,
                                        schedule: ScheduleModel,
                                        dateFrom: moment.Moment,
                                        dateTo: moment.Moment): moment.Moment | null {

        if ( schedule.specificDateTime !== null) {

            if ( schedule.specificDateTime.isAfter(dateTo)) {
                // console.log(`too late schedule.specificDateTime${schedule.specificDateTime}  dateTo:${dateTo}`);
                return null;
            }

            const finishDate = schedule.specificDateTime.clone().add(program.estimatedDuration);

            if ( finishDate.isBefore(dateFrom)) {
                // console.log(`too early finishDate${finishDate}  dateFrom:${dateFrom}`);
                return null;
            }

            // console.log(`return specific date ${schedule.specificDateTime}`);
            return schedule.specificDateTime;

        }
        // console.log('no specific date specified');
        return null;
    }
}
