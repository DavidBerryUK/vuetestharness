import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';
import ScheduleModel                            from '../../models/ScheduleModel';
import SchedulerCommonFunctions                 from './SchedulerCommonFunctions';

export default class EventSchedulerEveryMonth {

    public static createDatesForSchedule(   program: ProgramModel,
                                            schedule: ScheduleModel,
                                            dateFrom: moment.Moment,
                                            dateTo: moment.Moment): Array<moment.Moment> {

        const schedules = new Array<moment.Moment>();

        const dateIndex = dateFrom.clone().startOf('month');
        const maxDate = dateTo.clone().endOf('month');

        while (dateIndex.isBefore(maxDate)) {

            const scheduleDate = SchedulerCommonFunctions.calculateFirstDateTimeInPeriod(
                schedule.schedulePeriod,
                dateIndex,
                dateTo,
                schedule.startTime,
                schedule.scheduledDayOfWeek,
                schedule.dateOfMonth,
                schedule.scheduleDayOccurrenceInMonth);

            if ( scheduleDate !== null) {
                schedules.push(scheduleDate);
            }

            dateIndex.add(1, 'month');
        }

        return schedules;
    }

}
