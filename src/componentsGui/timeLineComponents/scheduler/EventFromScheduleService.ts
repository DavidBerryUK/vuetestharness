import { EnumSchedulePeriod }                   from '../enums/ScheduleEnums';
import EventModel                               from '../models/EventModel';
import EventSchedulerEveryMonth                 from './helpers/EventSchedulerEveryMonth';
import EventSchedulerOnce                       from './helpers/EventSchedulerOnce';
import EventSchedulerRepeater                   from './helpers/EventSchedulerRepeater';
import moment                                   from 'moment';
import ProgramModel                             from '../models/ProgramModel';
import ScheduleModel                            from '../models/ScheduleModel';


export default class EventFromScheduleService {


    /**
     * Create a list of events from a schedule
     *
     * @param {ScheduleModel} schedule
     * @param {moment.Moment} dateFrom
     * @param {moment.Moment} dateTo
     * @returns {Array<EventModel>}
     * @memberof EventFromScheduleService
     */
    public createEvents(program: ProgramModel,
                        schedule: ScheduleModel,
                        dateFrom: moment.Moment,
                        dateTo: moment.Moment): Array<EventModel> {

        const scheduleDates = this.createDatesFromSchedule(program, schedule, dateFrom, dateTo);
        const events = new Array<EventModel>();

        scheduleDates.forEach((scheduleDateTime: moment.Moment) => {
            const event = new EventModel(program, scheduleDateTime, program.estimatedDuration);
            events.push(event);
        });

        return events;
    }

    private createDatesFromSchedule(    program: ProgramModel,
                                        schedule: ScheduleModel,
                                        dateFrom: moment.Moment,
                                        dateTo: moment.Moment): Array<moment.Moment> {

        let schedules = new Array<moment.Moment>();

        switch (schedule.schedulePeriod) {
            case EnumSchedulePeriod.once:
                schedules =  EventSchedulerOnce.createDatesForSchedule(program, schedule, dateFrom, dateTo);
                break;

            case EnumSchedulePeriod.everyMonth:
                schedules = EventSchedulerEveryMonth.createDatesForSchedule(program, schedule, dateFrom, dateTo);
                break;

            case EnumSchedulePeriod.dayOfWeek:
            case EnumSchedulePeriod.everyDay:
            case EnumSchedulePeriod.everyHour:
            case EnumSchedulePeriod.everyWeekday:
            case EnumSchedulePeriod.everyWeekendDay:
                schedules =  EventSchedulerRepeater.createDatesForSchedule(program, schedule, dateFrom, dateTo);
                break;
        }

        return schedules;
    }

}

