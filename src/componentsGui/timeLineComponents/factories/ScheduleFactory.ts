import { Duration }                             from 'moment';
import { EnumScheduleDayOccurrenceInMonth }     from '../enums/ScheduleEnums';
import { EnumScheduleDayOfWeek }                from '../enums/ScheduleEnums';
import { EnumSchedulePeriod }                   from '../enums/ScheduleEnums';
import ScheduleModel                            from '../models/ScheduleModel';

export default class ScheduleFactory {

    public static everyHourAtTime(minutesPastHour: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.everyHour;
        schedule.startTime = minutesPastHour;
        return schedule;
    }

    public static everyDayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.everyDay;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyWeekdayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.everyWeekday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyWeekendDayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.everyWeekendDay;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyMondayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.monday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyTuesdayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.tuesday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyWednesdayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.wednesday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyThursdayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.thursday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyFridayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.friday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everySaturdayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.saturday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everySundayAtTime(timeOfDay: Duration): ScheduleModel {
        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.dayOfWeek;
        schedule.scheduledDayOfWeek = EnumScheduleDayOfWeek.sunday;
        schedule.startTime = timeOfDay;
        return schedule;
    }

    public static everyMonthOnDayAtTime(    dayOfWeek: EnumScheduleDayOfWeek,
                                            dayOccurrenceInMonth: EnumScheduleDayOccurrenceInMonth,
                                            timeOfDay: Duration): ScheduleModel {

        const schedule = new ScheduleModel();
        schedule.schedulePeriod = EnumSchedulePeriod.everyMonth;
        schedule.scheduledDayOfWeek = dayOfWeek;
        schedule.scheduleDayOccurrenceInMonth = dayOccurrenceInMonth;
        schedule.startTime = timeOfDay;
        return schedule;
    }

}
