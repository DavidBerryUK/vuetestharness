import { duration }                             from 'moment';
import { EnumScheduleEventStatus }              from '../../enums/ScheduleEnums';
import AggregateEventStatusByMonth              from '../AggregateEventStatusByMonth';
import AggregatorEngine                         from '@/utilities/aggregator/AggregateEngine';
import CategoryModel                            from '../../models/CategoryModel';
import EventModel                               from '../../models/EventModel';
import EventSummaryByMonthModel                 from '../../models/EventSummaryByTimeModel';
import moment                                   from 'moment';
import ProgramModel                             from '../../models/ProgramModel';


describe('AggregateEventsByMonth Tests', () => {

    const categoryHomeImprovement = new CategoryModel('hi', 'Home Improvement');
    const programGardenersWorld = new ProgramModel(
        'Gardeners World',
        'Professional gardeners offer viewers advice on how best to get the most out of their own gardens',
        moment.duration('00:05:00'),
        categoryHomeImprovement);

    const may = moment('2019-05-01 00:00:00');
    const june = moment('2019-06-01 00:00:00');
    const sept = moment('2019-09-01 00:00:00');

    test('Test EventSummaryByMonthModel model class', () => {


        const eventSuccess = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        eventSuccess.status = EnumScheduleEventStatus.success;

        const eventPending = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        eventPending.status = EnumScheduleEventStatus.pending;

        const eventUnknown = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        eventUnknown.status = EnumScheduleEventStatus.unknown;

        const eventFailed = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        eventFailed.status = EnumScheduleEventStatus.failed;

        const eventWarnings = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        eventWarnings.status = EnumScheduleEventStatus.warnings;

        const summary = new EventSummaryByMonthModel();

        const aggregator = new  AggregateEventStatusByMonth();

        aggregator.addObjectToSummary(eventFailed, summary);

        aggregator.addObjectToSummary(eventPending, summary);
        aggregator.addObjectToSummary(eventPending, summary);

        aggregator.addObjectToSummary(eventSuccess, summary);
        aggregator.addObjectToSummary(eventSuccess, summary);
        aggregator.addObjectToSummary(eventSuccess, summary);

        aggregator.addObjectToSummary(eventUnknown, summary);
        aggregator.addObjectToSummary(eventUnknown, summary);
        aggregator.addObjectToSummary(eventUnknown, summary);
        aggregator.addObjectToSummary(eventUnknown, summary);

        aggregator.addObjectToSummary(eventWarnings, summary);
        aggregator.addObjectToSummary(eventWarnings, summary);
        aggregator.addObjectToSummary(eventWarnings, summary);
        aggregator.addObjectToSummary(eventWarnings, summary);
        aggregator.addObjectToSummary(eventWarnings, summary);

        expect(summary.failedCount).toBe(1);
        expect(summary.pendingCount).toBe(2);
        expect(summary.successCount).toBe(3);
        expect(summary.unknownCount).toBe(4);
        expect(summary.warningsCount).toBe(5);

    });

    test('Test EventSummaryByMonthModel AggregateEventStatusByMonth', () => {

        const maySuccess01 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        maySuccess01.status = EnumScheduleEventStatus.success;

        const mayPending01 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayPending01.status = EnumScheduleEventStatus.pending;

        const mayPending02 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayPending02.status = EnumScheduleEventStatus.pending;

        const mayUnknown01 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayUnknown01.status = EnumScheduleEventStatus.unknown;

        const mayUnknown02 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayUnknown02.status = EnumScheduleEventStatus.unknown;

        const mayUnknown03 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayUnknown03.status = EnumScheduleEventStatus.unknown;

        const mayFailed01 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayFailed01.status = EnumScheduleEventStatus.failed;

        const mayFailed02 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayFailed02.status = EnumScheduleEventStatus.failed;

        const mayFailed03 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayFailed03.status = EnumScheduleEventStatus.failed;

        const mayFailed04 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayFailed04.status = EnumScheduleEventStatus.failed;

        const mayWarnings01 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayWarnings01.status = EnumScheduleEventStatus.warnings;

        const mayWarnings02 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayWarnings02.status = EnumScheduleEventStatus.warnings;

        const mayWarnings03 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayWarnings03.status = EnumScheduleEventStatus.warnings;

        const mayWarnings04 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayWarnings04.status = EnumScheduleEventStatus.warnings;

        const mayWarnings05 = new EventModel(programGardenersWorld, may, duration('00:00:00'));
        mayWarnings05.status = EnumScheduleEventStatus.warnings;

        const juneWarnings01 = new EventModel(programGardenersWorld, june, duration('00:00:00'));
        juneWarnings01.status = EnumScheduleEventStatus.warnings;

        const juneSuccess01 = new EventModel(programGardenersWorld, june, duration('00:00:00'));
        juneSuccess01.status = EnumScheduleEventStatus.success;

        const juneSuccess02 = new EventModel(programGardenersWorld, june, duration('00:00:00'));
        juneSuccess02.status = EnumScheduleEventStatus.success;




        const septemberFailed01 = new EventModel(programGardenersWorld, sept, duration('00:00:00'));
        septemberFailed01.status = EnumScheduleEventStatus.failed;


        const list: Array<EventModel> = [   maySuccess01,
                                            mayPending01,   mayPending02,
                                            mayUnknown01,   mayUnknown02,   mayUnknown03,
                                            mayFailed01,    mayFailed02,    mayFailed03,    mayFailed04,
                                            mayWarnings01,  mayWarnings02,  mayWarnings03,  mayWarnings04, mayWarnings05,
                                            juneWarnings01,
                                            juneSuccess01,  juneSuccess02,
                                            septemberFailed01];

        const summary =  AggregatorEngine.aggregate(list, new AggregateEventStatusByMonth());

        expect(summary).toBeDefined();
        expect(summary.length).toBe(3);
        // may
        expect(summary[0].successCount).toBe(1);
        expect(summary[0].pendingCount).toBe(2);
        expect(summary[0].unknownCount).toBe(3);
        expect(summary[0].failedCount).toBe(4);
        expect(summary[0].warningsCount).toBe(5);
        // june
        expect(summary[1].warningsCount).toBe(1);
        expect(summary[1].successCount).toBe(2);
        expect(summary[1].pendingCount).toBe(0);
        expect(summary[1].unknownCount).toBe(0);
        expect(summary[1].failedCount).toBe(0);
        // september
        expect(summary[2].failedCount).toBe(1);
        expect(summary[2].warningsCount).toBe(0);
        expect(summary[2].successCount).toBe(0);
        expect(summary[2].pendingCount).toBe(0);
        expect(summary[2].unknownCount).toBe(0);
    });


});
