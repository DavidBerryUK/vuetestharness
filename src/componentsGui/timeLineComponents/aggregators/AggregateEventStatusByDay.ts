import { EnumScheduleEventStatus }              from '../enums/ScheduleEnums';
import { IAggregateFunction }                   from '@/utilities/aggregator/IAggregateFunction';
import EventModel                               from '../models/EventModel';
import EventSummaryByTimeModel                  from '../models/EventSummaryByTimeModel';


export default class AggregateEventStatusByDay implements IAggregateFunction<EventModel, EventSummaryByTimeModel> {

    public createAggregationKey(object: EventModel): string {
        return `${object.startDateTime.year()}:${object.startDateTime.dayOfYear()}`;
    }

    public createSummaryObject(object: EventModel): EventSummaryByTimeModel {
        const instance = new EventSummaryByTimeModel();
        instance.timePeriodStart = object.startDateTime.startOf('day');
        return instance;
    }

    public addObjectToSummary(object: EventModel, summary: EventSummaryByTimeModel): EventSummaryByTimeModel {

        switch ( object.status ) {
            case EnumScheduleEventStatus.unknown:
            summary.unknownCount++;
            break;

            case EnumScheduleEventStatus.pending:
            summary.pendingCount++;
            break;

            case EnumScheduleEventStatus.success:
            summary.successCount++;
            break;

            case EnumScheduleEventStatus.warnings:
            summary.warningsCount++;
            break;

            case EnumScheduleEventStatus.failed:
            summary.failedCount++;
            break;

        }
        return summary;
    }
}
