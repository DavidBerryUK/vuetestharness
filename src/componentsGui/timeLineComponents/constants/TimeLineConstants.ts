import ColourModel from '@/componentsGui/coloursComponents/models/ColourModel';

export default class TimeLineConstants {

    public static ColourSuccessDark = ColourModel.fromNamedHex('Success', '007E33');
    public static ColourWarningDark = ColourModel.fromNamedHex('Warning', 'FF8800');
    public static ColourFailureDark = ColourModel.fromNamedHex('Error', 'CC0000');
    public static ColourPendingDark = ColourModel.fromNamedHex('Pending', '0099CC');
    public static ColourUnknownDark = ColourModel.fromNamedHex('Pending', '607d8b');

}
