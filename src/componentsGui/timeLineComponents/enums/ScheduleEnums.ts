export enum EnumSchedulePeriod {
    once,
    everyHour,
    everyDay,
    everyWeekday,
    everyWeekendDay,
    everyWeek,
    dayOfWeek,
    everyMonth,
}

export enum EnumScheduleDayOfWeek {
    notApplicable = -1,
    monday = 1,
    tuesday = 2,
    wednesday = 3,
    thursday = 4,
    friday = 5,
    saturday = 6,
    sunday = 7,
}

export enum EnumScheduleDayOccurrenceInMonth {
    notApplicable,
    first,
    second,
    third,
    fourth,
    last,
}

export enum EnumPresentationVisualScale {
    quarterHour = 'Quarter Hour',
    halfHour = 'Half Hour',
    hour = 'Hour',
    halfDay = 'Half Day',
    fullDay = 'Day',
    fiveDays = '5 Days',
    week = 'Week',
    twoWeeks = '2 Weeks',
    fourWeeks = '4 Weeks',
}

export enum EnumScheduleEventStatus {
    unknown,
    pending,
    success,
    warnings,
    failed,
}

export enum EnumSummaryTimeLineSummaryStyle {
    none = 'None',
    lineGraph = 'Line Graph',
    summaryPerMonth = 'Summary Per Month',
    pieChartPerMonth = 'Pie Chart Per Month',
}

