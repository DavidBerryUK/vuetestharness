import CategoryModel from '../models/CategoryModel';

export default class CategoryModelServices {

    public static doesCategoryExistInArray(category: CategoryModel, array: Array<CategoryModel>) {
        const filtered = array.filter((item: CategoryModel) => item.name === category.name);
        return filtered.length > 0;
    }

    public static mergeAndDeduplicate(a: Array<CategoryModel>, b: Array<CategoryModel>): Array<CategoryModel> {
        let data = new Array<CategoryModel>();
        data.concat(a);
        data.concat(b);
        data = data.filter((category, index, array) => index === array.indexOf(category));
        return data;
    }


}
