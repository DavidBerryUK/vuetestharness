import ProgramModel from '../models/ProgramModel';

export default class ProgramModelServices {

    public static doesProgramExistInArray(program: ProgramModel, array: Array<ProgramModel>) {
        const filtered = array.filter((item: ProgramModel) => item.name === program.name);
        return filtered.length > 0;
    }

    /**
     * Add a single program or array of program
     * to the timetable if do not already exist
     *
     * @param {ProgramModel} program
     * @returns
     * @memberof TimeTableModel
     */
    public static addProgramToArray(programs: Array<ProgramModel>, program: ProgramModel | Array<ProgramModel>) {

        if ( program instanceof Array) {
            program.forEach((item: ProgramModel) => {
                if ( ! ProgramModelServices.doesProgramExistInArray(item, programs)) {
                    programs.push(item);
                }
            });
        } else {
            if ( !ProgramModelServices.doesProgramExistInArray(program, programs)) {
                programs.push(program);
            }
        }
    }
}
