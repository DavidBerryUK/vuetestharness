import { EnumPresentationVisualScale }          from '../enums/ScheduleEnums';
import DateRangeModel                           from '../models/DateRangeModel';
import moment                                   from 'moment';


export default class  VisualScaleHelper {

    /**
     * Calculate the begin and end date for a
     * given target date and visual scale.
     *
     * The target date will be surrounded by a number of
     * days depending on the days in the scale
     *
     * The result will be limited to the supplied date range
     *
     * @static
     * @param {EnumPresentationVisualScale} scale
     * @param {moment.Moment} targetDate
     * @param {moment.Moment} rangeFrom
     * @param {moment.Moment} rangeTo
     * @memberof VisualScaleHelper
     */
    public static CalculateDateRange(scale: EnumPresentationVisualScale,
                                     targetDate: moment.Moment,
                                     rangeFrom: moment.Moment,
                                     rangeTo: moment.Moment): DateRangeModel {

        const days = this.DaysInVisualScale(scale);
        const daysBefore = Math.floor((days - 1) / 2);
        const daysAfter = (days - 1) - daysBefore;
        let rangeStart = targetDate.clone().add(-daysBefore, 'days');
        let rangeEnd = targetDate.clone().add(daysAfter, 'days');

        if ( rangeStart.isBefore(rangeFrom)) {
            rangeStart = rangeFrom.clone();
            rangeEnd = rangeStart.clone().add(days - 1, 'days');
        }

        if ( rangeEnd.isAfter(rangeTo)) {
            rangeEnd = rangeTo.clone();
            rangeStart = rangeEnd.clone().add( - (days - 1), 'days');
        }

        return  new DateRangeModel(rangeStart, rangeEnd);
    }


    /**
     * Return the number of days in a visual scale
     *
     * @static
     * @param {EnumPresentationVisualScale} scale
     * @returns {number}
     * @memberof VisualScaleHelper
     */
    public static DaysInVisualScale(scale: EnumPresentationVisualScale): number {
        let days = 1;
        switch (scale) {
            case EnumPresentationVisualScale.quarterHour:
            case EnumPresentationVisualScale.halfHour:
            case EnumPresentationVisualScale.hour:
            case EnumPresentationVisualScale.halfDay:
            case EnumPresentationVisualScale.fullDay:
                days = 1;
                break;

            case EnumPresentationVisualScale.fiveDays:
                days = 5;
                break;

            case EnumPresentationVisualScale.week:
                days = 7;
                break;

            case EnumPresentationVisualScale.twoWeeks:
                days = 14;
                break;

            case EnumPresentationVisualScale.fourWeeks:
                days = 28;
                break;
        }
        return days;
    }
}
