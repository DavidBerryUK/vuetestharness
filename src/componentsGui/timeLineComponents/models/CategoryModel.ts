
/**
 * A program belongs to a category, a category can
 * contain many programs
 * @export
 * @class CategoryModel
 */
export default class CategoryModel {

    public id: string;
    public name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}
