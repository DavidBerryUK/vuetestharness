import { duration }                             from 'moment';
import { EnumScheduleDayOccurrenceInMonth }     from '../enums/ScheduleEnums';
import { EnumScheduleDayOfWeek }                from '../enums/ScheduleEnums';
import { EnumSchedulePeriod }                   from '../enums/ScheduleEnums';
import moment                                   from 'moment';


export default class ScheduleModel {
    public schedulePeriod: EnumSchedulePeriod;
    public scheduledDayOfWeek: EnumScheduleDayOfWeek;
    public scheduleDayOccurrenceInMonth: EnumScheduleDayOccurrenceInMonth;
    public startTime: moment.Duration;


    /**
     * for monthly schedules
     *
     * @type {(number | null)}
     * @memberof ScheduleModel
     */
    public dateOfMonth: number | null;


    /**
     * for once off schedules
     *
     * @type {(Moment.moment | null)}
     * @memberof ScheduleModel
     */
    public specificDateTime: moment.Moment | null;


    constructor() {
        this.schedulePeriod = EnumSchedulePeriod.once;
        this.scheduledDayOfWeek = EnumScheduleDayOfWeek.notApplicable;
        this.scheduleDayOccurrenceInMonth = EnumScheduleDayOccurrenceInMonth.notApplicable;
        this.startTime = duration(0, 'minutes');
        this.dateOfMonth = null;
        this.specificDateTime = null;
    }
}
