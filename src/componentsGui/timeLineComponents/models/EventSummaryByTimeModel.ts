import moment                                   from 'moment';

export default class EventSummaryByTimeModel {

    public timePeriodStart: moment.Moment;
    public unknownCount: number;
    public pendingCount: number;
    public successCount: number;
    public warningsCount: number;
    public failedCount: number;

    constructor() {
        this.timePeriodStart =  moment();
        this.unknownCount = 0;
        this.pendingCount = 0;
        this.successCount = 0;
        this.warningsCount = 0;
        this.failedCount = 0;
    }
}
