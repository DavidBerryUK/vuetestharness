import { EnumScheduleEventStatus }              from '../enums/ScheduleEnums';
import moment                                   from 'moment';
import ProgramModel                             from './ProgramModel';


/**
 * contains the instance of a program upon the time table
 *
 * @export
 * @class TimeTableItemModel
 */
export default class EventModel {
    public program: ProgramModel;
    public startDateTime: moment.Moment;
    public estimatedDuration: moment.Duration;
    public estimatedEndDateTime: moment.Moment;

    public actualDuration: moment.Duration | undefined;

    public status: EnumScheduleEventStatus;

    constructor(program: ProgramModel, startDateTime: moment.Moment, estimatedDuration: moment.Duration) {
        this.program = program;
        this.startDateTime = startDateTime;
        this.estimatedDuration = estimatedDuration;
        this.estimatedEndDateTime = moment(startDateTime).add(estimatedDuration);
        this.status = EnumScheduleEventStatus.unknown;
    }
}
