import moment                                   from 'moment';

export default class DateRangeModel {
    public start: moment.Moment;
    public end: moment.Moment;

    constructor(start: moment.Moment, end: moment.Moment) {
        this.start = start;
        this.end = end;
    }
}
