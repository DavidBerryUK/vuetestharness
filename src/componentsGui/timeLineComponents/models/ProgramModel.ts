import CategoryModel                            from './CategoryModel';
import EventModel                               from './EventModel';
import moment                                   from 'moment';
import ScheduleModel                            from './ScheduleModel';


/**
 * Contains information about a program, including its name, category
 * and schedules. A program can have more than one schedule
 *
 * @export
 * @class ProgramModel
 */
export default class ProgramModel {

    public name: string;
    public description: string;
    public schedules: Array<ScheduleModel>;
    public category: CategoryModel;

    public estimatedDuration: moment.Duration;

    public events: Array<EventModel>;

    constructor(name: string, description: string, estimatedDuration: moment.Duration, category: CategoryModel) {
        this.name = name;
        this.description = description;
        this.schedules = Array<ScheduleModel>();
        this.category = category;
        this.events = Array<EventModel>();
        this.estimatedDuration = estimatedDuration;
    }

    public addSchedule(schedule: ScheduleModel) {
        this.schedules.push(schedule);
    }
}
