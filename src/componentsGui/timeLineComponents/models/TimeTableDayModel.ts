import EventModel                       from './EventModel';


/**
 * contains the items on the time table for a specific day
 *
 * @export
 * @class TimeTableDayModel
 */
export default class TimeTableDayModel {

    public timeTableItems: Array<EventModel>;

    constructor() {
        this.timeTableItems = new Array<EventModel>();
    }
}
