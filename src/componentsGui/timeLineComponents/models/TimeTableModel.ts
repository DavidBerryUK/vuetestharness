import CategoryModel                            from './CategoryModel';
import CategoryModelServices                    from '../services/CategoryModelServices';
import ProgramModel                             from './ProgramModel';
import ProgramModelServices                     from '../services/ProgramModelServices';
import TimeTableDayModel                        from './TimeTableDayModel';



/**
 * the highest level of classes that contains time table information
 *
 * @export
 * @class TimeTableModel
 */
export default class TimeTableModel {

    public categories: Array<CategoryModel>;
    public instancesByDays: Array<TimeTableDayModel>;
    public programs: Array<ProgramModel>;

    constructor() {
        this.categories = new Array<CategoryModel>();
        this.programs = new Array<ProgramModel>();
        this.instancesByDays = new Array<TimeTableDayModel>();
    }


    /**
     * Add a single program or array of program
     * to the timetable if do not already exist
     *
     * @param {ProgramModel} program
     * @returns
     * @memberof TimeTableModel
     */
    public addProgram(program: ProgramModel | Array<ProgramModel>) {

        ProgramModelServices.addProgramToArray(this.programs, program);
        this.updateCategories();
    }


    private updateCategories() {

        const allCategories = this.programs.map((program) => program.category);

        const merged = this.categories.concat(allCategories);
        // deduplicate
        this.categories = CategoryModelServices.mergeAndDeduplicate(this.categories, merged);
    }


}
