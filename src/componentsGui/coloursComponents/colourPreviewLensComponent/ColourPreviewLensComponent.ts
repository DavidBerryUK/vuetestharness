import { Prop }                                 from 'vue-property-decorator';
import ColourList                               from '../constants/ColourList';
import ColourModel                              from '../models/ColourModel';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';
import WebNamedColourConstants                  from '../constants/WebNamedColourConstants';

@Component({
    components: {
    },
  })
export default class ColourPreviewLensComponent extends Vue   {

    private coordsX: number = 0;
    private coordsY: number = 0;

    private active: boolean = false;

    @Prop()
    public colour!: ColourModel;

    @Prop()
    public colourList!: Array<ColourList>;

    @Prop()
    public cellWidth!: number;

    @Prop()
    public cellHeight!: number;

    @Prop()
    public marginTop!: number;

    @Prop()
    public marginLeft!: number;

    private activeColour: ColourModel = WebNamedColourConstants.White;

    private visible: boolean = true;

    private mounted() {
      if ( this.colour) {
        this.activeColour = this.colour;
      }
    }

    private onMouseEnter(event: MouseEvent) {
      this.active = true;
      this.processMouseEvent(event);
    }

    private onMouseMove(event: MouseEvent) {
      if (! this.active) {
        return;
      }
      this.processMouseEvent(event);
    }
    private onMouseLeave(event: MouseEvent) {
      this.active = false;
    }

    private onMouseClick(event: MouseEvent) {
      if (this.visible) {
        console.log('selected colour');
        this.$emit('colourChanged', this.activeColour);
      }
    }

    private get swatchStyle(): string {
      return `background-color:${this.activeColour.hex};`;
    }

    private processMouseEvent(event: MouseEvent) {
      const container = this.$refs.container as HTMLDivElement;
      const containerRect = container.getBoundingClientRect();
      const x = event.pageX;
      const y = event.pageY;
      this.coordsX = x - containerRect.left - 80;
      this.coordsY = y - containerRect.top - 195;

     // console.log(`cx:${containerRect.left}  px:${event.pageX}   ox:${event.offsetX}   cx:${event.clientX}`);

      if (this.colourList) {
        const col = Math.floor(( event.pageX - containerRect.left - this.marginLeft) / this.cellWidth);
        const row =  Math.floor(( event.pageY - containerRect.top - this.marginTop) / this.cellHeight);

        this.visible = false;
        if (row >= 0 && row < this.colourList.length ) {
          if ( col >= 0 && col < this.colourList[row].colours.length) {
            this.activeColour = this.colourList[row].colours[col];
            this.visible = true;
          }
        }
      }
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-colour-preview-lens', ColourPreviewLensComponent);
