import { Prop }                                 from 'vue-property-decorator';
import ColourModel                              from '../models/ColourModel';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';

@Component({
    components: {
    },
  })
export default class ColourSwatchComponent extends Vue   {

    @Prop()
    public value!: ColourModel;


    private get swatchStyle(): string {
      return `background-color:${this.value.hex};`;
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-colour-swatch', ColourSwatchComponent);
