import ColourModel from '../models/ColourModel';

export default class ColourList {

    public name: string;
    public colours: Array<ColourModel>;

    constructor(name: string, colours: Array<ColourModel>) {
        this.name = name;
        this.colours = colours;
    }

}
