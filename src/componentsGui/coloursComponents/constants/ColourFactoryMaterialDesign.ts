import ColourModel                                   from '../models/ColourModel';
import ColourList                               from './ColourList';
import MaterialDesignColorConstants             from './MaterialDesignColourConstants';

export default class ColourFactoryMaterialDesign {

    public static get materialDesignColours(): Array<ColourList> {
        const data = new Array<ColourList>();

        data.push(new ColourList('Red', this.MdRedList));
        data.push(new ColourList('Pink', this.MdPinkList));
        data.push(new ColourList('Purple', this.MdPurpleList));
        data.push(new ColourList('Deep Purple', this.MdDeepPurpleList));
        data.push(new ColourList('Indigo', this.MdIndigoList));
        data.push(new ColourList('Blue', this.MdBlueList));
        data.push(new ColourList('Light Blue', this.MdLightBlueList));
        data.push(new ColourList('Cyan', this.MdCyanList));
        data.push(new ColourList('Teal', this.MdTealList));
        data.push(new ColourList('Green', this.MdGreenList));
        data.push(new ColourList('Light Green', this.MdLightGreenList));
        data.push(new ColourList('Lime', this.MdLimeList));
        data.push(new ColourList('Yellow', this.MdYellowList));
        data.push(new ColourList('Amber', this.MdAmberList));
        data.push(new ColourList('Orange', this.MdOrangeList));
        data.push(new ColourList('Deep Orange', this.MdDeepOrangeList));
        data.push(new ColourList('Brown', this.MdBrownList));
        data.push(new ColourList('Grey', this.MdGreyList));
        data.push(new ColourList('Blue Grey', this.MdBlueGreyList));
        data.push(new ColourList('Mono', this.MdMonoList));

        return data;
    }

    public static get MdRedList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.red50,
            MaterialDesignColorConstants.red100,
            MaterialDesignColorConstants.red200,
            MaterialDesignColorConstants.red300,
            MaterialDesignColorConstants.red400,
            MaterialDesignColorConstants.red500,
            MaterialDesignColorConstants.red600,
            MaterialDesignColorConstants.red700,
            MaterialDesignColorConstants.red800,
            MaterialDesignColorConstants.red900,
            MaterialDesignColorConstants.redA100,
            MaterialDesignColorConstants.redA200,
            MaterialDesignColorConstants.redA400,
            MaterialDesignColorConstants.redA700,
                ];
    }

    public static get MdPinkList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.pink50,
            MaterialDesignColorConstants.pink100,
            MaterialDesignColorConstants.pink200,
            MaterialDesignColorConstants.pink300,
            MaterialDesignColorConstants.pink400,
            MaterialDesignColorConstants.pink500,
            MaterialDesignColorConstants.pink600,
            MaterialDesignColorConstants.pink700,
            MaterialDesignColorConstants.pink800,
            MaterialDesignColorConstants.pink900,
            MaterialDesignColorConstants.pinkA100,
            MaterialDesignColorConstants.pinkA200,
            MaterialDesignColorConstants.pinkA400,
            MaterialDesignColorConstants.pinkA700,
                ];
    }

    public static get MdPurpleList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.purple50,
            MaterialDesignColorConstants.purple100,
            MaterialDesignColorConstants.purple200,
            MaterialDesignColorConstants.purple300,
            MaterialDesignColorConstants.purple400,
            MaterialDesignColorConstants.purple500,
            MaterialDesignColorConstants.purple600,
            MaterialDesignColorConstants.purple700,
            MaterialDesignColorConstants.purple800,
            MaterialDesignColorConstants.purple900,
            MaterialDesignColorConstants.purpleA100,
            MaterialDesignColorConstants.purpleA200,
            MaterialDesignColorConstants.purpleA400,
            MaterialDesignColorConstants.purpleA700,
                ];
    }

    public static get MdDeepPurpleList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.deepPurple50,
            MaterialDesignColorConstants.deepPurple100,
            MaterialDesignColorConstants.deepPurple200,
            MaterialDesignColorConstants.deepPurple300,
            MaterialDesignColorConstants.deepPurple400,
            MaterialDesignColorConstants.deepPurple500,
            MaterialDesignColorConstants.deepPurple600,
            MaterialDesignColorConstants.deepPurple700,
            MaterialDesignColorConstants.deepPurple800,
            MaterialDesignColorConstants.deepPurple900,
            MaterialDesignColorConstants.deepPurpleA100,
            MaterialDesignColorConstants.deepPurpleA200,
            MaterialDesignColorConstants.deepPurpleA400,
            MaterialDesignColorConstants.deepPurpleA700,
                ];
    }

    public static get MdIndigoList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.indigo50,
            MaterialDesignColorConstants.indigo100,
            MaterialDesignColorConstants.indigo200,
            MaterialDesignColorConstants.indigo300,
            MaterialDesignColorConstants.indigo400,
            MaterialDesignColorConstants.indigo500,
            MaterialDesignColorConstants.indigo600,
            MaterialDesignColorConstants.indigo700,
            MaterialDesignColorConstants.indigo800,
            MaterialDesignColorConstants.indigo900,
            MaterialDesignColorConstants.indigoA100,
            MaterialDesignColorConstants.indigoA200,
            MaterialDesignColorConstants.indigoA400,
            MaterialDesignColorConstants.indigoA700,
                ];
    }

    public static get MdBlueList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.blue50,
            MaterialDesignColorConstants.blue100,
            MaterialDesignColorConstants.blue200,
            MaterialDesignColorConstants.blue300,
            MaterialDesignColorConstants.blue400,
            MaterialDesignColorConstants.blue500,
            MaterialDesignColorConstants.blue600,
            MaterialDesignColorConstants.blue700,
            MaterialDesignColorConstants.blue800,
            MaterialDesignColorConstants.blue900,
            MaterialDesignColorConstants.blueA100,
            MaterialDesignColorConstants.blueA200,
            MaterialDesignColorConstants.blueA400,
            MaterialDesignColorConstants.blueA700,
                ];
    }

    public static get MdLightBlueList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.lightBlue50,
            MaterialDesignColorConstants.lightBlue100,
            MaterialDesignColorConstants.lightBlue200,
            MaterialDesignColorConstants.lightBlue300,
            MaterialDesignColorConstants.lightBlue400,
            MaterialDesignColorConstants.lightBlue500,
            MaterialDesignColorConstants.lightBlue600,
            MaterialDesignColorConstants.lightBlue700,
            MaterialDesignColorConstants.lightBlue800,
            MaterialDesignColorConstants.lightBlue900,
            MaterialDesignColorConstants.lightBlueA100,
            MaterialDesignColorConstants.lightBlueA200,
            MaterialDesignColorConstants.lightBlueA400,
            MaterialDesignColorConstants.lightBlueA700,
                ];
    }

    public static get MdCyanList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.cyan50,
            MaterialDesignColorConstants.cyan100,
            MaterialDesignColorConstants.cyan200,
            MaterialDesignColorConstants.cyan300,
            MaterialDesignColorConstants.cyan400,
            MaterialDesignColorConstants.cyan500,
            MaterialDesignColorConstants.cyan600,
            MaterialDesignColorConstants.cyan700,
            MaterialDesignColorConstants.cyan800,
            MaterialDesignColorConstants.cyan900,
            MaterialDesignColorConstants.cyanA100,
            MaterialDesignColorConstants.cyanA200,
            MaterialDesignColorConstants.cyanA400,
            MaterialDesignColorConstants.cyanA700,
                ];
    }

    public static get MdTealList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.teal50,
            MaterialDesignColorConstants.teal100,
            MaterialDesignColorConstants.teal200,
            MaterialDesignColorConstants.teal300,
            MaterialDesignColorConstants.teal400,
            MaterialDesignColorConstants.teal500,
            MaterialDesignColorConstants.teal600,
            MaterialDesignColorConstants.teal700,
            MaterialDesignColorConstants.teal800,
            MaterialDesignColorConstants.teal900,
            MaterialDesignColorConstants.tealA100,
            MaterialDesignColorConstants.tealA200,
            MaterialDesignColorConstants.tealA400,
            MaterialDesignColorConstants.tealA700,
                ];
    }

    public static get MdGreenList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.green50,
            MaterialDesignColorConstants.green100,
            MaterialDesignColorConstants.green200,
            MaterialDesignColorConstants.green300,
            MaterialDesignColorConstants.green400,
            MaterialDesignColorConstants.green500,
            MaterialDesignColorConstants.green600,
            MaterialDesignColorConstants.green700,
            MaterialDesignColorConstants.green800,
            MaterialDesignColorConstants.green900,
            MaterialDesignColorConstants.greenA100,
            MaterialDesignColorConstants.greenA200,
            MaterialDesignColorConstants.greenA400,
            MaterialDesignColorConstants.greenA700,
                ];
    }

    public static get MdLightGreenList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.lightGreen50,
            MaterialDesignColorConstants.lightGreen100,
            MaterialDesignColorConstants.lightGreen200,
            MaterialDesignColorConstants.lightGreen300,
            MaterialDesignColorConstants.lightGreen400,
            MaterialDesignColorConstants.lightGreen500,
            MaterialDesignColorConstants.lightGreen600,
            MaterialDesignColorConstants.lightGreen700,
            MaterialDesignColorConstants.lightGreen800,
            MaterialDesignColorConstants.lightGreen900,
            MaterialDesignColorConstants.lightGreenA100,
            MaterialDesignColorConstants.lightGreenA200,
            MaterialDesignColorConstants.lightGreenA400,
            MaterialDesignColorConstants.lightGreenA700,
                ];
    }

    public static get MdLimeList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.lime50,
            MaterialDesignColorConstants.lime100,
            MaterialDesignColorConstants.lime200,
            MaterialDesignColorConstants.lime300,
            MaterialDesignColorConstants.lime400,
            MaterialDesignColorConstants.lime500,
            MaterialDesignColorConstants.lime600,
            MaterialDesignColorConstants.lime700,
            MaterialDesignColorConstants.lime800,
            MaterialDesignColorConstants.lime900,
            MaterialDesignColorConstants.limeA100,
            MaterialDesignColorConstants.limeA200,
            MaterialDesignColorConstants.limeA400,
            MaterialDesignColorConstants.limeA700,
                ];
    }

    public static get MdYellowList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.yellow50,
            MaterialDesignColorConstants.yellow100,
            MaterialDesignColorConstants.yellow200,
            MaterialDesignColorConstants.yellow300,
            MaterialDesignColorConstants.yellow400,
            MaterialDesignColorConstants.yellow500,
            MaterialDesignColorConstants.yellow600,
            MaterialDesignColorConstants.yellow700,
            MaterialDesignColorConstants.yellow800,
            MaterialDesignColorConstants.yellow900,
            MaterialDesignColorConstants.yellowA100,
            MaterialDesignColorConstants.yellowA200,
            MaterialDesignColorConstants.yellowA400,
            MaterialDesignColorConstants.yellowA700,
                ];
    }

    public static get MdAmberList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.amber50,
            MaterialDesignColorConstants.amber100,
            MaterialDesignColorConstants.amber200,
            MaterialDesignColorConstants.amber300,
            MaterialDesignColorConstants.amber400,
            MaterialDesignColorConstants.amber500,
            MaterialDesignColorConstants.amber600,
            MaterialDesignColorConstants.amber700,
            MaterialDesignColorConstants.amber800,
            MaterialDesignColorConstants.amber900,
            MaterialDesignColorConstants.amberA100,
            MaterialDesignColorConstants.amberA200,
            MaterialDesignColorConstants.amberA400,
            MaterialDesignColorConstants.amberA700,
                ];
    }

    public static get MdOrangeList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.orange50,
            MaterialDesignColorConstants.orange100,
            MaterialDesignColorConstants.orange200,
            MaterialDesignColorConstants.orange300,
            MaterialDesignColorConstants.orange400,
            MaterialDesignColorConstants.orange500,
            MaterialDesignColorConstants.orange600,
            MaterialDesignColorConstants.orange700,
            MaterialDesignColorConstants.orange800,
            MaterialDesignColorConstants.orange900,
            MaterialDesignColorConstants.orangeA100,
            MaterialDesignColorConstants.orangeA200,
            MaterialDesignColorConstants.orangeA400,
            MaterialDesignColorConstants.orangeA700,
                ];
    }

    public static get MdDeepOrangeList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.deepOrange50,
            MaterialDesignColorConstants.deepOrange100,
            MaterialDesignColorConstants.deepOrange200,
            MaterialDesignColorConstants.deepOrange300,
            MaterialDesignColorConstants.deepOrange400,
            MaterialDesignColorConstants.deepOrange500,
            MaterialDesignColorConstants.deepOrange600,
            MaterialDesignColorConstants.deepOrange700,
            MaterialDesignColorConstants.deepOrange800,
            MaterialDesignColorConstants.deepOrange900,
            MaterialDesignColorConstants.deepOrangeA100,
            MaterialDesignColorConstants.deepOrangeA200,
            MaterialDesignColorConstants.deepOrangeA400,
            MaterialDesignColorConstants.deepOrangeA700,
                ];
    }

    public static get MdBrownList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.brown50,
            MaterialDesignColorConstants.brown100,
            MaterialDesignColorConstants.brown200,
            MaterialDesignColorConstants.brown300,
            MaterialDesignColorConstants.brown400,
            MaterialDesignColorConstants.brown500,
            MaterialDesignColorConstants.brown600,
            MaterialDesignColorConstants.brown700,
            MaterialDesignColorConstants.brown800,
            MaterialDesignColorConstants.brown900,
                ];
    }

    public static get MdGreyList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.grey50,
            MaterialDesignColorConstants.grey100,
            MaterialDesignColorConstants.grey200,
            MaterialDesignColorConstants.grey300,
            MaterialDesignColorConstants.grey400,
            MaterialDesignColorConstants.grey500,
            MaterialDesignColorConstants.grey600,
            MaterialDesignColorConstants.grey700,
            MaterialDesignColorConstants.grey800,
            MaterialDesignColorConstants.grey900,
                ];
    }

    public static get MdBlueGreyList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.blueGrey50,
            MaterialDesignColorConstants.blueGrey100,
            MaterialDesignColorConstants.blueGrey200,
            MaterialDesignColorConstants.blueGrey300,
            MaterialDesignColorConstants.blueGrey400,
            MaterialDesignColorConstants.blueGrey500,
            MaterialDesignColorConstants.blueGrey600,
            MaterialDesignColorConstants.blueGrey700,
            MaterialDesignColorConstants.blueGrey800,
            MaterialDesignColorConstants.blueGrey900,
                ];
    }

    public static get MdMonoList(): Array<ColourModel> {
        return [
            MaterialDesignColorConstants.white,
            MaterialDesignColorConstants.black,
                ];
    }

}
