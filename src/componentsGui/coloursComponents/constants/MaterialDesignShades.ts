export default class MaterialDesignShades {
  //
  // list of color modifiers that change the shade of colours
  //
  public static lighten5: string = 'lighten-5';
  public static lighten4: string = 'lighten-4';
  public static lighten3: string = 'lighten-3';
  public static lighten2: string = 'lighten-2';
  public static lighten1: string = 'lighten-1';
  public static neutral: string = '';
  public static darken1: string = 'darken-1';
  public static darken2: string = 'darken-2';
  public static darken3: string = 'darken-3';
  public static darken4: string = 'darken-4';
  public static accent1: string = 'accent-1';
  public static accent2: string = 'accent-2';
  public static accent3: string = 'accent-3';
  public static accent4: string = 'accent-4';
  public static shadeRange: Array<string> = [
    MaterialDesignShades.lighten5,
    MaterialDesignShades.lighten4,
    MaterialDesignShades.lighten3,
    MaterialDesignShades.lighten2,
    MaterialDesignShades.lighten1,
    MaterialDesignShades.neutral,
    MaterialDesignShades.darken1,
    MaterialDesignShades.darken2,
    MaterialDesignShades.darken3,
    MaterialDesignShades.darken4,
  ];


public static shadeRangeWithAccent: Array<string> = [
  MaterialDesignShades.lighten5,
  MaterialDesignShades.lighten4,
  MaterialDesignShades.lighten3,
  MaterialDesignShades.lighten2,
  MaterialDesignShades.lighten1,
  MaterialDesignShades.neutral,
  MaterialDesignShades.darken1,
  MaterialDesignShades.darken2,
  MaterialDesignShades.darken3,
  MaterialDesignShades.darken4,
  MaterialDesignShades.accent1,
  MaterialDesignShades.accent2,
  MaterialDesignShades.accent3,
  MaterialDesignShades.accent4,
];
}
