import ColourList                               from './ColourList';
import ColourModel                              from '../models/ColourModel';
import WebNamedColourConstants                  from './WebNamedColourConstants';

export default class ColourFactorWebNamed {

    public static get webNamedColours(): Array<ColourList> {

        const data = new Array<ColourList>();

        data.push(new ColourList('Pink', this.pink));
        data.push(new ColourList('Red', this.red));
        data.push(new ColourList('Orange', this.orange));
        data.push(new ColourList('Yellow', this.yellow));
        data.push(new ColourList('Brown', this.brown));
        data.push(new ColourList('Green', this.green));
        data.push(new ColourList('Cyan', this.cyan));
        data.push(new ColourList('Blue', this.blue));
        data.push(new ColourList('Purple', this.purple));
        data.push(new ColourList('White', this.white));
        data.push(new ColourList('Grey', this.grey));

        return data;
    }

    private static get pink(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Pink,
            WebNamedColourConstants.LightPink,
            WebNamedColourConstants.HotPink,
            WebNamedColourConstants.DeepPink,
            WebNamedColourConstants.PaleVioletRed,
            WebNamedColourConstants.MediumVioletRed,
        ];
    }

    private static get red(): Array<ColourModel> {
        return [
            WebNamedColourConstants.LightSalmon,
            WebNamedColourConstants.Salmon,
            WebNamedColourConstants.DarkSalmon,
            WebNamedColourConstants.LightCoral,
            WebNamedColourConstants.IndianRed,
            WebNamedColourConstants.Crimson,
            WebNamedColourConstants.Firebrick,
            WebNamedColourConstants.DarkRed,
            WebNamedColourConstants.Red,
        ];
    }

    private static get orange(): Array<ColourModel> {
        return [
            WebNamedColourConstants.OrangeRed,
            WebNamedColourConstants.Tomato,
            WebNamedColourConstants.Coral,
            WebNamedColourConstants.DarkOrange,
            WebNamedColourConstants.Orange,
        ];
    }

    private static get yellow(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Yellow,
            WebNamedColourConstants.LightYellow,
            WebNamedColourConstants.LemonChiffon,
            WebNamedColourConstants.LightGoldenrodYellow,
            WebNamedColourConstants.PapayaWhip,
            WebNamedColourConstants.Moccasin,
            WebNamedColourConstants.PeachPuff,
            WebNamedColourConstants.PaleGoldenrod,
            WebNamedColourConstants.Khaki,
            WebNamedColourConstants.DarkKhaki,
            WebNamedColourConstants.Gold,
        ];
    }

    private static get brown(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Cornsilk,
            WebNamedColourConstants.BlanchedAlmond,
            WebNamedColourConstants.Bisque,
            WebNamedColourConstants.NavajoWhite,
            WebNamedColourConstants.Wheat,
            WebNamedColourConstants.Burlywood,
            WebNamedColourConstants.Tan,
            WebNamedColourConstants.RosyBrown,
            WebNamedColourConstants.SandyBrown,
            WebNamedColourConstants.Goldenrod,
            WebNamedColourConstants.DarkGoldenrod,
            WebNamedColourConstants.Peru,
            WebNamedColourConstants.Chocolate,
            WebNamedColourConstants.SaddleBrown,
            WebNamedColourConstants.Sienna,
            WebNamedColourConstants.Brown,
            WebNamedColourConstants.Maroon,
        ];
    }

    private static get green(): Array<ColourModel> {
        return [
            WebNamedColourConstants.DarkOliveGreen,
            WebNamedColourConstants.Olive,
            WebNamedColourConstants.OliveDrab,
            WebNamedColourConstants.YellowGreen,
            WebNamedColourConstants.LimeGreen,
            WebNamedColourConstants.Lime,
            WebNamedColourConstants.LawnGreen,
            WebNamedColourConstants.Chartreuse,
            WebNamedColourConstants.GreenYellow,
            WebNamedColourConstants.SpringGreen,
            WebNamedColourConstants.MediumSpringGreen,
            WebNamedColourConstants.LightGreen,
            WebNamedColourConstants.PaleGreen,
            WebNamedColourConstants.DarkSeaGreen,
            WebNamedColourConstants.MediumAquamarine,
            WebNamedColourConstants.MediumSeaGreen,
            WebNamedColourConstants.SeaGreen,
            WebNamedColourConstants.ForestGreen,
            WebNamedColourConstants.Green,
            WebNamedColourConstants.DarkGreen,
        ];
    }

    private static get cyan(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Aqua,
            WebNamedColourConstants.Cyan,
            WebNamedColourConstants.LightCyan,
            WebNamedColourConstants.PaleTurquoise,
            WebNamedColourConstants.Aquamarine,
            WebNamedColourConstants.Turquoise,
            WebNamedColourConstants.MediumTurquoise,
            WebNamedColourConstants.DarkTurquoise,
            WebNamedColourConstants.LightSeaGreen,
            WebNamedColourConstants.CadetBlue,
            WebNamedColourConstants.DarkCyan,
            WebNamedColourConstants.Teal,
        ];
    }

    private static get blue(): Array<ColourModel> {
        return [
            WebNamedColourConstants.LightSteelBlue,
            WebNamedColourConstants.PowderBlue,
            WebNamedColourConstants.LightBlue,
            WebNamedColourConstants.SkyBlue,
            WebNamedColourConstants.LightSkyBlue,
            WebNamedColourConstants.DeepSkyBlue,
            WebNamedColourConstants.DodgerBlue,
            WebNamedColourConstants.CornflowerBlue,
            WebNamedColourConstants.SteelBlue,
            WebNamedColourConstants.RoyalBlue,
            WebNamedColourConstants.Blue,
            WebNamedColourConstants.MediumBlue,
            WebNamedColourConstants.DarkBlue,
            WebNamedColourConstants.Navy,
            WebNamedColourConstants.MidnightBlue,
        ];
    }

    private static get purple(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Lavender,
            WebNamedColourConstants.Thistle,
            WebNamedColourConstants.Plum,
            WebNamedColourConstants.Violet,
            WebNamedColourConstants.Orchid,
            WebNamedColourConstants.Fuchsia,
            WebNamedColourConstants.Magenta,
            WebNamedColourConstants.MediumOrchid,
            WebNamedColourConstants.MediumPurple,
            WebNamedColourConstants.BlueViolet,
            WebNamedColourConstants.DarkViolet,
            WebNamedColourConstants.DarkOrchid,
            WebNamedColourConstants.DarkMagenta,
            WebNamedColourConstants.Purple,
            WebNamedColourConstants.Indigo,
            WebNamedColourConstants.DarkSlateBlue,
            WebNamedColourConstants.SlateBlue,
            WebNamedColourConstants.MediumSlateBlue,
        ];
    }

    private static get white(): Array<ColourModel> {
        return [
            WebNamedColourConstants.White,
            WebNamedColourConstants.Snow,
            WebNamedColourConstants.Honeydew,
            WebNamedColourConstants.MintCream,
            WebNamedColourConstants.Azure,
            WebNamedColourConstants.AliceBlue,
            WebNamedColourConstants.GhostWhite,
            WebNamedColourConstants.WhiteSmoke,
            WebNamedColourConstants.Seashell,
            WebNamedColourConstants.Beige,
            WebNamedColourConstants.OldLace,
            WebNamedColourConstants.FloralWhite,
            WebNamedColourConstants.Ivory,
            WebNamedColourConstants.AntiqueWhite,
            WebNamedColourConstants.Linen,
            WebNamedColourConstants.LavenderBlush,
            WebNamedColourConstants.MistyRose,
        ];
    }

    private static get grey(): Array<ColourModel> {
        return [
            WebNamedColourConstants.Gainsboro,
            WebNamedColourConstants.LightGray,
            WebNamedColourConstants.Silver,
            WebNamedColourConstants.DarkGray,
            WebNamedColourConstants.Gray,
            WebNamedColourConstants.DimGray,
            WebNamedColourConstants.LightSlateGray,
            WebNamedColourConstants.SlateGray,
            WebNamedColourConstants.DarkSlateGray,
            WebNamedColourConstants.Black,
        ];
    }
}
