import ColourModel from '../models/ColourModel';

export default class WebNamedColourConstants {

    public static Pink = ColourModel.fromNamedHex('Pink', 'FFC0CB');
    public static LightPink = ColourModel.fromNamedHex('LightPink ', 'FFB6C1');
    public static HotPink = ColourModel.fromNamedHex('HotPink ', 'FF69B4');
    public static DeepPink = ColourModel.fromNamedHex('DeepPink ', 'FF1493');
    public static PaleVioletRed = ColourModel.fromNamedHex('PaleVioletRed ', 'DB7093');
    public static MediumVioletRed = ColourModel.fromNamedHex('MediumVioletRed ', 'C71585');


    public static LightSalmon = ColourModel.fromNamedHex('LightSalmon ', 'FFA07A');
    public static Salmon = ColourModel.fromNamedHex('Salmon ', 'FA8072');
    public static DarkSalmon = ColourModel.fromNamedHex('DarkSalmon ', 'E9967A');
    public static LightCoral = ColourModel.fromNamedHex('LightCoral ', 'F08080');
    public static IndianRed = ColourModel.fromNamedHex('IndianRed ', 'CD5C5C');
    public static Crimson = ColourModel.fromNamedHex('Crimson ', 'DC143C');
    public static Firebrick = ColourModel.fromNamedHex('Firebrick ', 'B22222');
    public static DarkRed = ColourModel.fromNamedHex('DarkRed ', '8B0000');
    public static Red = ColourModel.fromNamedHex('Red ', 'FF0000');

    public static OrangeRed = ColourModel.fromNamedHex('OrangeRed ', 'FF4500');
    public static Tomato = ColourModel.fromNamedHex('Tomato ', 'FF6347');
    public static Coral = ColourModel.fromNamedHex('Coral ', 'FF7F50');
    public static DarkOrange = ColourModel.fromNamedHex('DarkOrange ', 'FF8C00');
    public static Orange = ColourModel.fromNamedHex('Orange ', 'FFA500');

    public static Yellow = ColourModel.fromNamedHex('Yellow ', 'FFFF00');
    public static LightYellow = ColourModel.fromNamedHex('LightYellow ', 'FFFFE0');
    public static LemonChiffon = ColourModel.fromNamedHex('LemonChiffon ', 'FFFACD');
    public static LightGoldenrodYellow = ColourModel.fromNamedHex('LightGoldenrodYellow ', 'FAFAD2');
    public static PapayaWhip = ColourModel.fromNamedHex('PapayaWhip ', 'FFEFD5');
    public static Moccasin = ColourModel.fromNamedHex('Moccasin ', 'FFE4B5');
    public static PeachPuff = ColourModel.fromNamedHex('PeachPuff ', 'FFDAB9');
    public static PaleGoldenrod = ColourModel.fromNamedHex('PaleGoldenrod ', 'EEE8AA');
    public static Khaki = ColourModel.fromNamedHex('Khaki ', 'F0E68C');
    public static DarkKhaki = ColourModel.fromNamedHex('DarkKhaki ', 'BDB76B');
    public static Gold = ColourModel.fromNamedHex('Gold ', 'FFD700');

    public static Cornsilk = ColourModel.fromNamedHex('Cornsilk ', 'FFF8DC');
    public static BlanchedAlmond = ColourModel.fromNamedHex('BlanchedAlmond ', 'FFEBCD');
    public static Bisque = ColourModel.fromNamedHex('Bisque ', 'FFE4C4');
    public static NavajoWhite = ColourModel.fromNamedHex('NavajoWhite ', 'FFDEAD');
    public static Wheat = ColourModel.fromNamedHex('Wheat ', 'F5DEB3');
    public static Burlywood = ColourModel.fromNamedHex('Burlywood ', 'DEB887');
    public static Tan = ColourModel.fromNamedHex('Tan ', 'D2B48C');
    public static RosyBrown = ColourModel.fromNamedHex('RosyBrown ', 'BC8F8F');
    public static SandyBrown = ColourModel.fromNamedHex('SandyBrown ', 'F4A460');
    public static Goldenrod = ColourModel.fromNamedHex('Goldenrod ', 'DAA520');
    public static DarkGoldenrod = ColourModel.fromNamedHex('DarkGoldenrod ', 'B8860B');
    public static Peru = ColourModel.fromNamedHex('Peru ', 'CD853F');
    public static Chocolate = ColourModel.fromNamedHex('Chocolate ', 'D2691E');
    public static SaddleBrown = ColourModel.fromNamedHex('SaddleBrown ', '8B4513');
    public static Sienna = ColourModel.fromNamedHex('Sienna ', 'A0522D');
    public static Brown = ColourModel.fromNamedHex('Brown ', 'A52A2A');
    public static Maroon = ColourModel.fromNamedHex('Maroon ', '800000');


    public static DarkOliveGreen = ColourModel.fromNamedHex('DarkOliveGreen ', '556B2F');
    public static Olive = ColourModel.fromNamedHex('Olive ', '808000');
    public static OliveDrab = ColourModel.fromNamedHex('OliveDrab ', '6B8E23');
    public static YellowGreen = ColourModel.fromNamedHex('YellowGreen ', '9ACD32');
    public static LimeGreen = ColourModel.fromNamedHex('LimeGreen ', '32CD32');
    public static Lime = ColourModel.fromNamedHex('Lime ', '00FF00');
    public static LawnGreen = ColourModel.fromNamedHex('LawnGreen ', '7CFC00');
    public static Chartreuse = ColourModel.fromNamedHex('Chartreuse ', '7FFF00');
    public static GreenYellow = ColourModel.fromNamedHex('GreenYellow ', 'ADFF2F');
    public static SpringGreen = ColourModel.fromNamedHex('SpringGreen ', '00FF7F');
    public static MediumSpringGreen = ColourModel.fromNamedHex('MediumSpringGreen ', '00FA9A');
    public static LightGreen = ColourModel.fromNamedHex('LightGreen ', '90EE90');
    public static PaleGreen = ColourModel.fromNamedHex('PaleGreen ', '98FB98');
    public static DarkSeaGreen = ColourModel.fromNamedHex('DarkSeaGreen ', '8FBC8F');
    public static MediumAquamarine = ColourModel.fromNamedHex('MediumAquamarine ', '66CDAA');
    public static MediumSeaGreen = ColourModel.fromNamedHex('MediumSeaGreen ', '3CB371');
    public static SeaGreen = ColourModel.fromNamedHex('SeaGreen ', '2E8B57');
    public static ForestGreen = ColourModel.fromNamedHex('ForestGreen ', '228B22');
    public static Green = ColourModel.fromNamedHex('Green ', '008000');
    public static DarkGreen = ColourModel.fromNamedHex('DarkGreen ', '006400');

    public static Aqua = ColourModel.fromNamedHex('Aqua ', '00FFFF');
    public static Cyan = ColourModel.fromNamedHex('Cyan ', '00FFFF');
    public static LightCyan = ColourModel.fromNamedHex('LightCyan ', 'E0FFFF');
    public static PaleTurquoise = ColourModel.fromNamedHex('PaleTurquoise ', 'AFEEEE');
    public static Aquamarine = ColourModel.fromNamedHex('Aquamarine ', '7FFFD4');
    public static Turquoise = ColourModel.fromNamedHex('Turquoise ', '40E0D0');
    public static MediumTurquoise = ColourModel.fromNamedHex('MediumTurquoise ', '48D1CC');
    public static DarkTurquoise = ColourModel.fromNamedHex('DarkTurquoise ', '00CED1');
    public static LightSeaGreen = ColourModel.fromNamedHex('LightSeaGreen ', '20B2AA');
    public static CadetBlue = ColourModel.fromNamedHex('CadetBlue ', '5F9EA0');
    public static DarkCyan = ColourModel.fromNamedHex('DarkCyan ', '008B8B');
    public static Teal = ColourModel.fromNamedHex('Teal ', '008080');

    public static LightSteelBlue = ColourModel.fromNamedHex('LightSteelBlue ', 'B0C4DE');
    public static PowderBlue = ColourModel.fromNamedHex('PowderBlue ', 'B0E0E6');
    public static LightBlue = ColourModel.fromNamedHex('LightBlue ', 'ADD8E6');
    public static SkyBlue = ColourModel.fromNamedHex('SkyBlue ', '87CEEB');
    public static LightSkyBlue = ColourModel.fromNamedHex('LightSkyBlue ', '87CEFA');
    public static DeepSkyBlue = ColourModel.fromNamedHex('DeepSkyBlue ', '00BFFF');
    public static DodgerBlue = ColourModel.fromNamedHex('DodgerBlue ', '1E90FF');
    public static CornflowerBlue = ColourModel.fromNamedHex('CornflowerBlue ', '6495ED');
    public static SteelBlue = ColourModel.fromNamedHex('SteelBlue ', '4682B4');
    public static RoyalBlue = ColourModel.fromNamedHex('RoyalBlue ', '4169E1');
    public static Blue = ColourModel.fromNamedHex('Blue ', '0000FF');
    public static MediumBlue = ColourModel.fromNamedHex('MediumBlue ', '0000CD');
    public static DarkBlue = ColourModel.fromNamedHex('DarkBlue ', '00008B');
    public static Navy = ColourModel.fromNamedHex('Navy ', '000080');
    public static MidnightBlue = ColourModel.fromNamedHex('MidnightBlue ', '191970');


    public static Lavender = ColourModel.fromNamedHex('Lavender ', 'E6E6FA');
    public static Thistle = ColourModel.fromNamedHex('Thistle ', 'D8BFD8');
    public static Plum = ColourModel.fromNamedHex('Plum ', 'DDA0DD');
    public static Violet = ColourModel.fromNamedHex('Violet ', 'EE82EE');
    public static Orchid = ColourModel.fromNamedHex('Orchid ', 'DA70D6');
    public static Fuchsia = ColourModel.fromNamedHex('Fuchsia ', 'FF00FF');
    public static Magenta = ColourModel.fromNamedHex('Magenta ', 'FF00FF');
    public static MediumOrchid = ColourModel.fromNamedHex('MediumOrchid ', 'BA55D3');
    public static MediumPurple = ColourModel.fromNamedHex('MediumPurple ', '9370DB');
    public static BlueViolet = ColourModel.fromNamedHex('BlueViolet ', '8A2BE2');
    public static DarkViolet = ColourModel.fromNamedHex('DarkViolet ', '9400D3');
    public static DarkOrchid = ColourModel.fromNamedHex('DarkOrchid ', '9932CC');
    public static DarkMagenta = ColourModel.fromNamedHex('DarkMagenta ', '8B008B');
    public static Purple = ColourModel.fromNamedHex('Purple ', '800080');
    public static Indigo = ColourModel.fromNamedHex('Indigo ', '4B0082');
    public static DarkSlateBlue = ColourModel.fromNamedHex('DarkSlateBlue ', '483D8B');
    public static SlateBlue = ColourModel.fromNamedHex('SlateBlue ', '6A5ACD');
    public static MediumSlateBlue = ColourModel.fromNamedHex('MediumSlateBlue ', '7B68EE');

    public static White = ColourModel.fromNamedHex('White ', 'FFFFFF');
    public static Snow = ColourModel.fromNamedHex('Snow ', 'FFFAFA');
    public static Honeydew = ColourModel.fromNamedHex('Honeydew ', 'F0FFF0');
    public static MintCream = ColourModel.fromNamedHex('MintCream ', 'F5FFFA');
    public static Azure = ColourModel.fromNamedHex('Azure ', 'F0FFFF');
    public static AliceBlue = ColourModel.fromNamedHex('AliceBlue ', 'F0F8FF');
    public static GhostWhite = ColourModel.fromNamedHex('GhostWhite ', 'F8F8FF');
    public static WhiteSmoke = ColourModel.fromNamedHex('WhiteSmoke ', 'F5F5F5');
    public static Seashell = ColourModel.fromNamedHex('Seashell ', 'FFF5EE');
    public static Beige = ColourModel.fromNamedHex('Beige ', 'F5F5DC');
    public static OldLace = ColourModel.fromNamedHex('OldLace ', 'FDF5E6');
    public static FloralWhite = ColourModel.fromNamedHex('FloralWhite ', 'FFFAF0');
    public static Ivory = ColourModel.fromNamedHex('Ivory ', 'FFFFF0');
    public static AntiqueWhite = ColourModel.fromNamedHex('AntiqueWhite ', 'FAEBD7');
    public static Linen = ColourModel.fromNamedHex('Linen ', 'FAF0E6');
    public static LavenderBlush = ColourModel.fromNamedHex('LavenderBlush ', 'FFF0F5');
    public static MistyRose = ColourModel.fromNamedHex('MistyRose ', 'FFE4E1');

    public static Gainsboro = ColourModel.fromNamedHex('Gainsboro ', 'DCDCDC');
    public static LightGray = ColourModel.fromNamedHex('LightGray ', 'D3D3D3');
    public static Silver = ColourModel.fromNamedHex('Silver ', 'C0C0C0');
    public static DarkGray = ColourModel.fromNamedHex('DarkGray ', 'A9A9A9');
    public static Gray = ColourModel.fromNamedHex('Gray ', '808080');
    public static DimGray = ColourModel.fromNamedHex('DimGray ', '696969');
    public static LightSlateGray = ColourModel.fromNamedHex('LightSlateGray ', '778899');
    public static SlateGray = ColourModel.fromNamedHex('SlateGray ', '708090');
    public static DarkSlateGray = ColourModel.fromNamedHex('DarkSlateGray ', '2F4F4F');
    public static Black = ColourModel.fromNamedHex('Black ', '000000');
}
