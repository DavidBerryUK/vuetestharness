import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import ColourModel                              from '../models/ColourModel';
import Component                                from 'vue-class-component';
import NumberSpinnerComponent                   from '@/componentsGui/numberSpinnerComponent/NumberSpinnerComponent';
import Vue                                      from 'vue';
import WebNamedColourConstants                  from '../constants/WebNamedColourConstants';


@Component({
    components: {
        NumberSpinnerComponent,
    },
  })
export default class ColourSlidersComponent extends Vue   {

    @Prop()
    public value!: ColourModel;

    private colour = ColourModel.fromRgb(128, 64, 32);


    @Watch('value')
    private onNumberChanged(value: ColourModel, oldValue: ColourModel) {
        this.colour = value;
    }

    public startRed() {
        this.colour.lockAllExceptRed();
    }

    public startGreen() {
        this.colour.lockAllExceptGreen();
    }

    public startBlue() {
        this.colour.lockAllExceptBlue();
    }

    public startHue() {
        this.colour.lockAllExceptHue();
    }

    public startSaturation() {
        this.colour.lockAllExceptSaturation();
    }

    public startLuminosity() {
        this.colour.lockAllExceptLuminosity();
    }

    public releaseSlider() {
        this.colour.unlockAll();
    }

    public get styleRedGradient(): string {
        const none =  ColourModel.fromRgb(0, this.colour.green, this.colour.blue);
        const full =  ColourModel.fromRgb(255, this.colour.green, this.colour.blue);
        const style = `background-image: linear-gradient(to right, ${none.hex} 0%,  ${full.hex} 100%);`;
        return style;
    }

    public get styleGreenGradient(): string {
        const none =  ColourModel.fromRgb(this.colour.red, 0, this.colour.blue);
        const full =  ColourModel.fromRgb(this.colour.red, 255, this.colour.blue);
        const style = `background-image: linear-gradient(to right, ${none.hex} 0%,  ${full.hex} 100%);`;
        return style;
    }

    public get styleBlueGradient(): string {
        const none =  ColourModel.fromRgb(this.colour.red, this.colour.green, 0);
        const full =  ColourModel.fromRgb(this.colour.red, this.colour.green, 255);
        const style = `background-image: linear-gradient(to right, ${none.hex} 0%,  ${full.hex} 100%);`;
        return style;
    }

    public get styleHueGradient(): string {
        const red =  ColourModel.fromHsl(WebNamedColourConstants.Red.hue, this.colour.saturation, this.colour.luminosity);
        const yellow =  ColourModel.fromHsl(WebNamedColourConstants.Yellow.hue, this.colour.saturation, this.colour.luminosity);
        const green = ColourModel.fromHsl(WebNamedColourConstants.Green.hue, this.colour.saturation, this.colour.luminosity);
        const cyan = ColourModel.fromHsl(WebNamedColourConstants.Cyan.hue, this.colour.saturation, this.colour.luminosity);
        const blue = ColourModel.fromHsl(WebNamedColourConstants.Blue.hue, this.colour.saturation, this.colour.luminosity);
        const magenta = ColourModel.fromHsl(WebNamedColourConstants.Magenta.hue, this.colour.saturation, this.colour.luminosity);
        const style = `background-image: linear-gradient(to right, ${red.hex} 0%, ${yellow.hex} 17%, ${green.hex} 34%, ${cyan.hex} 50%, ${blue.hex} 66%, ${magenta.hex} 82%, ${red.hex} 100%);`;
        return style;

    }

    public get styleSaturationGradient(): string {
        const none =  ColourModel.fromHsl(this.colour.hue, 0, this.colour.luminosity);
        const full =  ColourModel.fromHsl(this.colour.hue, 1, this.colour.luminosity);
        const style = `background-image: linear-gradient(to right, ${none.hex} 0%,  ${full.hex} 100%);`;
        return style;
    }

    public get styleLuminosityGradient(): string {
        const none =  ColourModel.fromHsl(this.colour.hue, this.colour.saturation, 0);
        const full =  ColourModel.fromHsl(this.colour.hue, this.colour.saturation, 1);
        const style = `background-image: linear-gradient(to right, ${none.hex} 0%,  ${full.hex} 100%);`;
        return style;
    }

    public data(): any  {
        return {};
    }

  }

Vue.component('db-colour-sliders', ColourSlidersComponent);
