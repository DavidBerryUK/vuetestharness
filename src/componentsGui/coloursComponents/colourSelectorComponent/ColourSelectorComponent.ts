import { EnumColourPalette }                    from '../enums/ColourEnums';
import ColourModel                              from '../models/ColourModel';
import ColourSlidersComponent                   from '../colourSlidersComponent/ColourSlidersComponent';
import ColoursPaletteComponent                  from '../coloursPaletteComponent/ColoursPaletteComponent';
import ColourSwatchComponent                    from '../colourSwatchComponent/ColourSwatchComponent';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';
import WebNamedColourConstants                  from '../constants/WebNamedColourConstants';



@Component({
    components: {
      ColourSlidersComponent,
      ColoursPaletteComponent,
      ColourSwatchComponent,
    },
  })
export default class ColourSelectorComponent extends Vue   {
    private value: ColourModel = WebNamedColourConstants.White;
    private EnumColourPalette = EnumColourPalette;
    private readonly pickerMaterialDesign: string = 'm';
    private readonly pickerNamedWebColours: string = 'w';
    private readonly pickerPantoneColours: string = 'p';

    private colourPicker: string = this.pickerMaterialDesign;

    private onColourChanged(colour: ColourModel) {
      console.log('colour selector - colour changed');
      this.value = ColourModel.fromColour(colour);
    }


    private get showPickerMaterialDesign(): boolean {
      return this.colourPicker === this.pickerMaterialDesign;
    }

    private get showPickerNamedWebColours(): boolean {
      return this.colourPicker === this.pickerNamedWebColours;
    }

    private get showPickerPantoneColours(): boolean {
      return this.colourPicker === this.pickerPantoneColours;
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-colour-selector', ColourSelectorComponent);
