import { EnumColourPalette }                    from '../enums/ColourEnums';
import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import ColourFactorWebNamed                     from '../constants/ColourFactorWebNamed';
import ColourFactoryMaterialDesign              from '../constants/ColourFactoryMaterialDesign';
import ColourModel                              from '../models/ColourModel';
import ColourPreviewLensComponent               from '../colourPreviewLensComponent/ColourPreviewLensComponent';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';


@Component({
    components: {
        ColourPreviewLensComponent,
    },
  })
export default class ColoursPaletteComponent extends Vue   {


    @Prop()
    public palette!: EnumColourPalette;

    // private colourList = ColourFactoryMaterialDesign.materialDesignColours;
    private colourList = this.obtainPalette;

    private get obtainPalette() {
        if ( this.palette === EnumColourPalette.MaterialDesign ) {
            return ColourFactoryMaterialDesign.materialDesignColours;
        }
        return ColourFactorWebNamed.webNamedColours;
    }

    @Watch('palette')
    private onNumberChanged(value: EnumColourPalette, oldValue: EnumColourPalette) {
        this.colourList = this.obtainPalette;
    }

    private onColourChanged(colour: ColourModel) {
        console.log('colour palette - colour changed');
        this.$emit('colourChanged', colour);
    }

    public mouseEnter(colour: ColourModel) {
        // console.log(`mouse enter:${colour.name}`);
        // console.log(`r:${colour.red} g:${colour.blue} g:${colour.green}  h:${colour.hue.toFixed(2)} s:${colour.saturation.toFixed(2)} l:${colour.luminosity.toFixed(2)}`);
    }
    public mouseLeave(colour: ColourModel) {
        // console.log('mouse leave:' + colour.name);
    }
    public data(): any  {
        return {};
    }
  }

Vue.component('db-colour-palette', ColoursPaletteComponent);
