export interface IAvatarListItemModel {
    id: number;
    name: string;
    alternativeText: string;
    imageUrl: string;
    selected: boolean;
    visible: boolean;
    notifications: number;

    borderAnimation: string;
}
