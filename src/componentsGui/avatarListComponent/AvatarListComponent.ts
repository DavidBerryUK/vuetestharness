import { EnumListOrientation }                  from './enums/AvatarListEnums';
import { Prop }                                 from 'vue-property-decorator';
import AvatarComponent                          from '../avatarComponent/AvatarComponent';
import AvatarListItemModel                      from '@/harnesses/pages/avatarList/models/AvatarListItemModel';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';

@Component({
    components: {
        AvatarComponent,
    },
  })
export default class AvatarListComponent extends Vue   {


    @Prop()
    public orientation!: string;

    @Prop()
    public avatars!: Array<AvatarListItemModel>;


    public get visibleAvatars(): Array<AvatarListItemModel> {
        const shown = this.avatars.filter((avatar) => avatar.visible);
        return shown;
    }


    private get safeOrientation(): string {
        if ( this.orientation) {
            if (this.orientation ===  EnumListOrientation.portrait) {
                return 'portrait';
            }
        }
        return 'landscape';
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-avatar-list', AvatarListComponent);
