export default class SampleModel {
    public name!: string;

    public age!: number;

    public eligibleToVote!: boolean;

    public favoriteAnimalImageUrl!: string;
    public favoritePaintingImageUrl!: string;
    public favoriteStockImageImageUrl!: string;

}
