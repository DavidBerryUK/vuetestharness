import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';
import SampleModel                              from './SampleModel';

@Component
export default class SampleComponent extends Vue {

    @Prop()
    public value!: SampleModel;

    @Prop()
    public houseNumber!: number;

    @Prop()
    public address!: string;

    @Prop()
    public rented!: boolean;



    /**
     * SAMPLE EVENTS RAISED FOR TESTING
     *
     * @memberof SampleComponent
     */
    public sampleEventA() {
        this.$emit('sampleEventA');
    }

    public sampleEventB() {
        this.$emit('sampleEventB');
    }

    public sampleEventC() {
        this.$emit('sampleEventC');
    }

    public sampleEventD() {
        this.$emit('sampleEventD');
    }

    public data(): any {
        return {};
    }

}

Vue.component('sample-component', SampleComponent);
