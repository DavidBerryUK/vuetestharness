import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';

export enum EnumLoaderSize {
    default = 1,
    avatar = 2,
    small = 3,
    medium = 4,
    large = 5,
}

@Component
export default class LoaderComponent extends Vue {

    @Prop({ type: String, default: EnumLoaderSize.default })
    public loaderSize!: string;

    private get loaderSizeAsEnum(): EnumLoaderSize {
        const enumValue = EnumLoaderSize[this.loaderSize as keyof typeof EnumLoaderSize];
        return enumValue;
    }

    /**
     * Sets the diameter of the circle in pixels
     *
     * @readonly
     * @type {number}
     * @memberof Loader
     */
    public get size(): number {

        switch (this.loaderSizeAsEnum) {
            case EnumLoaderSize.default:
                return 32;

            case EnumLoaderSize.avatar:
                return 60;

            case EnumLoaderSize.small:
                return 16;

            case EnumLoaderSize.medium:
                return 32;

            case EnumLoaderSize.large:
                return 64;
        }
    }


    /**
     * Sets the stroke of the circle in pixels
     *
     * @readonly
     * @type {number}
     * @memberof Loader
     */
    public get width(): number {

        const enumValue = EnumLoaderSize[this.loaderSize as keyof typeof EnumLoaderSize];

        switch (this.loaderSizeAsEnum) {
            case EnumLoaderSize.default:
                return 2;

            case EnumLoaderSize.avatar:
                return 2;

            case EnumLoaderSize.small:
                return 2;

            case EnumLoaderSize.medium:
                return 4;

            case EnumLoaderSize.large:
                return 6;
        }
    }


    /**
     * Applies specified color to the control -
     * it can be the name of material color (for example success or purple)
     * or css color (#033 or rgba(255, 0, 0, 0.5))
     *
     * @readonly
     * @type {string}
     * @memberof Loader
     */
    get colour(): string {

        const enumValue = EnumLoaderSize[this.loaderSize as keyof typeof EnumLoaderSize];

        switch (this.loaderSizeAsEnum) {
            case EnumLoaderSize.default:
                return 'SteelBlue';

            case EnumLoaderSize.avatar:
                return 'SteelBlue';

            case EnumLoaderSize.small:
                return 'SteelBlue';

            case EnumLoaderSize.medium:
                return 'SteelBlue';

            case EnumLoaderSize.large:
                return 'SteelBlue';
        }
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-loader', LoaderComponent);
