import { IChartRenderer }                       from '../interfaces/IChartRenderer';
import ChartModel                               from '../models/ChartModel';
import DrawPieChart                             from '@/componentsModules/graphicLibrary/canvasOperations/DrawPieChart';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';
import Point                                    from '@/componentsModules/trigonometryLibrary/models/Point';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';
import Size                                     from '@/componentsModules/trigonometryLibrary/models/Size';


export default class PieChartRenderer extends HtmlCanvas implements IChartRenderer {

    public chartModel: ChartModel;

    constructor(canvasElement: HTMLCanvasElement, chartModel: ChartModel) {
        super(canvasElement);
        this.chartModel = chartModel;
    }

    public render(): void {
        const rect = new Rect(new Point(0, 0), new Size(this.width, this.height));
        DrawPieChart.draw(this, rect, this.chartModel);
    }
}
