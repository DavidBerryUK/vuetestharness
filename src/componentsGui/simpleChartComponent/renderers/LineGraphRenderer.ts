import { IChartRenderer }                       from '../interfaces/IChartRenderer';
import ChartModel                               from '../models/ChartModel';
import DrawLineGraph                            from '@/componentsModules/graphicLibrary/canvasOperations/DrawLineGraph';
import HtmlCanvas                               from '@/componentsModules/graphicLibrary/htmlCanvas/HtmlCanvas';
import Point                                    from '@/componentsModules/trigonometryLibrary/models/Point';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';
import Size                                     from '@/componentsModules/trigonometryLibrary/models/Size';


export default class LineGraphRenderer extends HtmlCanvas implements IChartRenderer {

    public chartModel: ChartModel;

    constructor(canvasElement: HTMLCanvasElement, chartModel: ChartModel) {
        super(canvasElement);
        this.chartModel = chartModel;
    }

    public render(): void {
        const rect = new Rect(new Point(0, 0), new Size(this.width, this.height));
        DrawLineGraph.draw(this, rect, this.chartModel);
    }

}
