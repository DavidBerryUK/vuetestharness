import { EnumChartType }                        from '../enums/SimpleChartEnums';
import ChartDataPoint                           from '../models/ChartDataPoint';
import ChartDataSet                             from '../models/ChartDataSet';
import ChartModel                               from '../models/ChartModel';
import ChartServices                            from '../../../componentsModules/graphicLibrary/canvasOperations/helper/ChartServices';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

describe('Chart Service - calculate dataset sums', () => {

    test('Calculate sum Values with no dataset', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        svc.calculateDataSetSums(model);

    });


    test('Calculate sum Values single data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(10, 20));

        const svc = new ChartServices();

        svc.calculateDataSetSums(model);

        expect(model.dataSets[0].sumX).toBe(10);
        expect(model.dataSets[0].sumY).toBe(20);
    });


    test('Calculate sum Values multiple data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(10, 20));
        dataset.dataPoints.push( new ChartDataPoint(20, 25));
        dataset.dataPoints.push( new ChartDataPoint(35, 75));
        dataset.dataPoints.push( new ChartDataPoint(42, 125));

        const svc = new ChartServices();

        svc.calculateDataSetSums(model);

        expect(model.dataSets[0].sumX).toBe(107);
        expect(model.dataSets[0].sumY).toBe(245);
    });


    test('Calculate sum Values multiple dataSets and multiple data points', () => {

        const model = new ChartModel(EnumChartType.LineGraph);

        const datasetA = new ChartDataSet('sample A', WebNamedColourConstants.White);
        model.dataSets.push(datasetA);
        datasetA.dataPoints.push( new ChartDataPoint(10, 20));
        datasetA.dataPoints.push( new ChartDataPoint(20, 25));
        datasetA.dataPoints.push( new ChartDataPoint(35, 75));
        datasetA.dataPoints.push( new ChartDataPoint(42, 125));

        const datasetB = new ChartDataSet('sample B', WebNamedColourConstants.White);
        model.dataSets.push(datasetB);
        datasetB.dataPoints.push( new ChartDataPoint(100, 10));
        datasetB.dataPoints.push( new ChartDataPoint(200, 20));
        datasetB.dataPoints.push( new ChartDataPoint(300, 30));

        const svc = new ChartServices();

        svc.calculateDataSetSums(model);

        expect(model.dataSets[0].sumX).toBe(107);
        expect(model.dataSets[0].sumY).toBe(245);

        expect(model.dataSets[1].sumX).toBe(600);
        expect(model.dataSets[1].sumY).toBe(60);
    });

});
