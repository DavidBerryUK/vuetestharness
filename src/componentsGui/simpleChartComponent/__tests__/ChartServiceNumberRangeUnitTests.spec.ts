import { EnumChartType }                        from '../enums/SimpleChartEnums';
import ChartDataPoint                           from '../models/ChartDataPoint';
import ChartDataSet                             from '../models/ChartDataSet';
import ChartModel                               from '../models/ChartModel';
import ChartServices                            from '../../../componentsModules/graphicLibrary/canvasOperations/helper/ChartServices';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

describe('Chart Service', () => {

    test('Calculate Number Range - when calc not called', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        expect(model.statistics.rangeX).toBeNull();
        expect(model.statistics.rangeY).toBeNull();
    });

    test('Calculate Number Range - empty graph model', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);

        expect(model.statistics.rangeX).toBe(0);
        expect(model.statistics.rangeY).toBe(0);
    });

    test('Calculate Number Range single data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(50, 50));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);

        expect(model.statistics.rangeX).toBe(0);
        expect(model.statistics.rangeY).toBe(0);
    });



    test('Calculate Min Max Values 2 data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(50, 100));
        dataset.dataPoints.push( new ChartDataPoint(25, 150));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);

        expect(model.statistics.rangeX).toBe(25);
        expect(model.statistics.rangeY).toBe(50);
    });


    test('Calculate Min Max Values 2 data point multiple data sets', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const datasetA = new ChartDataSet('sample A', WebNamedColourConstants.White);
        const datasetB = new ChartDataSet('sample B', WebNamedColourConstants.Red);

        model.dataSets.push(datasetA);
        datasetA.dataPoints.push( new ChartDataPoint(50, 100));
        datasetA.dataPoints.push( new ChartDataPoint(25, 150));

        model.dataSets.push(datasetB);
        datasetB.dataPoints.push( new ChartDataPoint(10, 100));
        datasetB.dataPoints.push( new ChartDataPoint(25, 250));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);

        expect(model.statistics.rangeX).toBe(40);
        expect(model.statistics.rangeY).toBe(150);
    });

});
