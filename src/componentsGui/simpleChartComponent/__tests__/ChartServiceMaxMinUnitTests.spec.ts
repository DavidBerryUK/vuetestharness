import { EnumChartType }                        from '../enums/SimpleChartEnums';
import ChartDataPoint                           from '../models/ChartDataPoint';
import ChartDataSet                             from '../models/ChartDataSet';
import ChartModel                               from '../models/ChartModel';
import ChartServices                            from '../../../componentsModules/graphicLibrary/canvasOperations/helper/ChartServices';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

describe('Chart Service', () => {

    test('Calculate Min Max Values with no dataset', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        svc.calculateMinMaxAxisValues(model);

        expect(model.statistics.minX).toBeNull();
        expect(model.statistics.maxX).toBeNull();
        expect(model.statistics.minY).toBeNull();
        expect(model.statistics.maxY).toBeNull();
    });


    test('Calculate Min Max Values single data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(0, 0));

        const svc = new ChartServices();

        svc.calculateMinMaxAxisValues(model);

        expect(model.statistics.minX).toBe(0);
        expect(model.statistics.maxX).toBe(0);
        expect(model.statistics.minY).toBe(0);
        expect(model.statistics.maxY).toBe(0);
    });

    test('Calculate Min Max Values 2 data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(50, 100));
        dataset.dataPoints.push( new ChartDataPoint(25, 150));

        const svc = new ChartServices();

        svc.calculateMinMaxAxisValues(model);

        expect(model.statistics.minX).toBe(25);
        expect(model.statistics.maxX).toBe(50);
        expect(model.statistics.minY).toBe(100);
        expect(model.statistics.maxY).toBe(150);
    });

    test('Calculate Min Max Values multiple data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(50, 100));
        dataset.dataPoints.push( new ChartDataPoint(25, 1000));
        dataset.dataPoints.push( new ChartDataPoint(25, -10));
        dataset.dataPoints.push( new ChartDataPoint(25, 150));
        dataset.dataPoints.push( new ChartDataPoint(95, 150));
        dataset.dataPoints.push( new ChartDataPoint(25, 150));
        dataset.dataPoints.push( new ChartDataPoint(5, 150));

        const svc = new ChartServices();

        svc.calculateMinMaxAxisValues(model);

        expect(model.statistics.minX).toBe(5);
        expect(model.statistics.maxX).toBe(95);
        expect(model.statistics.minY).toBe(-10);
        expect(model.statistics.maxY).toBe(1000);
    });

    test('Calculate Min Max Values multiple data point and dataset', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const datasetA = new ChartDataSet('sample A', WebNamedColourConstants.White);
        const datasetB = new ChartDataSet('sample B', WebNamedColourConstants.White);

        model.dataSets.push(datasetA);
        datasetA.dataPoints.push( new ChartDataPoint(50, 100));
        datasetA.dataPoints.push( new ChartDataPoint(25, 1000));
        datasetA.dataPoints.push( new ChartDataPoint(25, -10));
        datasetA.dataPoints.push( new ChartDataPoint(25, 150));
        datasetA.dataPoints.push( new ChartDataPoint(95, 150));
        datasetA.dataPoints.push( new ChartDataPoint(25, 150));
        datasetA.dataPoints.push( new ChartDataPoint(5, 150));

        model.dataSets.push(datasetB);
        datasetB.dataPoints.push( new ChartDataPoint(50, 100));
        datasetB.dataPoints.push( new ChartDataPoint(25, 1500));
        datasetB.dataPoints.push( new ChartDataPoint(25, -10));
        datasetB.dataPoints.push( new ChartDataPoint(1, 150));
        datasetB.dataPoints.push( new ChartDataPoint(95, 150));
        datasetB.dataPoints.push( new ChartDataPoint(25, 150));
        datasetB.dataPoints.push( new ChartDataPoint(5, 150));

        const svc = new ChartServices();

        svc.calculateMinMaxAxisValues(model);

        expect(model.statistics.minX).toBe(1);
        expect(model.statistics.maxX).toBe(95);
        expect(model.statistics.minY).toBe(-10);
        expect(model.statistics.maxY).toBe(1500);
    });

});
