import { EnumChartType }                        from '../enums/SimpleChartEnums';
import ChartDataPoint                           from '../models/ChartDataPoint';
import ChartDataSet                             from '../models/ChartDataSet';
import ChartModel                               from '../models/ChartModel';
import ChartServices                            from '../../../componentsModules/graphicLibrary/canvasOperations/helper/ChartServices';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

describe('Chart Service', () => {

    test('Calculate scale factor - when calc not called', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        expect(model.statistics.scaleFactorX).toBeNull();
        expect(model.statistics.scaleFactorY).toBeNull();
    });

    test('Calculate scale factor - empty graph model', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);
        svc.calculateScaleFactor(model, 1000, 500);

        expect(model.statistics.scaleFactorX).toBe(1);
        expect(model.statistics.scaleFactorY).toBe(1);
    });

    test('Calculate scale factor single data point', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(50, 50));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);
        svc.calculateScaleFactor(model, 1000, 500);

        expect(model.statistics.scaleFactorX).toBe(1);
        expect(model.statistics.scaleFactorY).toBe(1);
    });



    test('Calculate Min Max Values 2 data point - scale up', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(0, 0));
        dataset.dataPoints.push( new ChartDataPoint(100, 100));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);
        svc.calculateScaleFactor(model, 1000, 5000);

        expect(model.statistics.scaleFactorX).toBe(10);
        expect(model.statistics.scaleFactorY).toBe(50);
    });

    test('Calculate Min Max Values 2 data point - scale down', () => {

        const model = new ChartModel(EnumChartType.LineGraph);
        const dataset = new ChartDataSet('sample', WebNamedColourConstants.White);
        model.dataSets.push(dataset);
        dataset.dataPoints.push( new ChartDataPoint(0, 0));
        dataset.dataPoints.push( new ChartDataPoint(1000, 5000));

        const svc = new ChartServices();

        svc.calculateLineGraphStats(model);
        svc.calculateScaleFactor(model, 100, 200);

        expect(model.statistics.scaleFactorX).toBe(0.1);
        expect(model.statistics.scaleFactorY).toBe(0.04);
    });


});
