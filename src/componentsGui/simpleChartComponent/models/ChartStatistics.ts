export default class ChartStatistics {

    public minX: number | null = null;
    public maxX: number | null = null;

    public minY: number | null = null;
    public maxY: number | null = null;

    public rangeX: number | null = null;
    public rangeY: number | null = null;

    public scaleFactorX: number | null = null;

    public scaleFactorY: number | null = null;

    public offsetX: number | null = null;
    public offsetY: number | null = null;

}
