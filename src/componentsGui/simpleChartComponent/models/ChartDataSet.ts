import ChartDataPoint                           from './ChartDataPoint';
import ColourModel                              from '@/componentsGui/coloursComponents/models/ColourModel';
import WebNamedColourConstants                  from '@/componentsGui/coloursComponents/constants/WebNamedColourConstants';

export default class ChartDataSet {

    public name: string;
    public colour: ColourModel;
    public lineWidth: number = 2;

    public sumX: number | undefined;
    public sumY: number | undefined;

    public dataPoints: Array<ChartDataPoint>;

    constructor(name: string, colour?: ColourModel | undefined) {
        this.name = name;
        this.colour = colour === undefined ? WebNamedColourConstants.Black : colour;
        this.dataPoints = new Array<ChartDataPoint>();
    }
}
