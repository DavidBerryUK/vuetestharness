import { EnumChartType }                        from '../enums/SimpleChartEnums';
import ChartDataSet                             from './ChartDataSet';
import ChartStatistics                          from './ChartStatistics';

export default class ChartModel {

    public chartType: EnumChartType;

    public dataSets: Array<ChartDataSet>;

    public statistics: ChartStatistics;

    constructor(chartType: EnumChartType) {
        this.chartType = chartType;
        this.dataSets = new Array<ChartDataSet>();
        this.statistics = new ChartStatistics();
    }
}
