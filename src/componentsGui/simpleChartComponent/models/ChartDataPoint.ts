import ColourModel                              from '@/componentsGui/coloursComponents/models/ColourModel';


export default class ChartDataPoint {
    public x: number;
    public y: number;
    public name: string | undefined;
    public colour: ColourModel | undefined;

    constructor(x: number,
                y: number,
                name?: string | undefined,
                colour?: ColourModel | undefined) {
       this.x = x;
       this.y = y;
       this.name = name;
       this.colour =  colour;
    }
}
