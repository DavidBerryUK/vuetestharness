export enum EnumChartType {
    LineGraph = 'LineGraph',
    PieChart = 'PieChart',
}
