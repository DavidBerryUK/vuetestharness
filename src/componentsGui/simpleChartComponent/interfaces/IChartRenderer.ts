import ChartModel                               from '../models/ChartModel';

export interface IChartRenderer {
    render(): void;
}
