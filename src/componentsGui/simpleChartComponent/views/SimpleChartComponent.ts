import { EnumChartType }                        from '../enums/SimpleChartEnums';
import { IChartRenderer }                       from '../interfaces/IChartRenderer';
import { Prop }                                 from 'vue-property-decorator';
import ChartModel                               from '../models/ChartModel';
import Component                                from 'vue-class-component';
import LineGraphRenderer                        from '../renderers/LineGraphRenderer';
import PieChartRenderer                         from '../renderers/PieChartRenderer';
import Vue                                      from 'vue';

@Component({
    components: {
    },
  })
export default class SimpleChartComponent extends Vue   {

    @Prop()
    public value!: ChartModel;

    private renderer!: IChartRenderer;

    private mounted() {
      // need to render on 'next tick' as screen may not be rendered, thus
      // the size of the parent would not be known. this 'bug' only happens on a screen
      // refresh
      //
      const instance = this;
      this.$nextTick(() => {
          instance.setupCanvas();
          instance.renderer.render();
      });

  }

  public redraw() {
    this.$nextTick(() => {
      this.setupCanvas();
      this.renderer.render();
    });
  }

    private setupCanvas(): void {

      const canvasElement = this.$refs.canvas as HTMLCanvasElement;

      if (this.value && this.value.chartType) {
        switch (this.value.chartType) {
          case EnumChartType.LineGraph:
          this.renderer = new LineGraphRenderer(canvasElement, this.value);
          break;

           case EnumChartType.PieChart:
           this.renderer = new PieChartRenderer(canvasElement, this.value);
           break;
        }
      }
      if ( this.renderer === null) {
        this.renderer = new LineGraphRenderer(canvasElement, this.value);
      }

    }

    private onResize(event: CustomEvent) {
      this.setupCanvas();
      this.redraw();
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-simple-chart', SimpleChartComponent);
