export enum EnumMessageRecipientStatus {
    undelivered,
    delivered,
    read,
}

export enum EnumDateTimeFormat {
    none = 'None',
    time = 'Time',
    date = 'Date',
    dateTime = 'Date Time',
}
