import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ComponentIconConstants                   from '@/constants/ComponentIconConstants';
import IChatModel                               from '../interfaces/IChatModel';
import IMessageModel                            from '../interfaces/IMessageModel';
import Vue                                      from 'vue';
import moment                                   from 'moment';
import { EnumDateTimeFormat }                   from '../enums/MessageListEnums';
import DateTimeFormatter                        from '../utilities/DateTimeFormatter';

@Component
export default class MessageListComponent extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    @Prop()
    public value!: IChatModel;

    public updated() {
        this.scrollToBottomOfMessages();
    }

    public scrollToBottomOfMessages() {
        const l = this.$refs.list as Element;
        l.scrollTop = l.clientHeight;
    }

    public senderOrViewer(message: IMessageModel): string {
        if (this.value.isViewer(message)) {
            return 'viewer';
        } else {
            return 'sender';
        }
    }

    private formatDateTime(value: moment.Moment, format: EnumDateTimeFormat): string {
        return DateTimeFormatter.formatDateTime(value, format);
    }

    private avatarSpacingCss() {
        if (this.value.showAvatar) {
            return 'avatar-spacing';
        }
        return '';
    }

    private data(): any  {
        return {};
    }
}

Vue.component('db-message-list', MessageListComponent);
