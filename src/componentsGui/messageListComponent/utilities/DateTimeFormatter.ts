import moment                                   from  'moment';
import { EnumDateTimeFormat } from '../enums/MessageListEnums';
export default class DateTimeFormatter {

    public static formatDateTime(value: moment.Moment, format: EnumDateTimeFormat): string {

        switch (format) {
            case EnumDateTimeFormat.none:
                return '';

            case EnumDateTimeFormat.time:
                return value.format('h:mm a');

            case EnumDateTimeFormat.date:
                return value.format('D MMMM YYYY');

            case EnumDateTimeFormat.dateTime:
                return value.format('D MMMM YYYY h:mm a');
        }
        return '';
    }
}
