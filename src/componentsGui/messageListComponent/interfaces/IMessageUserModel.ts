export default interface IMessageUserModel {
    userId: string;
    userName: string;
    avatarImageUrl: string;
}
