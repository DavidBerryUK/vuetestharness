import IMessageUser                             from './IMessageUserModel';
import moment                                   from 'moment';
export default interface IMessageModel {

    id: string;

    author: IMessageUser;

    sendDateTime: moment.Moment;

    content: string;

    authorFirstAppearance: boolean;

}
