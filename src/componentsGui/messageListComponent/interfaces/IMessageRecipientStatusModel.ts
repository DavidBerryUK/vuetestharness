import { EnumMessageRecipientStatus }           from '../enums/MessageListEnums';
import IMessageUserModel                        from './IMessageUserModel';
import moment                                   from 'moment';

export default interface IMessageRecipientStatusModel {

        user: IMessageUserModel;
        readDateTime: moment.Moment;
        deliveredDateTime: moment.Moment;

        status: EnumMessageRecipientStatus;

}
