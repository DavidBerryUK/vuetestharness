import { EnumDateTimeFormat }                   from '../enums/MessageListEnums';
import IMessageModel                            from './IMessageModel';
import IMessageUserModel                        from './IMessageUserModel';

export default interface IChatModel {

    viewer: IMessageUserModel;

    showAvatar: boolean;

    isViewer(author: IMessageModel): boolean;

    addMessage(messages: IMessageModel): void;

    addMessages(messages: Array<IMessageModel>): void;

    messageDateTimeFormat: EnumDateTimeFormat;
}
