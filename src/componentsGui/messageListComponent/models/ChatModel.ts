import { EnumDateTimeFormat }                   from '../enums/MessageListEnums';
import IChatModel                               from '../interfaces/IChatModel';
import IMessageModel                            from './MessageModel';
import IMessageUserModel                        from '../interfaces/IMessageUserModel';
import MessageUserModel                         from './MessageUserModel';
import moment                                   from  'moment';
import DateTimeFormatter                        from '../utilities/DateTimeFormatter';


export default class ChatModel implements IChatModel {

    public viewer: IMessageUserModel;
    public showAvatar: boolean;
    private messages: Array<IMessageModel>;

    public messageDateTimeFormat: EnumDateTimeFormat = EnumDateTimeFormat.time;
    public daySeparatorDateTimeFormat: EnumDateTimeFormat = EnumDateTimeFormat.date;


    /**
     * a list of MAP of unique users in the chat
     *
     * @type {Map<string, IMessageUserModel>}
     * @memberof ChatModel
     */
    public usersMap: Map<string, IMessageUserModel>;

    constructor(viewer: IMessageUserModel) {
        this.viewer = viewer;
        this.showAvatar = true;
        this.usersMap = new Map<string, IMessageUserModel>();
        this.messages = new Array<IMessageModel>();
    }

    public static get empty(): ChatModel {
        const viewer = new MessageUserModel('', '', '');
        const chat = new ChatModel(viewer);
        return chat;
    }

    public isViewer(message: IMessageModel): boolean {
            return this.viewer === message.author;
    }


    /**
     * return a list of users that exist in the chat
     *
     * @readonly
     * @type {Array<IMessageUserModel>}
     * @memberof ChatModel
     */
    public get users(): Array<IMessageUserModel> {
        return  Array.from(this.usersMap.values());
    }

    public addMessage(messages: IMessageModel) {
        this.messages.push(messages);
        this.addUserToDictionary(messages.author);
        this.processMessageGroups();
    }

    public addMessages(messages: Array<IMessageModel>) {
        this.messages = this.messages.concat(messages);
        const users = messages.map((message) => message.author);
        this.addUsersToDictionary(users);
        this.processMessageGroups();
    }



    private addUserToDictionary(user: IMessageUserModel) {
        if ( !this.usersMap.has(user.userId) ) {
            this.usersMap.set(user.userId, user);
        }
    }

    private addUsersToDictionary(users: Array<IMessageUserModel>) {
        const uniqueUsers = Array.from(new Set(users));
        uniqueUsers.forEach((user: IMessageUserModel) => {
            if ( !this.usersMap.has(user.userId) ) {
                this.usersMap.set(user.userId, user);
            }
        });
    }

    public processMessageGroups(): void {


        if ( this.messages.length >= 1) {

            let firstMsgInGroup = this.messages[0];
            let dateChangeTracker = moment().add(-1000, 'years');

            this.messages.forEach((message: IMessageModel) => {

                if ( ! message.sendDateTime.isSame(dateChangeTracker, 'day') ) {
                    message.groupFirstMessageOfTheDay = true;
                    dateChangeTracker = message.sendDateTime;
                } else {
                    message.groupFirstMessageOfTheDay = false;
                }

                if (firstMsgInGroup.author === message.author) {
                    message.authorFirstAppearance = false;
                } else {
                    firstMsgInGroup = message;
                }

                firstMsgInGroup.authorFirstAppearance = true;
                console.log(this.messageDateTimeFormat);
                firstMsgInGroup.groupSendDateTime = message.sendDateTime;
            });
        }
    }
}
