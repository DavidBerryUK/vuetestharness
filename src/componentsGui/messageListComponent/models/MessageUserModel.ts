import IMessageUserModel                        from '../interfaces/IMessageUserModel';

export default class MessageUserModel implements IMessageUserModel {
    public userId: string;
    public userName: string;
    public avatarImageUrl: string;

    constructor(userId: string, userName: string, avatarImageUrl: string) {
        this.userId = userId;
        this.userName = userName;
        this.avatarImageUrl = avatarImageUrl;
    }
}
