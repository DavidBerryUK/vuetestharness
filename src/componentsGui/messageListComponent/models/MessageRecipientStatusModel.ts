import { EnumMessageRecipientStatus }           from '../enums/MessageListEnums';
import IMessageRecipientStatusModel             from '../interfaces/IMessageRecipientStatusModel';
import IMessageUserModel                        from '../interfaces/IMessageUserModel';
import moment                                   from 'moment';

export default class MessageRecipientStatusModel implements IMessageRecipientStatusModel {

        public user: IMessageUserModel;
        public readDateTime: moment.Moment;
        public deliveredDateTime: moment.Moment;

        public get status(): EnumMessageRecipientStatus {
            return EnumMessageRecipientStatus.undelivered;
        }

        constructor(user: IMessageUserModel, readDateTime: moment.Moment, deliveredDateTime: moment.Moment) {
            this.user = user;
            this.readDateTime = readDateTime;
            this.deliveredDateTime = deliveredDateTime;
        }

}
