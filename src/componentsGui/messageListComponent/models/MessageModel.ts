import IMessageModel                            from '../interfaces/IMessageModel';
import IMessageUserModel                        from './MessageUserModel';
import moment                                   from 'moment';

export default class MessageModel implements IMessageModel {
    public id: string;
    public author: IMessageUserModel;
    public sendDateTime: moment.Moment;
    public content: string;
    public authorFirstAppearance: boolean;
    public groupSendDateTime: moment.Moment;
    public groupFirstMessageOfTheDay: boolean;

    public constructor(id: string, author: IMessageUserModel, sendDateTime: moment.Moment,  content: string) {
        this.id = id;
        this.author = author;
        this.sendDateTime = sendDateTime.clone();
        this.content = content;
        this.authorFirstAppearance = true;
        this.groupSendDateTime = sendDateTime.clone();
        this.groupFirstMessageOfTheDay = true;
    }
}
