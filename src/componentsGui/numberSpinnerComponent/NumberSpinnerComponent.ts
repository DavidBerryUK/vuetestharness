import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';


@Component
export default class NumberSpinnerComponent extends Vue {

    @Prop()
    public value!: number;

    @Prop({ type: Number, default: 0 })
    public min!: number;

    @Prop({ type: Number, default: 999 })
    public max!: number;

    public number: number = 0;

    public mounted() {
        this.number = this.value;
    }

    @Watch('value')
    private onValueChanged(value: number, oldValue: number) {
        this.number = value;
    }

    @Watch('number')
    private onNumberChanged(value: number, oldValue: number) {
        console.log('number changed');
        this.$emit('input', this.number);
    }

    private onAddClicked() {
        this.number++;
    }

    private onSubtractClicked() {
        this.number--;
    }

    private onPasteHandler(event: ClipboardEvent ) {

        const originalText = event.clipboardData.getData('text');
        if (originalText === null) {
            event.preventDefault();
            return;
        }
        // only allow numbers to be pasted;
        const amendText = originalText.replace ( /[^0-9]/g, '' );
        if ( amendText !== originalText) {
            event.preventDefault();
        }
    }

    private onKeyDownHandler(event: KeyboardEvent) {

        // up arrow - increment number by 1
        if ( event.keyCode === 38) {
            this.number++;
            event.preventDefault();
            return;
        }

        // down arrow - decrement number by 1
        if ( event.keyCode === 40) {
            this.number--;
            event.preventDefault();
            return;
        }

        const valid = this.isCharCodeValid(event);

        if ( !valid) {
            event.preventDefault();
        }
    }

    private isCharCodeValid(event: KeyboardEvent): boolean {

        // any char with command key allow, cut page etc
        if (event.metaKey) {
            return true;
        }

        const charCode = event.which;

        // key functions, deletes, enter, move cursor etc
        if ( charCode >= 0 && charCode <= 46) {
            return true;
        }

        // numbers, standard or number pad
        if ( (charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105)) {
            return true;
        }

        return false;
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-number-spinner', NumberSpinnerComponent);
