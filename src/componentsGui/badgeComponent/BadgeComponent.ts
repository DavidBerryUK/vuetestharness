import { enumBadgeAlignment }                   from './enums/BadgeEnums';
import { enumBadgePosition }                    from './enums/BadgeEnums';
import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import Vue                                      from 'vue';

@Component
export default class BadgeComponent extends Vue   {

    @Prop()
    public value!: number;

    @Prop()
    public alignment!: string;

    @Prop()
    public position!: string;

    get showBadge(): boolean {
        return this.value > 0;
    }

    private get textValue(): string {
        if (this.value > 9999) {
            return '9999+';
        }
        return `${this.value}`;
    }

    private get textCss(): string {

        let widthCss = '';
        let alignCss = '';

        // determine width
        //
        if ( this.value > 9999 ) {
            widthCss = 'width-5';
        } else if ( this.value > 999 ) {
            widthCss = 'width-4';
        } else if ( this.value > 99 ) {
            widthCss = 'width-3';
        } else if ( this.value > 9 ) {
            widthCss = 'width-2';
        } else {
            widthCss = 'width-1';
        }


        let alignmentValue = '';
        let positionValue = '';
        // determine alignment
        //
        if (this.alignment) {
            alignmentValue = this.alignment;
        }

        if (this.position) {
            positionValue = this.position;
        }

        if (alignmentValue !==  '') {
            switch (alignmentValue) {
                case enumBadgeAlignment.topleft:
                case enumBadgeAlignment.middleLeft:
                case enumBadgeAlignment.bottomLeft:
                    alignCss = 'left';
                    break;
                case enumBadgeAlignment.topRight:
                case enumBadgeAlignment.middleRight:
                case enumBadgeAlignment.bottomRight:
                    alignCss = 'right';
                    break;
            }
        }

        if ( positionValue === enumBadgePosition.outside ) {
            if ( alignCss === 'left' ) {
                alignCss = 'right';
            } else if (alignCss === 'right') {
                alignCss = 'left';
            }
        }

        if ( positionValue === enumBadgePosition.border ) {
            alignCss = 'center';
        }

        return alignCss + ' ' + widthCss;
    }

    private get badgePosition(): string {
        if (this.position) {
            const value = this.position;

            switch (value) {
                case enumBadgePosition.inside:
                    return 'inside';
                case enumBadgePosition.border:
                    return 'border';
                case enumBadgePosition.outside:
                    return 'outside';
            }
        }
        return 'inside';
    }


    private get badgeAlignment(): string {

        if (this.alignment) {

            switch (this.alignment) {
                case enumBadgeAlignment.topleft:
                    return 'top-left';
                case enumBadgeAlignment.topCenter:
                    return 'top-center';
                case enumBadgeAlignment.topRight:
                    return 'top-right';
                case enumBadgeAlignment.middleLeft:
                    return 'middle-left';
                case enumBadgeAlignment.middleCenter:
                    return 'middle-center';
                case enumBadgeAlignment.middleRight:
                    return 'middle-right';
                case enumBadgeAlignment.bottomLeft:
                    return 'bottom-left';
                case enumBadgeAlignment.bottomCenter:
                    return 'bottom-center';
                case enumBadgeAlignment.bottomRight:
                    return 'bottom-right';
            }
        }
        return 'top-left';
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-badge', BadgeComponent);
