export enum enumBadgeAlignment {
    topleft = 'TopLeft',
    topCenter = 'TopCenter',
    topRight = 'TopRight',
    middleLeft = 'MiddleLeft',
    middleCenter = 'MiddleCenter',
    middleRight = 'MiddleRight',
    bottomLeft = 'BottomLeft',
    bottomCenter = 'BottomCenter',
    bottomRight = 'BottomRight',
}

export enum enumBadgePosition {
    inside = 'Inside',
    border = 'Border',
    outside = 'Outside',
}
