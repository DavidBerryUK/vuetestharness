import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import DataKeyItemModel                         from '../models/DataKeyItemModel';
import Vue                                      from 'vue';

@Component({
    components: {
    },
  })
export default class DataKeyListComponent extends Vue   {

    @Prop()
    public value!: Array<DataKeyItemModel>;

    public data(): any  {
        return {};
    }
  }

Vue.component('db-data-key-list', DataKeyListComponent);
