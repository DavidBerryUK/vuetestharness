import ColourModel                              from '@/componentsGui/coloursComponents/models/ColourModel';

export default class DataKeyItemModel {

    public name: string;
    public colour: ColourModel;

    constructor(name: string, colour: ColourModel) {
        this.name = name;
        this.colour = colour;
    }
}
