export enum EnumAvatarBorderStyle {
    none = 'None',
    greenQuick = 'GreenQuick',
    greySlow = 'GreySlow',
}
