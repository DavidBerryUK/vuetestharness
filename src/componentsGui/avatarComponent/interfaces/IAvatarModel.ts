export interface IAvatarModel {
    name: string;
    alternativeText: string;
    imageUrl: string;
    selected: boolean;

    borderAnimation: string;
}
