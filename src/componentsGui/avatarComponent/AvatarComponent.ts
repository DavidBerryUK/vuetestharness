import { EnumAvatarBorderStyle }                from './enums/AvatarEnums';
import { IAvatarModel }                         from './interfaces/IAvatarModel';
import { Prop }                                 from 'vue-property-decorator';
import BadgeComponent                           from '../badgeComponent/BadgeComponent';
import Component                                from 'vue-class-component';
import ConvertStringToBoolean                   from '@/utilities/convertions/ConvertStringToBoolean';
import Vue                                      from 'vue';

@Component({
    components: {
        BadgeComponent,
    },
  })
export default class AvatarComponent extends Vue   {

    @Prop()
    public value!: IAvatarModel;

    @Prop()
    public badgeAlignment!: string;

    @Prop()
    public badgePosition!: string;

    private get selectedCss(): string {
        if (ConvertStringToBoolean.convert(this.value.selected, false)) {
            return 'selected';
        }
        return '';
    }

    private get Label1Css(): string {
        if (this.value.alternativeText != null && this.value.alternativeText.length > 0) {
            return 'switch-1';
        }
        return '';
    }

    private get Label2Css(): string {
        if (this.value.alternativeText != null && this.value.alternativeText.length > 0) {
            return 'switch-2';
        }
        return '';
    }

    private get showSvgBorder(): boolean {
        if (this.value.borderAnimation) {
            if ( this.value.borderAnimation === EnumAvatarBorderStyle.none ) {
                return false;
            }
            return true;
        }
        return false;
    }
    private get svgAnimationClass(): string {
        if (this.value.borderAnimation) {
            switch (this.value.borderAnimation) {
                case EnumAvatarBorderStyle.greenQuick:
                    return 'svg-container svg-container-avatar-green-quick-animation';

                case EnumAvatarBorderStyle.greySlow:
                    return 'svg-container svg-container-avatar-grey-slow-animation';
            }
        }
        return '';
    }

    private get svgPathClass(): string {
        if (this.value.borderAnimation) {
            switch (this.value.borderAnimation) {
                case EnumAvatarBorderStyle.greenQuick:
                    return 'svg-path-green-quick';
                case EnumAvatarBorderStyle.greySlow:
                    return 'svg-path-grey-slow';
            }
        }
        return 'svg-path-online';
    }

    public data(): any  {
        return {};
    }
  }

Vue.component('db-avatar', AvatarComponent);
