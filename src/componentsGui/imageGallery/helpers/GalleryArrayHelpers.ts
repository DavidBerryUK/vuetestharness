import { EnumImageId }                          from '../constants/GalleryEnums';
import GalleryImageModel                        from '../models/GalleryImageModel';
import GalleryModel                             from '../models/GalleryModel';

export default class GalleryArrayHelpers {

    /**
     * Sort array of GalleryModel by name
     *
     * @static
     * @param {Array<GalleryModel>} galleries
     * @returns {Array<GalleryModel>}
     * @memberof GalleryModelSorter
     */
    public static sortByName(galleries: Array<GalleryModel>): Array<GalleryModel> {
        return galleries.sort((a: GalleryModel, b: GalleryModel) => {
            return a.name.localeCompare(b.name);
        });
    }

    public static getImageById(galleries: Array<GalleryModel>, id: EnumImageId): GalleryImageModel {

        // tslint:disable-next-line:prefer-for-of
        for (let g = 0; g < galleries.length; g++) {
            const gallery = galleries[g];

            // tslint:disable-next-line:prefer-for-of
            for (let s = 0; s < gallery.sections.length; s++) {
                const section = gallery.sections[s];

                // tslint:disable-next-line:prefer-for-of
                for (let i = 0; i < section.images.length; i++) {

                    if ( section.images[i].id === id) {
                        return section.images[i];
                    }
                }
            }
        }
        return  GalleryImageModel.None;
    }
}
