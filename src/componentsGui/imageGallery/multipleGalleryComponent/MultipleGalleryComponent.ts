import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ConvertStringToBoolean                   from '@/utilities/convertions/ConvertStringToBoolean';
import GalleryImageModel                        from '../models/GalleryImageModel';
import GalleryModel                             from '../models/GalleryModel';
import GallerySectionModel                      from '../models/GallerySectionModel';
import SingleGalleryComponent                   from '../singleGalleryComponent/SingleGalleryComponent';
import Vue                                      from 'vue';
import GalleryArrayHelpers                      from '../helpers/GalleryArrayHelpers';


@Component({
    components: {
        SingleGalleryComponent,
    },
  })
export default class MultipleGalleryComponent extends Vue {

    @Prop()
    public galleries!: Array<GalleryModel>;

    @Prop()
    public selectedImage!: GalleryImageModel;

    @Prop()
    public autoShowSelectedImage!: boolean;

    @Prop()
    public autoOpenOnLoad!: boolean;

    public selectedGalleryId: string = '';

    private selectedGalleryModel = new GalleryModel('', '', new Array<GallerySectionModel>());

    private get sortedGalleries(): Array<GalleryModel> {
        return GalleryArrayHelpers.sortByName(this.galleries);
    }

    private mounted() {
        if (ConvertStringToBoolean.convert(this.autoOpenOnLoad, false)) {
            if (this.selectedImage.isNone) {
                this.openFirstGallerySection();
            } else {
                this.makeSelectedImageVisible(true);
            }
        }
    }

    @Watch('selectedImage')
    private onSelectedImageChanged(value: GalleryImageModel, oldValue: GalleryImageModel) {
        this.makeSelectedImageVisible(false);
    }

    private openFirstGallerySection() {
        if (this.galleries.length === 0) {
            return;
        }
        const firstGallery = this.sortedGalleries[0];
        this.selectedGalleryId = firstGallery.id;
        this.selectedGalleryModel = firstGallery;
    }

    private makeSelectedImageVisible(forceVisible: boolean) {
        if (forceVisible || ConvertStringToBoolean.convert(this.autoShowSelectedImage, false)) {
            this.galleries.forEach((gallery: GalleryModel) => {
                gallery.sections.forEach((section: GallerySectionModel) => {
                    section.images.forEach((image: GalleryImageModel) => {
                        if (this.selectedImage === image) {
                            this.selectedGalleryId = gallery.id;
                            this.selectedGalleryModel = gallery;
                        }
                    });
                });
            });
        }
    }

    private onGalleryClick(gallery: GalleryModel) {
        this.selectedGalleryModel = gallery;
        this.$emit('selectedNewGallery', gallery);
    }

    private onSelectedNewSection(section: GallerySectionModel) {
        this.$emit('selectedNewSection', section);
    }

    private onSelectedNewImage(image: GalleryImageModel) {
        this.$emit('selectedNewImage', image);
    }

    public data(): any {
        return {};
    }
}

Vue.component('db-multiple-gallery', MultipleGalleryComponent);
