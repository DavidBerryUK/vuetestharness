import { Prop }                                 from 'vue-property-decorator';
import { Watch }                                from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ConvertStringToBoolean                   from '@/utilities/convertions/ConvertStringToBoolean';
import GalleryImageModel                        from '../models/GalleryImageModel';
import GalleryModel                             from '../models/GalleryModel';
import GallerySectionModel                      from '../models/GallerySectionModel';
import Vue                                      from 'vue';


@Component
export default class SingleGalleryComponent extends Vue {

    @Prop()
    public gallery!: GalleryModel;

    @Prop()
    public selectedImage!: GalleryImageModel;


    @Prop()
    public autoShowSelectedImage!: boolean;

    @Prop()
    public autoOpenOnLoad!: boolean;

    private currentOpenSection?: GallerySectionModel;

    private currentOpenPanel: number = -1;

    private mounted() {
        if (ConvertStringToBoolean.convert(this.autoOpenOnLoad, false)) {
                this.$nextTick(function() {
                if (this.selectedImage.isNone) {
                    this.openFirstSection();
                } else {
                    this.makeSelectedImageVisible(true);
                }
            });
        }
    }

    @Watch('selectedImage')
    private onSelectedImageChanged(value: GalleryImageModel, oldValue: GalleryImageModel) {
        this.$nextTick(function() {
            this.makeSelectedImageVisible(false);
        });
    }

    @Watch('gallery')
    private onGalleryChanged(value: GalleryModel, oldValue: GalleryModel) {
        this.currentOpenPanel = -1;

        this.$nextTick(function() {
            if (this.autoOpenOnLoad) {
                if (this.selectedImage.isNone) {
                    this.openFirstSection();
                } else {
                    this.makeSelectedImageVisible(true);
                }
            } else {
                this.makeSelectedImageVisible(false);
            }
        });
    }

    private openFirstSection() {
        if (this.gallery.sections.length >= 1) {
            this.currentOpenPanel = 0;
            this.$forceUpdate();
        }
    }

    public makeSelectedImageVisible(forceVisible: boolean) {

        if (forceVisible || ConvertStringToBoolean.convert(this.autoShowSelectedImage, false)) {

            this.$nextTick(function() {
                // this.$nextTick(function() {

                    let sectionIndex: number = 0;
                    let index: number = 0;
                    this.gallery.sections.forEach((section: GallerySectionModel) => {
                        section.images.forEach((image: GalleryImageModel) => {
                            if (this.selectedImage.id === image.id) {
                                sectionIndex = index;
                                this.$nextTick(function() {
                                    this.currentOpenPanel = sectionIndex;
                                });
                            }
                        });
                        index++;
                    });
                });
            // });
        }
    }

    private onSelectionClick(section: GallerySectionModel) {
         if ( this.currentOpenPanel === undefined || this.currentOpenSection !== section) {
            this.currentOpenSection = section;
            this.$emit('selectedNewSection', section);
         }
    }

    private onCardClick(image: GalleryImageModel) {
        this.$emit('selectedNewImage', image);
    }

}

Vue.component('db-single-gallery', SingleGalleryComponent);
