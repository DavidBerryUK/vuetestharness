import GalleryImageModel                        from '../models/GalleryImageModel';
import GallerySectionModel                      from '../models/GallerySectionModel';
import { EnumImageId }                          from '../constants/GalleryEnums';

export default class BusinessSectionsFactory {


    public static getSectionPeopleFullSize(): GallerySectionModel {

        const section = new GallerySectionModel(
            'People full sized',
             [
                 new GalleryImageModel(EnumImageId.BusinessPeopleAnnFullSized, 'Ann', 'business//people/ann.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleBarryFullSized, 'Barry', 'business//people/barry.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleCarryFullSized, 'Carry', 'business//people/carry.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleDaveFullSized, 'Dave', 'business//people/dave.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleEthanFullSized, 'Ethan', 'business//people/ethan.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleFionaFullSized, 'Fiona', 'business//people/fiona.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessPeopleGaryFullSized, 'Gary', 'business//people/gary.png', 800, 400),
            ]);

        return section;
    }

    public static getSectionPeopleThumbnail(): GallerySectionModel {

        const section = new GallerySectionModel(
            'People thumbnail sized',
             [
                 new GalleryImageModel(EnumImageId.BusinessPeopleAnnThumbnail, 'Ann', 'business//people/ann-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleBarryThumbnail, 'Barry', 'business//people/barry-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleCarryThumbnail, 'Carry', 'business//people/carry-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleDaveThumbnail, 'Dave', 'business//people/dave-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleEthanThumbnail, 'Ethan', 'business//people/ethan-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleFionaThumbnail, 'Fiona', 'business//people/fiona-tn.png', 96, 96),
                 new GalleryImageModel(EnumImageId.BusinessPeopleGaryThumbnail, 'Gary', 'business//people/gary-tn.png', 96, 96),
            ]);

        return section;
    }

    public static getSectionProducts(): GallerySectionModel {
        const section = new GallerySectionModel(
            'Products',
             [
                 new GalleryImageModel(EnumImageId.BusinessProductsBeard, 'Beard', 'business//products/beard.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsBeer, 'Beer', 'business//products/beer.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsCar, 'Car', 'business//products/car.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsChocolate, 'Chocolate', 'business//products/chocolate.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsCup, 'Cup', 'business//products/cup.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsGiftCard, 'Gift Card', 'business//products/giftcard.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsSpeaker, 'Speaker', 'business//products/Speaker.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsSweater, 'Sweater', 'business//products/sweater.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessProductsSweets, 'Sweets', 'business//products/sweets.png', 800, 400),
            ]);

        return section;
    }

    public static getSectionTeamsThumbnail(): GallerySectionModel {
        const section = new GallerySectionModel(
            'Teams thumbnail sized',
             [
                 new GalleryImageModel(EnumImageId.BusinessTeamsBackOfficeThumbnail, 'Back Office', 'business//teams/backoffice-tn.png', 72, 72),
                 new GalleryImageModel(EnumImageId.BusinessTeamsFinanceThumbnail, 'Finance', 'business//teams/finance-tn.png', 72, 72),
                 new GalleryImageModel(EnumImageId.BusinessTeamsSalesThumbnail, 'Sales', 'business//teams/sales-tn.png', 72, 72),
            ]);

        return section;
    }

    public static getSectionTeamsFullSize(): GallerySectionModel {
        const section = new GallerySectionModel(
            'Teams full sized',
             [
                 new GalleryImageModel(EnumImageId.BusinessTeamsBackOfficeFullSized, 'Back Office', 'business//teams/backoffice.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessTeamsFinanceFullSized, 'Finance', 'business//teams/finance.png', 800, 400),
                 new GalleryImageModel(EnumImageId.BusinessTeamsSalesFullSized, 'Sales', 'business//teams/sales.png', 800, 400),
            ]);

        return section;
    }
}
