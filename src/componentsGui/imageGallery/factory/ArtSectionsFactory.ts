import GalleryImageModel                        from '../models/GalleryImageModel';
import GallerySectionModel                      from '../models/GallerySectionModel';
import { EnumImageId }                          from '../constants/GalleryEnums';
export default class ArtSectionsFactory {

    public static getSectionClaudeMonet(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Claude Monet',
            [
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetGardenAtGiverny, 'Garden at Giverny', 'art//claudeMonet/GardenAtGiverny.jpg', 600, 535),
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetPoplars, 'Poplars', 'art//claudeMonet/Poplars.jpg', 250, 239),
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetRedBoatsAtArgenteuil, 'RedBoats at Argenteuil', 'art//claudeMonet/RedBoatsAtArgenteuil.jpg', 600, 562),
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetTheManneporteNearEtretat, 'The Manneporte Near Etretat', 'art//claudeMonet/TheManneporteNearEtretat.jpg', 430, 600),
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetWaterLilies, 'Water Lilies', 'art//claudeMonet/WaterLilies.jpg', 600, 576),
                 new GalleryImageModel(EnumImageId.ArtClaudeMonetWaterLilyPond, 'Water Lily Pond', 'art//claudeMonet/WaterLilyPond.jpg', 300, 291),
            ]);

        return section;
    }
    public static getSectionEdvardMunch(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Edvard Munch',
            [
                new GalleryImageModel(EnumImageId.ArtEdvardMunchBetweenTheClockAndTheBed, 'Between the Clock and the bed', 'art//edvardMunch/BetweenTheClockAndTheBed.jpg', 478, 600),
                new GalleryImageModel(EnumImageId.ArtEdvardMunchTheNightWanderer, 'The Night Wanderer', 'art//edvardMunch/TheNightWanderer.jpg', 120, 160),
                new GalleryImageModel(EnumImageId.ArtEdvardMunchTheScream, 'The Scream', 'art//edvardMunch/TheScream.jpg', 475, 600),
                new GalleryImageModel(EnumImageId.ArtEdvardMunchTheStorm, 'The Storm', 'art//edvardMunch/TheStorm.jpg', 400, 282),
                new GalleryImageModel(EnumImageId.ArtEdvardMunchTheYellowLog, 'The Yellow Log', 'art//edvardMunch/TheYellowLog.jpg', 369, 300),
            ]);

        return section;
    }
    public static getSectionLeonidAfremov(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Leonid Afremov',
            [
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovDeers, 'Deers', 'art//leonidAfremov/Deers.jpg', 800, 585),
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovFallenLeafs, 'Fallen Leafs', 'art//leonidAfremov/FallenLeafs.jpg', 800, 599),
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovLastDream, 'Last Dream', 'art//leonidAfremov/LastDream.jpg', 673, 500),
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovNightAura, 'Night Aura', 'art//leonidAfremov/NightAura.jpg', 475, 600),
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovStrollInAnAutumnPark, 'Stroll in an Autumn Park', 'art//leonidAfremov/StrollInAnAutumnPark.jpg', 450, 600),
                new GalleryImageModel(EnumImageId.ArtLeonidAfremovVeniceGrandCanal, 'Venice Grand Canal', 'art//leonidAfremov/VeniceGrandCanal.jpg', 500, 365),
            ]);

        return section;
    }
    public static getSectionVincentVanGogh(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Vincent van Gogh',
            [
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghSelfPortraitBlueWavy, 'Self Portrait Blue Wavy.', 'art//vincentVanGogh/SelfPortraitBlueWavy.jpg', 498, 600),
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheChurchAtAuvers, 'The Church At Auvers.', 'art//vincentVanGogh/TheChurchAtAuvers.jpg', 233, 300),
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheNightCafe, 'The Night Cafe', 'art//vincentVanGogh/TheNightCafe.jpg', 600, 463),
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghThePrisonExerciseYard, 'The Prison Exercise Yard', 'art//vincentVanGogh/ThePrisonExerciseYard.jpg', 500, 600),
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'art//vincentVanGogh/TheStarryNight.jpg', 454, 300),
                new GalleryImageModel(EnumImageId.ArtVincentVanGoghVincentVanGogh, 'Vincent van Gogh', 'art//vincentVanGogh/VincentVanGogh.jpg', 600, 600),
            ]);

        return section;
    }
}
