import GalleryImageModel                        from '../models/GalleryImageModel';
import GallerySectionModel                      from '../models/GallerySectionModel';
import { EnumImageId }                          from '../constants/GalleryEnums';


export default class AnimalSectionsFactory {


    public static getSectionAnimalsWater(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Animals in Water',
             [
                new GalleryImageModel(EnumImageId.AnimalsWaterBlackMoorGoldfish, 'BlackMoor Goldfish', 'animals//water/BlackMoorGoldFish.jpg', 400, 203),
                new GalleryImageModel(EnumImageId.AnimalsWaterCrab, 'Crab', 'animals//water/crab.jpg', 500, 271),
                new GalleryImageModel(EnumImageId.AnimalsWaterClownFish, 'Clown Fish', 'animals//water/fishClown.jpg', 800, 500),
                new GalleryImageModel(EnumImageId.AnimalsWaterGoldfish, 'Goldfish', 'animals//water/gold-fish.jpg', 400, 270),
                new GalleryImageModel(EnumImageId.AnimalsWaterJellyfish, 'Jellyfish', 'animals//water/jellyfish.jpg', 600, 360),
                new GalleryImageModel(EnumImageId.AnimalsWaterSeahorse, 'Seahorse', 'animals//water/seahorse.jpg', 400, 600),
            ]);

        return section;
    }
    public static getSectionAnimalsLand(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Animals on the Land',
             [
                new GalleryImageModel(EnumImageId.AnimalsLandBear, 'Bear', 'animals//land/bear.jpg', 600, 400),
                new GalleryImageModel(EnumImageId.AnimalsLandFox, 'Fox', 'animals//land/fox.jpg', 350, 350),
                new GalleryImageModel(EnumImageId.AnimalsLandOtter, 'Otter', 'animals//land/otter.jpeg', 600, 400),
                new GalleryImageModel(EnumImageId.AnimalsLandRaccoon, 'Raccoon', 'animals//land/raccoon.jpg', 600, 600),
                new GalleryImageModel(EnumImageId.AnimalsLandTapir, 'Tapir', 'animals//land/tapir.jpg', 600, 436),
            ]);

        return section;
    }

    public static getSectionAnimalsAir(): GallerySectionModel {

        const section = new GallerySectionModel(
            'Animals of the Air',
             [
                 new GalleryImageModel(EnumImageId.AnimalsAirGoose, 'Goose', 'animals//air/canadagoose.jpg', 600, 400),
                 new GalleryImageModel(EnumImageId.AnimalsAirHawk, 'Hawk', 'animals//air/sparrowhawk.png', 200, 300),
                 new GalleryImageModel(EnumImageId.AnimalsAirParrot, 'Parrot', 'animals//air/parrot.jpg', 500, 370),
                 new GalleryImageModel(EnumImageId.AnimalsAirPigeon, 'Pigeon', 'animals//air/woodpigeon.jpg', 400, 360),
                 new GalleryImageModel(EnumImageId.AnimalsAirOwl, 'Owl', 'animals//air/owl.png', 600, 240),
            ]);

        return section;
    }

}
