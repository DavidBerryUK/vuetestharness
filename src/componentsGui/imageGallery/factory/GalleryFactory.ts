import AnimalSectionsFactory                    from './AnimalSectionsFactory';
import ArtSectionsFactory                       from './ArtSectionsFactory';
import BusinessSectionsFactory                  from './BusinessSectionsFactory';
import GalleryModel                             from '../models/GalleryModel';

export default class GalleryFactory {

    public static getBusinessGallery(): GalleryModel {

        const gallery = new GalleryModel('bus', 'Business',
            [
                BusinessSectionsFactory.getSectionPeopleFullSize(),
                BusinessSectionsFactory.getSectionPeopleThumbnail(),
                BusinessSectionsFactory.getSectionProducts(),
                BusinessSectionsFactory.getSectionTeamsFullSize(),
                BusinessSectionsFactory.getSectionTeamsThumbnail(),
            ]);

        return gallery;
    }

    public static getAnimalGallery(): GalleryModel {
        const gallery = new GalleryModel('aml', 'Animals',
            [
                AnimalSectionsFactory.getSectionAnimalsAir(),
                AnimalSectionsFactory.getSectionAnimalsLand(),
                AnimalSectionsFactory.getSectionAnimalsWater(),
            ]);
        return gallery;
    }

    public static getArtGallery(): GalleryModel {
        const gallery = new GalleryModel('art', 'Art',
            [
                ArtSectionsFactory.getSectionClaudeMonet(),
                ArtSectionsFactory.getSectionEdvardMunch(),
                ArtSectionsFactory.getSectionLeonidAfremov(),
                ArtSectionsFactory.getSectionVincentVanGogh(),
            ]);
        return gallery;
    }

}
