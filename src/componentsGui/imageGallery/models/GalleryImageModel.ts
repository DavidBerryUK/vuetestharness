import GallerySectionModel                      from './GallerySectionModel';
import { EnumImageId }                          from '../constants/GalleryEnums';

export default class GalleryImageModel {

    public id: EnumImageId;
    public fileName: string;
    public name: string;
    public width: number;
    public height: number;
    private none: boolean;

    public section?: GallerySectionModel;


    constructor(id: EnumImageId, name: string, fileName: string, width: number, height: number) {

        if ( width === 0 && height === 0) {
            throw new Error('GallerySectionModel can not create image with dimensions 0x0, use GalleryImageModel.Empty');
        }

        this.id = id;
        this.fileName = fileName;
        this.name = name;
        this.width = width;
        this.height = height;
        this.none = false;
    }

    public static get None(): GalleryImageModel {
        const model =  new GalleryImageModel(EnumImageId.None, '', '', 1, 1);
        model.none = true;
        model.width = 0;
        model.height = 0;
        return model;
    }

    public get isNone(): boolean {
        return this.none;
    }

    public get isImageSelected() {
        if (this.id === EnumImageId.None) {
            return false;
        }
        return true;
    }

    public get localFileName(): string {
        return `\\images\\${this.fileName}`;
    }

    public get toString() {
        if ( this.none ) {
            return 'no image';
        } else {
            return `${this.id} ${this.name} ${this.width} x ${this.height}`;
        }
    }
}
