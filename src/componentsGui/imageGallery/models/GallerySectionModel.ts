import GalleryImageModel                        from './GalleryImageModel';

export default class GallerySectionModel {

    public name: string;

    public images: Array<GalleryImageModel>;

    constructor(name: string, images: Array<GalleryImageModel>) {
        this.name = name;
        this.images = images;
    }

    public get imageCount(): number {
        return this.images.length;
    }
}
