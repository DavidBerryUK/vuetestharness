import GallerySectionModel                      from './GallerySectionModel';
import GalleryImageModel from './GalleryImageModel';

export default class GalleryModel {

    public id: string;
    public name: string;
    public sections: Array<GallerySectionModel>;


    constructor( id: string, name: string, sections: Array<GallerySectionModel> ) {
        this.id = id;
        this.name = name;
        this.sections = sections;
    }

    public get allImages(): Array<GalleryImageModel> {

        let data = new Array<GalleryImageModel>();
        this.sections.forEach((section: GallerySectionModel ) => {
            data = data.concat(section.images);
        });
        return data;
    }

}
