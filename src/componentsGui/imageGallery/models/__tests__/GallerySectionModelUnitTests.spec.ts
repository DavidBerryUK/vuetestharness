import GalleryImageModel                        from '../GalleryImageModel';
import GallerySectionModel                      from '../GallerySectionModel';
import { EnumImageId }                          from '../../constants/GalleryEnums';

describe('GallerySectionModel Unit Tests', () => {

     test('test constructor', () => {
        const image1 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'TheStarryNight.jpg', 1000, 1200);
        const image2 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheNightCafe, 'Almond Blossoms', 'AlmondBlossoms.jpg', 1000, 1200);

        const section = new GallerySectionModel('Vincent van Gogh', [image1, image2]);

        expect(section).toBeDefined();
        expect(section.name).toBe('Vincent van Gogh');
        expect(section.images).toBeDefined();
        expect(section.images.length).toBe(2);
        expect(section.images[0]).toBe(image1);
        expect(section.images[1]).toBe(image2);
        expect(section.imageCount).toBe(2);
    });

});
