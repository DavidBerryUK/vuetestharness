import GalleryImageModel                        from '../GalleryImageModel';
import GalleryModel                             from '../GalleryModel';
import GallerySectionModel                      from '../GallerySectionModel';
import { EnumImageId } from '../../constants/GalleryEnums';

describe('GalleryModel Unit Tests', () => {

     test('test constructor', () => {
        const image101 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'TheStarryNight.jpg', 1000, 1200);
        const image102 = new GalleryImageModel(EnumImageId.ArtClaudeMonetWaterLilies, 'Almond Blossoms', 'AlmondBlossoms.jpg', 1000, 1200);

        const image201 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghSelfPortraitBlueWavy, 'The Great Wave off Kanagawa', 'TheGreatWaveOffKanagawa.jpg', 1000, 1200);
        const image202 = new GalleryImageModel(EnumImageId.ArtEdvardMunchTheNightWanderer, 'The Kirifuri Waterfall at Mt. Kurokami', 'TheKirifuriWaterfallAtMtKurokami.jpg', 1000, 1200);
        const image203 = new GalleryImageModel(EnumImageId.ArtLeonidAfremovLastDream, 'The Dream of the Fisherman Wife', 'TheDreamFishermanWife.jpg', 1000, 1200);

        const section1 = new GallerySectionModel('Vincent van Gogh', [image101, image102]);
        const section2 = new GallerySectionModel('Hokusai', [image201, image202, image203]);

        const gallery = new GalleryModel('1', 'picture gallery', [section1, section2]);

        expect(gallery).toBeDefined();
        expect(gallery.name).toBe('picture gallery');
        expect(gallery.sections.length).toBe(2);
        expect(gallery.sections[0]).toBe(section1);
        expect(gallery.sections[1]).toBe(section2);
        expect(gallery.sections[0].imageCount).toBe(2);
        expect(gallery.sections[1].imageCount).toBe(3);
    });

     test('test allImages', () => {
        const image101 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'TheStarryNight.jpg', 1000, 1200);
        const image102 = new GalleryImageModel(EnumImageId.ArtClaudeMonetWaterLilies, 'Almond Blossoms', 'AlmondBlossoms.jpg', 1000, 1200);

        const image201 = new GalleryImageModel(EnumImageId.ArtVincentVanGoghSelfPortraitBlueWavy, 'The Great Wave off Kanagawa', 'TheGreatWaveOffKanagawa.jpg', 1000, 1200);
        const image202 = new GalleryImageModel(EnumImageId.ArtEdvardMunchTheNightWanderer, 'The Kirifuri Waterfall at Mt. Kurokami', 'TheKirifuriWaterfallAtMtKurokami.jpg', 1000, 1200);
        const image203 = new GalleryImageModel(EnumImageId.ArtLeonidAfremovLastDream, 'The Dream of the Fisherman Wife', 'TheDreamFishermanWife.jpg', 1000, 1200);

        const section1 = new GallerySectionModel('Vincent van Gogh', [image101, image102]);
        const section2 = new GallerySectionModel('Hokusai', [image201, image202, image203]);

        const gallery = new GalleryModel('pg', 'picture gallery', [section1, section2]);
        const allImages = gallery.allImages;

        expect(gallery.name).toBe('picture gallery');
        expect(gallery.sections[0].imageCount).toBe(2);
        expect(gallery.sections[1].imageCount).toBe(3);

        expect(allImages).toBeDefined();
        expect(allImages.length).toBe(5);
        expect(allImages[0]).toBe(image101);
        expect(allImages[1]).toBe(image102);
        expect(allImages[2]).toBe(image201);
        expect(allImages[3]).toBe(image202);
        expect(allImages[4]).toBe(image203);
    });



});
