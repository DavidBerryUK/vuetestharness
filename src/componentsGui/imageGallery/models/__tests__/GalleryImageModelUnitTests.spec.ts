import GalleryImageModel                        from '../GalleryImageModel';
import { EnumImageId }                          from '../../constants/GalleryEnums';

describe('GalleryImageModel Unit Tests', () => {

     test('test constructor', () => {
        const image = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'TheStarryNight.jpg', 1000, 1200);

        expect(image).toBeDefined();
        expect(image.id).toBe('Art405');
        expect(image.name).toBe('The Starry Night');
        expect(image.fileName).toBe('TheStarryNight.jpg');
        expect(image.width).toBe(1000);
        expect(image.height).toBe(1200);
    });

     test('Local Filename Evaluation', () => {
        const image = new GalleryImageModel(EnumImageId.ArtVincentVanGoghTheStarryNight, 'The Starry Night', 'TheStarryNight.jpg', 1000, 1200);
        expect(image.localFileName).toBe('\\images\\TheStarryNight.jpg');
    });

});
