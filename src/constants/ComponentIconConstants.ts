export default class ComponentIconConstants {

    public static AboutIcon: string = 'fas fa-info-circle';
    public static AvatarIcon: string = 'fas fa-user';
    public static AvatarListIcon: string = 'fas fa-users';
    public static AxisRulerTimeIcon: string = 'fas fa-ruler-horizontal';
    public static BadgeIcon: string = 'fas fa-circle';
    public static ColourPreviewLensIcon: string = 'fas fa-microscope';
    public static ColourSelectorIcon: string = 'fas fa-palette';
    public static ColourSlidersIcon: string = 'fas fa-sliders-h';

    public static DataListKeyIcon: string = 'far fa-list-alt';
    public static DatePickerIcon: string = 'far fa-calendar';
    public static DemoIcon: string = 'fas fa-tablet-alt';
    public static EventLimiterIcon: string = 'fas fa-poll';
    public static GalleryMultipleIcon: string = 'fas fa-images';
    public static GallerySingleIcon: string = 'fas fa-image';
    public static HomeIcon: string = 'fas fa-home';
    public static ImagePickerIcon: string = 'fas fa-camera-retro';
    public static LoaderIcon: string = 'fas fa-spinner';
    public static MessageListIcon: string = 'far fa-comment-alt';
    public static NumberSpinnerIcon: string = 'fas fa-sort-numeric-up';
    public static ResizeDirectiveIcon = 'fas fa-expand-arrows-alt';
    public static SampleIcon: string = 'fab fa-creative-commons-sampling';
    public static ScheduleGridIcon: string = 'fas fa-th';
    public static ScheduleProgramGridIcon: string = 'fas fa-list-ul';
    public static SimpleChartIcon: string = 'far fa-chart-bar';
    public static timeLineSummaryIcon: string = 'fas fa-calendar-week';
}
