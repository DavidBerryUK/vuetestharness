import Point from './Point';
import Size                                     from './Size';

export default class Rect {
    public origin: Point;
    public size: Size;

    public get left(): number {
        return this.origin.x;
    }

    public get right(): number {
        return this.origin.x + this.size.width;
    }

    public get top(): number {
        return this.origin.y;
    }

    public get bottom(): number {
        return this.origin.y + this.size.height;
    }

    constructor(origin: Point , size: Size ) {
        this.origin = origin;
        this.size = size;
    }
}
