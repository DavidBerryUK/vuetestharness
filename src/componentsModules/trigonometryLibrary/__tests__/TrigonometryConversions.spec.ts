import Trigonometry                             from '../trigonometry';

describe('Trigonometry Conversions', () => {

    test('convert degrees to radians 0', () => {
        const radians = Trigonometry.degreesToRadians(0);
        expect(radians).toBe(0);
    });

    test('convert degrees to radians 5', () => {
        const radians = Trigonometry.degreesToRadians(5);
        expect(radians).toBeCloseTo(0.0872665);
    });

    test('convert degrees to radians 10', () => {
        const radians = Trigonometry.degreesToRadians(5);
        expect(radians).toBeCloseTo(0.0872665);
    });

    test('convert degrees to radians 45', () => {
        const radians = Trigonometry.degreesToRadians(45);
        expect(radians).toBeCloseTo(0.785398);
    });

    test('convert degrees to radians 90', () => {
        const radians = Trigonometry.degreesToRadians(90);
        expect(radians).toBeCloseTo(1.5708);
    });

    test('convert radians to degrees 0', () => {
        const radians = Trigonometry.radiansToDegrees(0);
        expect(radians).toBeCloseTo(0);
    });

    test('convert radians to degrees 0.1', () => {
        const radians = Trigonometry.radiansToDegrees(0.1);
        expect(radians).toBeCloseTo(5.72958);
    });

    test('convert radians to degrees 0.5', () => {
        const radians = Trigonometry.radiansToDegrees(0.5);
        expect(radians).toBeCloseTo(28.6479);
    });

    test('convert radians to degrees 1', () => {
        const radians = Trigonometry.radiansToDegrees(1);
        expect(radians).toBeCloseTo(57.2958);
    });

    test('convert radians to degrees 1.5', () => {
        const radians = Trigonometry.radiansToDegrees(1.5);
        expect(radians).toBeCloseTo(85.9437);
    });
});
