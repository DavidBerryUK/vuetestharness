export default class Trigonometry {

    public static degreesToRadians(degrees: number): number {
        return degrees * Math.PI / 180;
    }

    public static radiansToDegrees(radians: number): number {
        return radians * 180 / Math.PI;
    }

}
