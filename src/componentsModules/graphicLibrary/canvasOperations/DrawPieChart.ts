import ChartDataPoint                           from '@/componentsGui/simpleChartComponent/models/ChartDataPoint';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';
import ChartServices                            from './helper/ChartServices';
import HtmlCanvas                               from '../htmlCanvas/HtmlCanvas';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';


/**
 * Draw a pie chart within the specified rectangle
 *
 * @export
 * @class DrawPieChart
 */
export default class DrawPieChart  {

    public static draw(htmlCanvas: HtmlCanvas, rect: Rect, chartModel: ChartModel) {

        if (htmlCanvas.context == null) {
            return;
        }

        const svc = new ChartServices();
        svc.calculatePieChartStats(chartModel);
        this.render(htmlCanvas, rect, chartModel);
    }

    private static render(htmlCanvas: HtmlCanvas, rect: Rect, chartModel: ChartModel) {

        if (htmlCanvas.context === undefined ||
            htmlCanvas.context === null ||
            chartModel.dataSets.length < 1 ||
            chartModel.dataSets[0].dataPoints.length < 1) {
            return;
        }

        const canvasContext: CanvasRenderingContext2D = htmlCanvas.context;
        const originX = rect.size.width / 2 + rect.left;
        const originY = rect.size.height / 2 + rect.top;
        const radius = Math.floor(Math.min(rect.size.width, rect.size.height) / 2) - 5;

        const dataSet = chartModel.dataSets[0];
        let segmentStart = 0;

        canvasContext.lineCap = 'round';
        canvasContext.lineJoin = 'round';
        canvasContext.lineWidth = 2 * htmlCanvas.pixelRatio;

        dataSet.dataPoints.forEach((dataPoint: ChartDataPoint) => {
            if ( dataSet.sumX !== undefined) {
                const segmentSize = dataPoint.x / dataSet.sumX ;

                const angleStart = segmentStart * 360 ;
                const angleEnd = ( segmentStart + segmentSize) * 360;

                canvasContext.beginPath();
                canvasContext.moveTo(originX, originY);
                canvasContext.arc(originX, originY, radius, angleStart * Math.PI / 180, angleEnd * Math.PI / 180);
                canvasContext.lineTo(originX, originY);
                if (dataPoint.colour !== undefined) {
                    canvasContext.fillStyle = dataPoint.colour.hex;
                }
                canvasContext.fill();
                canvasContext.stroke();

                segmentStart = segmentStart + segmentSize;
            }

        });

    }

}
