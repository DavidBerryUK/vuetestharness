import ChartModel                               from '../../../../componentsGui/simpleChartComponent/models/ChartModel';
import ChartDataSet                             from '../../../../componentsGui/simpleChartComponent/models/ChartDataSet';

export default class ChartServices {

    public calculateLineGraphStats(model: ChartModel) {
        this.calculateMinMaxAxisValues(model);
        this.calculateValueRanges(model);
        this.calculateOffset(model);
    }

    public calculatePieChartStats(model: ChartModel) {
        this.calculateDataSetSums(model);
    }

    public calculateDataSetSums(model: ChartModel) {
        model.dataSets.forEach( (dataset: ChartDataSet) => {
            dataset.sumX = dataset.dataPoints.reduce((sum, chartPoint) => sum + chartPoint.x, 0);
            dataset.sumY = dataset.dataPoints.reduce((sum, chartPoint) => sum + chartPoint.y, 0);
        });
    }

    /**
     * calculate scaling required when plotting to UI Canvas
     *
     * @param {ChartModel} model
     * @param {number} uiWidth
     * @param {number} uiHeight
     * @returns
     * @memberof ChartServices
     */
    public calculateScaleFactor(model: ChartModel, uiWidth: number, uiHeight: number) {

        model.statistics.scaleFactorX = 1;
        model.statistics.scaleFactorY = 1;

        // if things aren't initialised then exit with default values
        //
        if ( model.statistics.rangeX === null ||
            model.statistics.rangeY === null ||
             model.statistics.minX === null ||
             model.statistics.maxX === null ||
             model.statistics.minY === null ||
             model.statistics.maxY === null ) {
            return;
        }

        model.statistics.scaleFactorX = model.statistics.rangeX === 0 ? 1 : uiWidth / model.statistics.rangeX;
        model.statistics.scaleFactorY = model.statistics.rangeY === 0 ? 1 : uiHeight / model.statistics.rangeY;
    }

    /**
     * Calculate min and max values for all points in graph model
     *
     * @param {ChartModel} model
     * @memberof ChartServices
     */
    public calculateMinMaxAxisValues(model: ChartModel) {


        model.dataSets.forEach( (dataset: ChartDataSet) => {
            if (dataset.dataPoints.length >= 1) {

                const maxX = dataset.dataPoints.reduce((max, p) => p.x > max ? p.x : max, dataset.dataPoints[0].x);
                const maxY = dataset.dataPoints.reduce((max, p) => p.y > max ? p.y : max, dataset.dataPoints[0].y);
                const minX = dataset.dataPoints.reduce((min, p) => p.x < min ? p.x : min, dataset.dataPoints[0].x);
                const minY = dataset.dataPoints.reduce((min, p) => p.y < min ? p.y : min, dataset.dataPoints[0].y);

                model.statistics.maxX = model.statistics.maxX === null ? maxX : Math.max(model.statistics.maxX, maxX);
                model.statistics.maxY = model.statistics.maxY === null ? maxY : Math.max(model.statistics.maxY, maxY);
                model.statistics.minX = model.statistics.minX === null ? minX : Math.min(model.statistics.minX, minX);
                model.statistics.minY = model.statistics.minY === null ? minY : Math.min(model.statistics.minY, minY);
            }
        });
    }


    /**
     * calculate the range of numbers along each axis
     *
     * @param {ChartModel} model
     * @memberof ChartServices
     */
    public calculateValueRanges(model: ChartModel) {

        if ( model.statistics.minX !== null && model.statistics.maxX !== null ) {
            model.statistics.rangeX = model.statistics.maxX - model.statistics.minX;
        } else {
            model.statistics.rangeX = 0;
        }

        if ( model.statistics.minY !== null && model.statistics.maxY !== null) {
            model.statistics.rangeY = model.statistics.maxY - model.statistics.minY;
        } else {
            model.statistics.rangeY = 0;
        }
    }

    public calculateOffset(model: ChartModel) {
        model.statistics.offsetX = model.statistics.minX === null ? 0 : model.statistics.minX * -1;
        model.statistics.offsetY = model.statistics.minY === null ? 0 : model.statistics.minY * -1;
    }

}
