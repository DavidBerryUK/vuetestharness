import HtmlCanvas                               from '../htmlCanvas/HtmlCanvas';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';

/**
 * Draw a test placeholder, used for when testing locations on a canvas
 *
 * @export
 * @class DrawPlaceholder
 */
export default class DrawPlaceholder  {

    public static draw(htmlCanvas: HtmlCanvas, rect: Rect, title?: string) {

        if (htmlCanvas.context == null) {
            return;
        }

        if ( title !== undefined) {
            htmlCanvas.drawText(title, rect.left + 10, rect.top + 10);
        }
        this.render(htmlCanvas, rect);
    }

    private static render(htmlCanvas: HtmlCanvas, rect: Rect) {

        if (htmlCanvas.context === undefined || htmlCanvas.context === null ) {
            return;
        }

        const canvasContext: CanvasRenderingContext2D = htmlCanvas.context;

        canvasContext.lineCap = 'round';
        canvasContext.lineJoin = 'round';
        canvasContext.lineWidth = 2 * htmlCanvas.pixelRatio;

        let radius = 12 * htmlCanvas.pixelRatio;

        if (rect.size.width < 2 * radius) { radius = rect.size.width / 2; }
        if (rect.size.height < 2 * radius) { radius = rect.size.height / 2; }

        canvasContext.beginPath();
        canvasContext.moveTo(rect.left + radius, rect.top);
        canvasContext.arcTo(rect.right, rect.top,   rect.right, rect.bottom, radius);
        canvasContext.arcTo(rect.right, rect.bottom, rect.left,   rect.bottom, radius);
        canvasContext.arcTo(rect.left,   rect.bottom, rect.left,   rect.top,   radius);
        canvasContext.arcTo(rect.left,   rect.top,   rect.right, rect.top,   radius);
        canvasContext.closePath();

        canvasContext.lineWidth = 4 * htmlCanvas.pixelRatio;
        canvasContext.strokeStyle = '#FF0000';
        canvasContext.stroke();

    }

}
