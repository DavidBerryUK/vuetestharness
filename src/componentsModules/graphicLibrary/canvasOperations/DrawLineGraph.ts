import ChartDataSet                             from '@/componentsGui/simpleChartComponent/models/ChartDataSet';
import ChartModel                               from '@/componentsGui/simpleChartComponent/models/ChartModel';
import ChartServices                            from '@/componentsModules/graphicLibrary/canvasOperations/helper/ChartServices';
import HtmlCanvas                               from '../htmlCanvas/HtmlCanvas';
import Rect                                     from '@/componentsModules/trigonometryLibrary/models/Rect';


/**
 * Draw a line graph on a canvas within the specified rectangle
 *
 * @export
 * @class DrawLineGraph
 */
export default class DrawLineGraph {

    public static draw(htmlCanvas: HtmlCanvas, rect: Rect, chartModel: ChartModel ): void {

        if (htmlCanvas.context == null) {
            return;
        }

        const svc = new ChartServices();
        svc.calculateLineGraphStats(chartModel);
        svc.calculateScaleFactor(chartModel, rect.size.width, rect.size.height);
        this.render(htmlCanvas, rect, chartModel);
    }

    private static render(htmlCanvas: HtmlCanvas, rect: Rect, chartModel: ChartModel) {

        if (    htmlCanvas.context === undefined ||
                htmlCanvas.context === null ||
                chartModel.statistics.scaleFactorX === null ||
                chartModel.statistics.scaleFactorY === null ||
                chartModel.statistics.maxY === null ||
                chartModel.statistics.rangeY === null) {
            return;
        }

        const canvasContext: CanvasRenderingContext2D = htmlCanvas.context;
        const scaleX = chartModel.statistics.scaleFactorX;
        const scaleY = chartModel.statistics.scaleFactorY;
        const offsetX = chartModel.statistics.offsetX === null ? 0 : chartModel.statistics.offsetX;
        const offsetY = chartModel.statistics.offsetY === null ? 0 : chartModel.statistics.offsetY;

        canvasContext.lineCap = 'round';
        canvasContext.lineJoin = 'round';

        const h = rect.size.height; // rect.size.height;


        chartModel.dataSets.forEach((dataSet: ChartDataSet) => {
            if ( dataSet.dataPoints.length > 0) {

                canvasContext.beginPath();

                for ( let i = 0; i < dataSet.dataPoints.length; i++ ) {
                    const x = ( offsetX + dataSet.dataPoints[i].x ) * scaleX + rect.origin.x;
                    const y =   h - ((offsetY +  dataSet.dataPoints[i].y)  * scaleY) + rect.origin.y;

                    if ( i === 0) {
                        canvasContext.moveTo( x, y );
                    } else {
                        canvasContext.lineTo( x, y );
                    }
                }

                canvasContext.lineWidth = dataSet.lineWidth * htmlCanvas.pixelRatio;
                canvasContext.strokeStyle = dataSet.colour.hex;

                canvasContext.stroke();
            }
        });
    }
}
