export default class HtmlCanvas {

    public canvasElement: HTMLCanvasElement | undefined;
    public context: CanvasRenderingContext2D | null | undefined;

    public pixelRatio: number = HtmlCanvas.calculatePixelRatio();

    public get  width(): number {
        if ( this.canvasElement === undefined) {
            return 0;
        }
        return this.canvasElement.width;
    }

    public get height(): number {
        if ( this.canvasElement === undefined) {
            return 0;
        }
        return this.canvasElement.height;
    }

    constructor(canvasElement: HTMLCanvasElement) {

        if ( canvasElement === undefined) {
            return;
        }
        this.canvasElement = canvasElement;
        this.context = this.canvasElement.getContext('2d') ;
        this.setupCanvas();
    }

    public setupCanvas() {
        if ( this.canvasElement === undefined) {
            return;
        }

        if (this.canvasElement.parentElement != null) {
            const width = this.canvasElement.parentElement.clientWidth;
            const height = this.canvasElement.parentElement.clientHeight;
            this.canvasElement.width = width * this.pixelRatio;
            this.canvasElement.height = height * this.pixelRatio;
            this.canvasElement.style.width = width + 'px';
            this.canvasElement.style.height = height + 'px';
        }
    }

    public clearCanvas(colour?: string) {
        if ( this.context !== undefined && this.context !== null) {
            if ( colour === undefined) {
                this.context.clearRect(0, 0, this.width, this.height);
            } else {
                this.context.fillStyle = colour;
                this.context.fillRect(0, 0, this.width, this.height);
            }
        }
    }

    public drawText(text: string, x: number, y: number) {
        if (this.context == null) {
            return;
        }

        this.context.fillStyle = '#000000';
        this.context.font = `${14 * this.pixelRatio}px Arial`;
        this.context.textAlign = 'left';
        this.context.fillText(text, x, y);
    }

    public static calculatePixelRatio() {
        const ctx = document.createElement('canvas').getContext('2d');
        if ( ctx == null) {
            return 1;
        }
        const dpr = window.devicePixelRatio || 1;
        const bsr = (ctx as any).webkitBackingStorePixelRatio ||
                    (ctx as any).mozBackingStorePixelRatio ||
                    (ctx as any).msBackingStorePixelRatio ||
                    (ctx as any).oBackingStorePixelRatio ||
                    (ctx as any).backingStorePixelRatio || 1;

        return dpr / bsr;
    }
}
