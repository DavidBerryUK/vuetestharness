import './plugins/vuetify';
import './styles/global-styles.styl';
import { directiveResize }                      from './componentsDirectives/onResizeDirective/directiveResize';
import App                                      from './application/App';
import ApplicationRouter                        from './router/ApplicationRouter';
import Vue                                      from 'vue';

Vue.config.productionTip = false;
const router = new ApplicationRouter();


Vue.use(require('vue-moment'));

Vue.directive('on-resize', directiveResize);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
