import moment                                   from 'moment';
import DateCalculations                         from '../DateCalculations';


describe('Months Between Dates', () => {

    test('Single Month', () => {
        const dateFrom = moment('2018-05-01 00:00:00');
        const dateTo = moment('2018-05-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(1);
    });

    test('Two Month', () => {
        const dateFrom = moment('2018-05-01 00:00:00');
        const dateTo = moment('2018-06-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(2);
    });

    test('Two Month', () => {
        const dateFrom = moment('2018-05-01 00:00:00');
        const dateTo = moment('2018-06-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(2);
    });

    test('Span 2 years - no gap', () => {
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2019-01-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(2);
    });

    test('Span 2 years  - gap in from year', () => {
        const dateFrom = moment('2018-07-01 00:00:00');
        const dateTo = moment('2019-01-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(7);
    });

    test('Span 2 years  - gap in to year', () => {
        const dateFrom = moment('2018-12-01 00:00:00');
        const dateTo = moment('2019-06-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(7);
    });

    test('Span 2 years  - gap in both year', () => {
        const dateFrom = moment('2018-07-01 00:00:00');
        const dateTo = moment('2019-06-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(12);
    });

    test('Span 3 years  - gap - simple', () => {
        const dateFrom = moment('2017-12-01 00:00:00');
        const dateTo = moment('2019-01-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(14);
    });

    test('Span 3 years  - gap - all from year', () => {
        const dateFrom = moment('2017-01-01 00:00:00');
        const dateTo = moment('2019-01-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(25);
    });

    test('Span 3 years  - gap - all to year', () => {
        const dateFrom = moment('2017-12-01 00:00:00');
        const dateTo = moment('2019-12-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(25);
    });

    test('Span 3 years  - gap - all months', () => {
        const dateFrom = moment('2017-01-01 00:00:00');
        const dateTo = moment('2019-12-10 00:00:00');
        const monthCount = DateCalculations.monthsBetweenDates(dateFrom, dateTo);
        expect(monthCount).toBe(36);
    });
});
