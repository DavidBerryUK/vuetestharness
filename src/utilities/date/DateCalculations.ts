import moment                                   from 'moment';

export default class DateCalculations {

    /**
     * calculate the number of months in a date range, this includes a count
     * of ANY months in the date range, even it its only a few days
     * @static
     * @param {moment.Moment} dateFrom
     * @param {moment.Moment} dateTo
     * @returns {number}
     * @memberof DateCalculations
     */
    public static monthsBetweenDates(dateFrom: moment.Moment, dateTo: moment.Moment): number {

        let dateMin: moment.Moment;
        let dateMax: moment.Moment;

        if ( dateTo.isSameOrAfter(dateFrom)) {
            dateMin = dateFrom;
            dateMax = dateTo;
        } else {
            dateMin = dateTo;
            dateMax = dateFrom;
        }


        const fromYear = dateMin.year();
        const fromMonth = dateMin.month();
        const toYear = dateMax.year();
        const toMonth = dateMax.month();
        let monthCount: number;

        if ( fromYear === toYear) {
            monthCount = toMonth - fromMonth + 1;
        } else {
            monthCount = (((toYear - fromYear) - 1)  * 12) +
                            (13 - fromMonth) +
                            toMonth;
        }

        return monthCount;

    }
}
