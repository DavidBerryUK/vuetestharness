export default class ConvertStringToBoolean {

    public static convert(
        value: string | boolean | null | undefined,
        defaultValue: boolean = false ): boolean {

        if (value === '') {
            return true;
        }

        if ( value === undefined || value === null || value === '' ) {
            return defaultValue;
        }

        if ( typeof value === 'boolean') {
            return value;
        }

        if ( typeof value === 'string') {
            const lower = value.toLowerCase();
            if ( lower === 'true' || lower === 't' || lower === '1' || lower === 'y' || lower === 'yes') {
                return true;
            }
        }

        return false;
    }
}
