import { IAggregateFunction } from './IAggregateFunction';

export default class AggregatorEngine {

    public static aggregate<T, S>(
        list: Array<T>,
        aggregateFunction: IAggregateFunction<T, S>): Array<S> {

        const aggregate = new Array<S>();
        const keys: { [index: string]: {listItem: S} } = {};

        list.forEach((index: T) => {
            const key = aggregateFunction.createAggregationKey(index);
            if (keys[key] === undefined) {
                const summary = aggregateFunction.createSummaryObject(index);
                keys[key] = {  listItem: summary };
                aggregate.push(keys[key].listItem);
            }
            aggregateFunction.addObjectToSummary(index, keys[key].listItem);
        });

        return aggregate;
    }
}
