export interface IAggregateFunction<T, S> {
    createSummaryObject(object: T): S;
    addObjectToSummary(object: T, summary: S): S;
    createAggregationKey(object: T): string;
}
