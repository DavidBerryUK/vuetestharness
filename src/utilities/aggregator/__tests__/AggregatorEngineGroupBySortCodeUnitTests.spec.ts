import { IAggregateFunction }                   from '../IAggregateFunction';
import AggregatorEngine                         from '../AggregateEngine';


class Account {

    public type: string;
    public sortCode: string;
    public accountCode: string;
    public balance: number;

    constructor(type: string, sortCode: string, accountCode: string, balance: number) {
        this.type = type;
        this.sortCode = sortCode;
        this.accountCode = accountCode;
        this.balance = balance;
    }
}

// tslint:disable-next-line:max-classes-per-file
class AccountAggregateSortCode {

    public sortCode: string;
    public count: number;
    public balance: number;

    constructor() {
        this.sortCode = '';
        this.count = 0;
        this.balance = 0;
    }
}

// tslint:disable-next-line:max-classes-per-file
class AccountAggregateSortCodeFunction implements IAggregateFunction<Account, AccountAggregateSortCode> {

    public createAggregationKey(object: Account): string {
        return object.sortCode;
    }

    public createSummaryObject(object: Account): AccountAggregateSortCode {
        const instance = new AccountAggregateSortCode();
        instance.sortCode = object.sortCode;
        return instance;
    }

    public addObjectToSummary(object: Account, summary: AccountAggregateSortCode): AccountAggregateSortCode {
        summary.balance = summary.balance + object.balance;
        summary.count = summary.count + 1;
        return summary;
    }
}

describe('Aggregate Objects to summary', () => {

    test('Simple List - tally by sort code', () => {

        const list: Array<Account> = [
            new Account('savings', '99-88-77', '00001000', 100),
            new Account('savings', '11-11-11', '00002000', 150),
            new Account('current', '22-22-22', '00003000', 500),
            new Account('current', '99-88-77', '00003000', 450),
            new Account('current', '33-33-33', '00004000', 150),
            new Account('current', '33-33-33', '00004500', 100),
        ];

        const grouped = AggregatorEngine.aggregate(list, new AccountAggregateSortCodeFunction());

        expect(grouped).toBeDefined();
        expect(grouped.length).toBe(4);

        expect(grouped[0].sortCode).toBe('99-88-77');
        expect(grouped[0].balance).toBe(550);
        expect(grouped[0].count).toBe(2);

        expect(grouped[1].sortCode).toBe('11-11-11');
        expect(grouped[1].balance).toBe(150);
        expect(grouped[1].count).toBe(1);

        expect(grouped[2].sortCode).toBe('22-22-22');
        expect(grouped[2].balance).toBe(500);
        expect(grouped[2].count).toBe(1);

        expect(grouped[3].sortCode).toBe('33-33-33');
        expect(grouped[3].balance).toBe(250);
        expect(grouped[3].count).toBe(2);
    });


});
