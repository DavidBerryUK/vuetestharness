import { IAggregateFunction }                   from '../IAggregateFunction';
import AggregatorEngine                     from '../AggregateEngine';

class Account {

    public type: string;
    public sortCode: string;
    public accountCode: string;
    public balance: number;

    constructor(type: string, sortCode: string, accountCode: string, balance: number) {
        this.type = type;
        this.sortCode = sortCode;
        this.accountCode = accountCode;
        this.balance = balance;
    }
}

// tslint:disable-next-line:max-classes-per-file
class AccountAggregateByType {

    public type: string;
    public count: number;
    public balance: number;

    constructor() {
        this.type = '';
        this.count = 0;
        this.balance = 0;
    }
}

// tslint:disable-next-line:max-classes-per-file
class AccountAggregateByTypeFunction implements IAggregateFunction<Account, AccountAggregateByType> {

    public createAggregationKey(object: Account): string {
        return object.type;
    }

    public createSummaryObject(object: Account): AccountAggregateByType {
        const instance = new AccountAggregateByType();
        instance.type = object.type;
        return instance;
    }

    public addObjectToSummary(object: Account, summary: AccountAggregateByType): AccountAggregateByType {
        summary.balance = summary.balance + object.balance;
        summary.count = summary.count + 1;
        return summary;
    }
}



describe('Aggregate Objects to summary', () => {

    test('Simple List - tally by account type', () => {

        const list: Array<Account> = [
            new Account('savings', '64-11-01', '00001000', 100),
            new Account('savings', '64-13-41', '00002000', 150),
            new Account('current', '65-11-91', '00003000', 500),
            new Account('current', '71-18-22', '00003000', 450),
            new Account('current', '65-19-02', '00004000', 150),
        ];

        const grouped = AggregatorEngine.aggregate(list, new AccountAggregateByTypeFunction());

        expect(grouped).toBeDefined();
        expect(grouped.length).toBe(2);
        expect(grouped[0].type).toBe('savings');
        expect(grouped[0].balance).toBe(250);
        expect(grouped[0].count).toBe(2);

        expect(grouped[1].type).toBe('current');
        expect(grouped[1].balance).toBe(1100);
        expect(grouped[1].count).toBe(3);

    });

});
