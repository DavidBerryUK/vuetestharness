import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';

export default class GalleryUtilities {

    public static GetAllImagesFromGalleries(galleries: Array<GalleryModel>): Array<GalleryImageModel> {

        let images = new Array<GalleryImageModel>();

        galleries.forEach((gallery: GalleryModel) => {
            images = images.concat(gallery.allImages);
        });

        return images;
    }
}
