import { FieldImageSelectorGallery }            from '../constants/DecoratorConstants';
import { KeyFieldRangeMin }                     from '../constants/DecoratorConstants';
import { KeyFieldRangeMax }                     from '../constants/DecoratorConstants';
import { KeyFieldBindUrlToField }               from '../constants/DecoratorConstants';
import { FieldObjectOptionsDisplayField }       from '../constants/DecoratorConstants';
import { KeyFieldDefaultValue }                 from '../constants/DecoratorConstants';
import { KeyFieldDescription }                  from '../constants/DecoratorConstants';
import { KeyFieldEventValue }                   from '../constants/DecoratorConstants';
import { KeyFieldList }                         from '../constants/DecoratorConstants';
import { KeyFieldNameOverride }                 from '../constants/DecoratorConstants';
import { KeyFieldSection }                      from '../constants/DecoratorConstants';
import { KeyFieldType }                         from '../constants/DecoratorConstants';
import { KeyFieldValueOptions }                 from '../constants/DecoratorConstants';
import TestHarnessFieldPropertyModel            from '../models/TestHarnessFieldPropertyModel';


export default class PropertyHelper {


    /**
     * Extract decorator information from model that describes the
     * test harness form layout
     *
     * @param {object} model
     * @returns {Array<TestHarnessFieldPropertyModel>}
     * @memberof PropertyHelper
     */
    public static getProperties(model: object): Array<TestHarnessFieldPropertyModel> {
        const propertyArray = new Array<TestHarnessFieldPropertyModel>();

        if (model) {

            const fieldList: Array<string> = Reflect.getMetadata(KeyFieldList, model);

            if (fieldList) {

                fieldList.forEach((fieldName) => {


                    const fieldProperties = new TestHarnessFieldPropertyModel(fieldName);

                    if (Reflect.hasMetadata(KeyFieldNameOverride, model, fieldName)) {
                        fieldProperties.name =
                            Reflect.getMetadata(KeyFieldNameOverride, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldSection, model, fieldName)) {
                        fieldProperties.documentSection =
                            Reflect.getMetadata(KeyFieldSection, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldDescription, model, fieldName)) {
                        fieldProperties.description =
                            Reflect.getMetadata(KeyFieldDescription, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldValueOptions, model, fieldName)) {
                        fieldProperties.valueOptions =
                            Reflect.getMetadata(KeyFieldValueOptions, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldType, model, fieldName)) {
                        fieldProperties.variableType =
                            Reflect.getMetadata(KeyFieldType, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldDefaultValue, model, fieldName)) {
                        fieldProperties.defaultValue =
                            Reflect.getMetadata(KeyFieldDefaultValue, model, fieldName);
                    }

                    if (Reflect.hasMetadata(FieldObjectOptionsDisplayField, model, fieldName)) {
                        fieldProperties.optionsDisplayField =
                            Reflect.getMetadata(FieldObjectOptionsDisplayField, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldBindUrlToField, model, fieldName)) {
                        fieldProperties.bindUrlToField =
                            Reflect.getMetadata(KeyFieldBindUrlToField, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldEventValue, model, fieldName)) {
                        fieldProperties.eventValue =
                            Reflect.getMetadata(KeyFieldEventValue, model, fieldName);
                    }

                    if (Reflect.hasMetadata(FieldImageSelectorGallery, model, fieldName)) {
                        fieldProperties.gallerySelection =
                            Reflect.getMetadata(FieldImageSelectorGallery, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldRangeMin, model, fieldName)) {
                        fieldProperties.rangeMin =
                            Reflect.getMetadata(KeyFieldRangeMin, model, fieldName);
                    }

                    if (Reflect.hasMetadata(KeyFieldRangeMax, model, fieldName)) {
                        fieldProperties.rangeMax =
                            Reflect.getMetadata(KeyFieldRangeMax, model, fieldName);
                    }

                    propertyArray.push(fieldProperties);
                });
            }
        }
        return propertyArray;
    }
}
