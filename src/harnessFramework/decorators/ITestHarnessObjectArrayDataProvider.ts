export  interface ITestHarnessObjectArrayDataProvider {
    dataForField(fieldName: string): Array<any>;
}
