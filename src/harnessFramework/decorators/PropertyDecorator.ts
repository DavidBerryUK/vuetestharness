import 'reflect-metadata';
import { EnumDocumentSection }                  from './enums/DecoratorEnums';
import { EnumGallerySelection }                 from './enums/DecoratorEnums';
import { EnumVariableType }                     from './enums/DecoratorEnums';
import { FieldImageSelectorGallery }            from './constants/DecoratorConstants';
import { FieldObjectOptionsDisplayField }       from './constants/DecoratorConstants';
import { KeyFieldBindUrlToField }               from './constants/DecoratorConstants';
import { KeyFieldDefaultValue }                 from './constants/DecoratorConstants';
import { KeyFieldDescription }                  from './constants/DecoratorConstants';
import { KeyFieldEventValue }                   from './constants/DecoratorConstants';
import { KeyFieldList }                         from './constants/DecoratorConstants';
import { KeyFieldNameOverride }                 from './constants/DecoratorConstants';
import { KeyFieldRangeMax }                     from './constants/DecoratorConstants';
import { KeyFieldRangeMin }                     from './constants/DecoratorConstants';
import { KeyFieldSection }                      from './constants/DecoratorConstants';
import { KeyFieldType }                         from './constants/DecoratorConstants';
import { KeyFieldValueOptions }                 from './constants/DecoratorConstants';

export function PropertyRange( min: number, max: number ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldRangeMin, min, target, key);
        Reflect.defineMetadata(KeyFieldRangeMax, max, target, key);
        Reflect.defineMetadata(KeyFieldType, EnumVariableType.number, target, key);
    };
}

export function PropertyFieldName( name: string ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldNameOverride, name, target, key);
    };
}

export function PropertyEventValue( value: string ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldEventValue, value, target, key);
    };
}

export function PropertyEvent( description: string ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldSection, EnumDocumentSection.events, target, key);
        Reflect.defineMetadata(KeyFieldDescription, description, target, key);
    };
}

export function Property(documentSection: EnumDocumentSection, description: string ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldSection, documentSection, target, key);
        Reflect.defineMetadata(KeyFieldDescription, description, target, key);
    };
}

/**
 * the model field must support ITestHarnessObjectArrayDataProvider for this to work
 *
 * @export
 * @param {string} displayField
 * @returns {PropertyDecorator}
 */
export function PropertyObjectOptionsProvider(displayField: string): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(FieldObjectOptionsDisplayField, displayField, target, key);
    };
}

export function PropertyImageSelector(galleryOption: EnumGallerySelection): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(FieldImageSelectorGallery, galleryOption, target, key);
    };
}



export function BindImageSelectorUrlToField(bindImageUrlToField: string): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldNameOverride, bindImageUrlToField, target, key);
        Reflect.defineMetadata(KeyFieldBindUrlToField, bindImageUrlToField, target, key);
    };
}

export function PropertyValueOptions(valueOptions: Array<string>): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldValueOptions, valueOptions, target, key);
    };
}

export function PropertyType(customTypeName: string | EnumVariableType): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldType, customTypeName, target, key);
    };
}

export function PropertyDefaultValue(defaultValue: string | boolean | number ): PropertyDecorator {
    return (target, key) => {
        registerFieldName(target, key);
        Reflect.defineMetadata(KeyFieldDefaultValue, defaultValue, target, key);
    };
}

/**
 * register a field name against the class,
 *  prevent the same name being added twice
 * @param {*} target
 * @param {string} name
 */
function registerFieldName(target: any, name: string | symbol) {


    const nameAsString = name.toString();

    let fieldList: Array<string>;
    if ( Reflect.hasMetadata(KeyFieldList, target)) {
        fieldList = Reflect.getMetadata(KeyFieldList, target);
    } else {
        fieldList = new Array<string>();
    }
    if ( fieldList.filter((item: string) => item === nameAsString).length === 0) {
        fieldList.push(nameAsString);
    }

    Reflect.defineMetadata(KeyFieldList, fieldList, target);
}
