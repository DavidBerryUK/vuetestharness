
/**
 * Specify the control type that is rendered on the test harness view
 *
 * @export
 * @enum {number}
 */
export enum EnumControlType {
    Text = 1,
    Switch = 2,
    Slider = 3,
    DropDown = 4,
    ObjectOptions = 5,
    ImagePicker = 6,
    DatePicker = 7,
    None = 99,
}


/**
 * describe the variable type
 *
 * @export
 * @enum {number}
 */
export enum EnumVariableType {
   unknown = 0,
   string = 1,
   number = 2,
   boolean = 3,
   date = 4,
   custom = 99,
}


/**
 * Determines which section the parameter will appear in on the
 * test harness form that is automatically generated
 * @export
 * @enum {number}
 */
export enum EnumDocumentSection {
    model = 1,
    attributes = 2,
    events = 3,
}


export enum EnumGallerySelection {
    Animals = 1,
    Paintings = 2,
    Business = 3,
    All = 4,
}
