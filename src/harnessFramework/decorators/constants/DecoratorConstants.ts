export const KeyFieldList =                     Symbol('__FieldList__');

export const FieldImageSelectorGallery =        Symbol('FieldImageSelectorGallery');
export const FieldObjectOptionsDisplayField =   Symbol('FieldObjectOptionsDisplayField');
export const KeyFieldBindUrlToField =           Symbol('FieldBindUrlToField');
export const KeyFieldDefaultValue =             Symbol('FieldDefaultValue');
export const KeyFieldDescription =              Symbol('FieldDescription');
export const KeyFieldEventValue =               Symbol('FieldEventValue');
export const KeyFieldNameOverride  =            Symbol('FieldNameOverride');
export const KeyFieldSection =                  Symbol('FieldSection');
export const KeyFieldType =                     Symbol('FieldType');
export const KeyFieldValueOptions =             Symbol('FieldValueOptions');
export const KeyFieldRangeMin =                 Symbol('FieldRangeMin');
export const KeyFieldRangeMax =                 Symbol('FieldRangeMax');

