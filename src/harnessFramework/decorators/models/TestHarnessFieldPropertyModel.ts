import { EnumControlType }                      from '../enums/DecoratorEnums';
import { EnumDocumentSection }                  from '../enums/DecoratorEnums';
import { EnumGallerySelection }                 from '../enums/DecoratorEnums';
import { EnumImageId }                          from '@/componentsGui/imageGallery/constants/GalleryEnums';
import { EnumVariableType }                     from '../enums/DecoratorEnums';


export default class TestHarnessFieldPropertyModel {
  public name: string;
  public nameOriginal: string;
  public description: string;
  public documentSection: EnumDocumentSection;
  public variableType: EnumVariableType | string;
  public defaultValue: string | number | boolean | undefined;
  public optionsDisplayField: string | undefined;
  public bindUrlToField: string | undefined;
  public valueOptions: Array<string> | undefined;
  public eventValue: string | undefined;
  public gallerySelection: EnumGallerySelection | undefined;

  public rangeMin: number | undefined;
  public rangeMax: number | undefined;

  constructor(
    name: string) {
    this.name = name;
    this.nameOriginal = name;
    this.description = '';
    this.documentSection = EnumDocumentSection.attributes;
    this.variableType = EnumVariableType.unknown;
  }

  public get typeAsString(): string {
    if (typeof this.variableType === 'string') {
      return this.variableType;
    }
    return this.variableTypeToString;
  }

  private get variableTypeToString(): string {
    const index = this.variableType as EnumVariableType;
    if (index) {
      return EnumVariableType[index];
    }
    return '';
  }

  public get controlType(): EnumControlType {

    if (this.valueOptions) {
      return EnumControlType.DropDown;
    }

    if ( this.optionsDisplayField) {
      return EnumControlType.ObjectOptions;
    }

    if ( this.gallerySelection) {
      return EnumControlType.ImagePicker;
    }

    const index = this.variableType as EnumVariableType;
    if (index) {
      switch (this.variableType) {
        case EnumVariableType.boolean:
          return EnumControlType.Switch;

        case EnumVariableType.string:
          return EnumControlType.Text;

        case EnumVariableType.number:
          return EnumControlType.Slider;

        case EnumVariableType.date:
          return EnumControlType.DatePicker;

        default:
          return EnumControlType.None;
      }
    }

    return EnumControlType.None;
  }

  public get dataInputComponentType(): string {

    switch (this.controlType) {
      case EnumControlType.Slider:
        return 'data-input-range';

      case EnumControlType.Switch:
        return 'data-input-switch';

      case EnumControlType.Text:
        return 'data-input-string';

      case EnumControlType.DropDown:
        return 'data-input-drop-down';

      case EnumControlType.ObjectOptions:
        return 'data-input-object-options';

      case EnumControlType.ImagePicker:
        return 'data-input-image-selector';

      case EnumControlType.DatePicker:
        return 'data-input-data-selector';

      case EnumControlType.None:
        return 'data-input-none';
    }

    return 'data-input-none';
  }
}
