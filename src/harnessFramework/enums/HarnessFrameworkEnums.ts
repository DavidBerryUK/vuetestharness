
/**
 * Supplied to the harness framework to indicate which page should
 * be displayed
 * @export
 * @enum {number}
 */
export enum EnumTab {
    Parameters = 0,
    DemoControls = 1,
    Events = 2,
    EventLog = 3,
}
