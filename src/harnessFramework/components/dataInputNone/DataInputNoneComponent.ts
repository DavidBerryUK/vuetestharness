import { Prop }                         from 'vue-property-decorator';
import Component                        from 'vue-class-component';
import Vue                              from 'vue';
import TestHarnessFieldPropertyModel    from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';

@Component
export default class DataInputNoneComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public meta!: TestHarnessFieldPropertyModel;

    private data(): any {
        return {};
    }

}

Vue.component('data-input-none', DataInputNoneComponent);
