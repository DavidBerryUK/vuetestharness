import { Prop }                         from 'vue-property-decorator';
import Component                        from 'vue-class-component';
import DataInputDatePickerComponent     from '../dataInputDatePicker/DataInputDatePickerComponent';
import DataInputDropDownComponent       from '../dataInputDropDown/DataInputDropDownComponent';
import DataInputImageSelectorComponent  from '../dataInputImageSelector/DataInputImageSelectorComponent';
import DataInputNoneComponent           from '../dataInputNone/DataInputNoneComponent';
import DataInputObjectOptionsComponent  from '../dataInputObjectOptions/DataInputObjectOptionsComponent';
import DataInputRangeComponent          from '../dataInputRange/DataInputRangeComponent';
import DataInputStringComponent         from '../dataInputString/DataInputStringComponent';
import DataInputSwitchComponent         from '../dataInputSwitch/DataInputSwitchComponent';
import TestHarnessFieldPropertyModel    from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import Vue                              from 'vue';

@Component({
    components: {
        DataInputDatePickerComponent,
        DataInputDropDownComponent,
        DataInputImageSelectorComponent,
        DataInputNoneComponent,
        DataInputObjectOptionsComponent,
        DataInputRangeComponent,
        DataInputStringComponent,
        DataInputSwitchComponent,
    },
  })
export default class ParametersListComponent extends Vue {

    @Prop()
    public title!: string;

    @Prop()
    public parameters!: Array<TestHarnessFieldPropertyModel>;

    @Prop()
    public value!: any;

    get showList(): boolean {
        return this.parameters.length > 0;
    }

    private data(): any {
        return {};
    }

}

Vue.component('db-parameters-list', ParametersListComponent);
