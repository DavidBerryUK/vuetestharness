import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import DatePickerComponent                      from '@/componentsGui/datePickerComponent/DatePickerComponent';
import TestHarnessFieldPropertyModel            from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import Vue                                      from 'vue';

@Component({
    components: {
        DatePickerComponent,
    },
  })
export default class DataInputDatePickerComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public meta!: TestHarnessFieldPropertyModel;

    private data(): any {
        return {};
    }

}

Vue.component('data-input-data-selector', DataInputDatePickerComponent);
