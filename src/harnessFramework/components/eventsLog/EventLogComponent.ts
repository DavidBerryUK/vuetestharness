import { Prop }                         from 'vue-property-decorator';
import Component                        from 'vue-class-component';
import EventLogCollectionModel          from '@/harnessFramework/models/EventLogCollectionModel';
import Vue                              from 'vue';

@Component({
    components: {
    },
  })
export default class EventLogComponent extends Vue {

    @Prop()
    public eventLog!: EventLogCollectionModel;

    get showEventList(): boolean {
        if (this.eventLog) {
            if ( this.eventLog.events) {
                return this.eventLog.events.length > 0;
            }
        }
        return false;
    }

    private data(): any {
        return {};
    }

}

Vue.component('db-event-log', EventLogComponent);
