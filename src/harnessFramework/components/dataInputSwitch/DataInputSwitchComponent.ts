import { Prop }                         from 'vue-property-decorator';
import Component                        from 'vue-class-component';
import TestHarnessFieldPropertyModel    from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import Vue                              from 'vue';

@Component
export default class DataInputSwitchComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public meta!: TestHarnessFieldPropertyModel;

    private data(): any {
        return {};
    }

}

Vue.component('data-input-switch', DataInputSwitchComponent);
