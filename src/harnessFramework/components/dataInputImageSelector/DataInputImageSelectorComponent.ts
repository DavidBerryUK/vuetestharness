import { EnumGallerySelection }                 from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import GalleryFactory                           from '@/componentsGui/imageGallery/factory/GalleryFactory';
import GalleryImageModel                        from '@/componentsGui/imageGallery/models/GalleryImageModel';
import GalleryModel                             from '@/componentsGui/imageGallery/models/GalleryModel';
import ImagePickerComponent                     from '@/componentsGui/imagePickerComponent/ImagePickerComponent';
import TestHarnessFieldPropertyModel            from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import Vue                                      from 'vue';

@Component({
    components: {
        ImagePickerComponent,
    },
  })
export default class DataInputImageSelectorComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public meta!: TestHarnessFieldPropertyModel;

    public galleries: Array<GalleryModel> = this.selectedGallery;

    public image: GalleryImageModel = GalleryImageModel.None;

    public onSelectedNewImage(selectedImage: GalleryImageModel) {
        this.image = selectedImage;
        if (this.meta.bindUrlToField) {
            this.value[this.meta.bindUrlToField] = this.image.localFileName;
        }
    }

    public mounted() {
        if ( this.meta.gallerySelection ) {

            switch (this.meta.gallerySelection) {
                case EnumGallerySelection.Animals:
                    this.galleries =  [ GalleryFactory.getAnimalGallery() ];
                    break;

                case EnumGallerySelection.Business:
                    this.galleries =  [ GalleryFactory.getBusinessGallery() ];
                    break;

                case EnumGallerySelection.Paintings:
                    this.galleries =  [ GalleryFactory.getArtGallery() ];
                    break;

                default:
                    this.galleries =  [ GalleryFactory.getAnimalGallery(),
                                    GalleryFactory.getBusinessGallery(),
                                    GalleryFactory.getArtGallery() ];
                    break;
            }
        }
        if ( this.value[this.meta.nameOriginal] ) {
            this.image = this.value[this.meta.nameOriginal];
        }
    }

    private get selectedGallery(): Array<GalleryModel> {
        if ( this.meta.gallerySelection ) {
            switch (this.meta.gallerySelection) {
                case EnumGallerySelection.Animals:
                    return  [ GalleryFactory.getAnimalGallery() ];

                case EnumGallerySelection.Business:
                    return [ GalleryFactory.getBusinessGallery() ];

                case EnumGallerySelection.Paintings:
                    return  [ GalleryFactory.getArtGallery() ];
            }
        }

        return  [ GalleryFactory.getAnimalGallery(),
            GalleryFactory.getBusinessGallery(),
            GalleryFactory.getArtGallery() ];
    }

    private data(): any {
        return {};
    }

}

Vue.component('data-input-image-selector', DataInputImageSelectorComponent);
