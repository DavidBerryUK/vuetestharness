import { EnumTab }                              from '@/harnessFramework/enums/HarnessFrameworkEnums';
import { Prop }                                 from 'vue-property-decorator';
import Component                                from 'vue-class-component';
import ElementPageWrapperComponent              from '@/componentsGui/elementPageWrapperComponent/ElementPageWrapperComponent';
import ElementTitleWrapperComponent             from '@/componentsGui/elementTitleComponent/ElementTitleWrapperComponent';
import EventCollectionModel                     from '@/harnessFramework/models/EventLogCollectionModel';
import EventLogComponent                        from '../eventsLog/EventLogComponent';
import ParametersComponent                      from '../parametersForm/ParametersFormComponent';
import Vue                                      from 'vue';

@Component({
    components: {
        ParametersComponent,
        ElementPageWrapperComponent,
        ElementTitleWrapperComponent,
        EventLogComponent,
    },
  })
export default class TestHarnessComponent extends Vue {

    private EnumTab = EnumTab;

    @Prop()
    public title!: string;

    @Prop()
    public titleIconCss!: string;

    @Prop()
    public value!: any;

    @Prop({ type: Number, default: EnumTab.Parameters })
    public selectedTab?: EnumTab;

    private  currentTab: EnumTab = EnumTab.Parameters;

    public eventLog: EventCollectionModel = new EventCollectionModel();

    private mounted() {
        if ( this.selectedTab === undefined) {
            this.currentTab = EnumTab.Parameters;
        } else {
            this.currentTab = this.selectedTab;
        }
    }

    public data(): any {
        return {};
    }

    private get showControls(): boolean {
        if (this.$slots.controls) {
            return true;
        }
        return false;
    }

    public auditEvent(eventName: string, eventValue: string) {
        this.eventLog.addEvent(eventName, eventValue);
    }

}

Vue.component('test-harness', TestHarnessComponent);
