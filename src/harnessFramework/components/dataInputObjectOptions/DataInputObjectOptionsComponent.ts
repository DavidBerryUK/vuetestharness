import { Prop }                             from 'vue-property-decorator';
import Component                            from 'vue-class-component';
import TestHarnessFieldPropertyModel        from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import Vue                                  from 'vue';


@Component
export default class DataInputObjectOptionsComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public meta!: TestHarnessFieldPropertyModel;

    private get objectListFromProvider(): Array<string> {
        return this.value.dataForField(this.meta.name);
    }

    public  itemObject(obj: any): any {
        return obj;
    }

    private data(): any {
        return {};
    }
}

Vue.component('data-input-object-options', DataInputObjectOptionsComponent);
