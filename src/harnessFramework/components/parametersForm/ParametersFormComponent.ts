import { EnumDocumentSection }          from '@/harnessFramework/decorators/enums/DecoratorEnums';
import { Prop }                         from 'vue-property-decorator';
import Component                        from 'vue-class-component';
import ParametersEventListComponent     from '../parametersEventList/ParametersEventListComponent';
import ParametersListComponent          from '../parametersList/ParametersListComponent';
import TestHarnessFieldPropertyModel    from '@/harnessFramework/decorators/models/TestHarnessFieldPropertyModel';
import PropertyHelper        from '@/harnessFramework/decorators/service/PropertyHelper';
import Vue                              from 'vue';
import ConvertStringToBoolean from '@/utilities/convertions/ConvertStringToBoolean';


@Component({
    components: {
        ParametersListComponent,
        ParametersEventListComponent,
    },
  })
export default class ParametersFormComponent extends Vue {

    @Prop()
    public value!: any;

    @Prop()
    public showProperties!: boolean;

    @Prop()
    public showEvents!: boolean;

    public get evaluatedShowProperties(): boolean {
        return ConvertStringToBoolean.convert(this.showProperties, false);
    }

    public get evaluatedShowEvents(): boolean {
        return ConvertStringToBoolean.convert(this.showEvents, false);
    }

    get doParametersExist(): boolean {
        return (this.modelFields.length + this.attributes.length) > 0;
    }

    get doEventsExist(): boolean {
        return (this.modelFields.length + this.events.length) > 0;
    }

    public get modelFields(): Array<TestHarnessFieldPropertyModel> {
        const parameters = PropertyHelper.getProperties(this.value);
        const data = parameters.filter((item) => item.documentSection === EnumDocumentSection.model);
        return data;
    }

    public get attributes(): Array<TestHarnessFieldPropertyModel> {
        const parameters = PropertyHelper.getProperties(this.value);
        const data = parameters.filter((item) => item.documentSection === EnumDocumentSection.attributes);
        return data;
    }

    public get events(): Array<TestHarnessFieldPropertyModel> {
        const parameters = PropertyHelper.getProperties(this.value);
        const data = parameters.filter((item) => item.documentSection === EnumDocumentSection.events);
        return data;
    }

    private data(): any {
        return {};
    }

}

Vue.component('parameters-form', ParametersFormComponent);
