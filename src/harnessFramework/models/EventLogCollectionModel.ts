import EventLogItemModel                           from './EventLogItemModel';

export default class EventLogCollectionModel {

    public events: Array<EventLogItemModel>;


    constructor() {
        this.events = new Array<EventLogItemModel>();
    }


    public get eventListReversed(): Array<EventLogItemModel> {
            return this.events.slice().reverse();
    }

    public addEvent(eventName: string, eventValue: string) {
        const event = new EventLogItemModel(this.events.length + 1, eventName, eventValue );
        this.events.push(event);
    }
}
