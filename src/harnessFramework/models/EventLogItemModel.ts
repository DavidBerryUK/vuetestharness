
import DateTimeConstants                        from '@/constants/DateTimeConstants';
import moment                                   from 'moment';

export default class EventLogItemModel {

    public eventId: number;

    public dateTime: moment.Moment;

    public name: string;

    public values: string;

    public get formattedDate(): string {
        return this.dateTime.format(DateTimeConstants.TimeWithMilliseconds);
    }

    constructor(eventId: number, name: string, values: string) {
        this.eventId = eventId;
        this.name = name;
        this.values = values;
        this.dateTime = moment();
    }
}
