import Component                                  from 'vue-class-component';
import Vue                                        from 'vue';
import ComponentIconConstants                     from '@/constants/ComponentIconConstants';
import ElementTitleWrapperComponent               from '@/componentsGui/elementTitleComponent/ElementTitleWrapperComponent';
import ElementPageWrapperComponent                from '@/componentsGui/elementPageWrapperComponent/ElementPageWrapperComponent';

@Component({
  components: {
    ElementTitleWrapperComponent,
    ElementPageWrapperComponent,
  },
})
export default class AboutPage extends Vue {

    public ComponentIconConstants = ComponentIconConstants;

    public data(): any  {
      return {};
    }
  }
