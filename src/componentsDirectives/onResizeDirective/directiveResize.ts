import { DirectiveBinding }                   from 'vue/types/options';
import { VNode }                              from 'vue';
import ResizeObserverPolyfill                 from '@juggle/resize-observer';
import ResizeObserverEntry                    from '@juggle/resize-observer/lib/ResizeObserverEntry';
import ResizeObserverParam                    from '@juggle/resize-observer';

// https://www.npmjs.com/package/@juggle/resize-observer

export const directiveResize = {
  bind( el: HTMLElement, binding: DirectiveBinding, vnode: VNode, oldVnode: VNode) {

   // el.style.position = 'fixed';

    if (vnode !== undefined && vnode.context !== undefined) {
      //
      // the element has been bound and we are ready to start
      // actual work...
      //

      let ResizeObserverFunction = (window as any).ResizeObserver;

      if (ResizeObserverFunction == null) {
        ResizeObserverFunction = ResizeObserverPolyfill;
      }

      const ro = new ResizeObserverFunction((entries: ResizeObserverEntry[], observer: ResizeObserverParam) => {
        entries.forEach((entry, index) => {
          const { width, height } = entry.contentRect;
          if (vnode !== undefined && vnode.context !== undefined) {

            // how an event is raised changes depending of the parent of the directive is a
            // Vue component or not
            //
            const eventName = 'resize';
            const eventData = {width, height };
            if (vnode.componentInstance) {
              vnode.componentInstance.$emit(eventName, {detail: eventData});
            } else {
              if (vnode.elm !== undefined) {
                vnode.elm.dispatchEvent(new CustomEvent(eventName, {detail: eventData}));
              }
            }


          }
        });
      });

      ro.observe(el);
    }
  },

  // unbind() {
  // },

};
